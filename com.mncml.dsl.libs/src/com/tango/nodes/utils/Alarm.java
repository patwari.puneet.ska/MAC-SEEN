package com.tango.nodes.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by admin on 03-10-2015.
 */
public class Alarm
{
    private int alarmId;
    private String timeStamp;
    private AlarmSeverityLevel severityLevel;
    private AlarmState alarmState;
    private String failedObjectId;
    private List<String> alarmDetectionPointList;
    private String parameterName;
    private String parameterVale;
    private String description;

    public Alarm(int alarmId, AlarmSeverityLevel severityLevel, AlarmState alarmState, String failedObjectId, String parameterName, String parameterVale, String description) {
        this.alarmId = alarmId;
        this.timeStamp = new Date().toString();
        this.severityLevel = severityLevel;
        this.alarmState = alarmState;
        this.failedObjectId = failedObjectId;
        this.alarmDetectionPointList = new ArrayList<String>();
        this.parameterName = parameterName;
        this.parameterVale = parameterVale;
        this.description = description;
    }

    public AlarmState getAlarmState() {
        return alarmState;
    }

    public void setDescription(String description) { this.description = description;}

    public void setAlarmState(AlarmState alarmState) {
        this.alarmState = alarmState;
    }

    public void addDetectionPoint(String detectionPoint)
    {
        alarmDetectionPointList.add(detectionPoint);
    }

    public void removeDetectionPoint(String detectionPoint) {
        alarmDetectionPointList.remove(detectionPoint);
    }

    public void setParameterVale(String parameterVale) {
        this.parameterVale = parameterVale;
    }

    public boolean alarmPersist()
    {
        if(alarmDetectionPointList.size() == 0)
            return false;
        else return true;
    }

    public String[] getAlarmInfo()
    {
        String[] alarmInfo = new String[9];
        alarmInfo[0] = String.valueOf(alarmId);
        alarmInfo[1] = timeStamp;
        alarmInfo[2] = severityLevel.toString();
        alarmInfo[3] = alarmState.toString();
        alarmInfo[4] = failedObjectId;
        alarmInfo[5] = String.valueOf(alarmDetectionPointList);
        alarmInfo[6] = parameterName;
        alarmInfo[7] = parameterVale;
        alarmInfo[8] = description;

        return alarmInfo;
    }
}
