package Emulator.generator

import Emulator.util.GeneratorUtils
import com.google.inject.Inject
import java.util.List
import mncModel.AlarmBlock
import mncModel.CommandResponseBlock
import mncModel.ControlNode
import mncModel.DataPoint
import mncModel.DataPointBlock
import mncModel.EventBlock
import mncModel.InterfaceDescription
import mncModel.Model
import mncModel.PrimitiveValue
import mncModel.PrimitiveValueType
import mncModel.SimpleType
import mncModel.impl.BoolValueImpl
import mncModel.impl.ControlNodeImpl
import mncModel.impl.FloatValueImpl
import mncModel.impl.IntValueImpl
import mncModel.impl.InterfaceDescriptionImpl
import mncModel.impl.StringValueImpl
import org.eclipse.emf.common.util.EList
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.jface.dialogs.MessageDialog
import org.eclipse.ui.PlatformUI
import org.eclipse.xtext.generator.IFileSystemAccess
import org.eclipse.xtext.generator.IGenerator

class EmulatorJavaGenerator implements IGenerator {
	@Inject
	static EList<DataPointBlock> dataPointBlockList
	@Inject
	static EList<AlarmBlock> alarmBlockList
	@Inject
	static EList<EventBlock> eventBlockList
	@Inject
	static EList<CommandResponseBlock> commandResponseBlockList
	IFileSystemAccess fsa
	// @Inject
	// ArrayList<String> nodeLists
	var controlNode = null as ControlNodeImpl
	boolean connect = false;

	def void setConnect(boolean connect) {
		this.connect = connect
	}
 
	override doGenerate(Resource resource, IFileSystemAccess fsa) {
		this.fsa = fsa

 

		val instance = resource.contents.head as Model
		if (instance !== null && instance.eContents.filter(InterfaceDescriptionImpl)!==null && instance.eContents.filter(InterfaceDescriptionImpl).size!==0 && instance.eContents.filter(ControlNode)!==null && instance.eContents.filter(ControlNode).size!==0) {
			val interface = instance.eContents.filter(InterfaceDescriptionImpl).get(0)
			controlNode = instance.eContents.filter(ControlNodeImpl).get(0)
			dataPointBlockList = new GeneratorUtils().getDataPointBlocks(controlNode) 
			eventBlockList = new GeneratorUtils().getEventBlocks(controlNode)
		 	alarmBlockList = new GeneratorUtils().getAlarmBlocks(controlNode)
			commandResponseBlockList = new GeneratorUtils().getCommandResponseBlocks(controlNode)
			val className = controlNode.name
			if (instance !== null) {
				fsa.generateFile(className + ".java",
					toJavaCode(interface as InterfaceDescriptionImpl, controlNode as ControlNodeImpl, fsa))
			}
		}else {

			PlatformUI.getWorkbench().getDisplay().asyncExec(
				new Runnable() {

					override run() {
						var shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
						MessageDialog.openError(shell, "Error",
							"Please create the interface description and the control node");
					}
				});
			
		}
	}

 
	
def toJavaCode(InterfaceDescriptionImpl ides, ControlNodeImpl controlNode,IFileSystemAccess fsa) {
		'''


/*----- PROTECTED REGION END -----*///	�controlNode.name.toFirstUpper�.java


package com.tango.nodes.java;

/*----- PROTECTED REGION ENABLED START -----*/

import utils.tango.Alarm;
import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DevState;
import fr.esrf.Tango.DispLevel;
import fr.esrf.TangoApi.DevicePipe;
import fr.esrf.TangoApi.PipeBlob;
import fr.esrf.TangoApi.PipeDataElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.ext.XLogger;
import org.slf4j.ext.XLoggerFactory;
import org.tango.DeviceState;
import org.tango.server.InvocationContext;
import org.tango.server.ServerManager;
import org.tango.server.annotation.*;
import org.tango.server.attribute.AttributeValue;
import org.tango.server.device.DeviceManager;
import org.tango.server.dynamic.DynamicManager;
import org.tango.server.events.EventType;
import org.tango.server.pipe.PipeValue;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import static java.lang.Thread.sleep;


//	Import Tango IDL types
// additional imported packages

/*----- PROTECTED REGION END -----*///	�controlNode.name.toFirstUpper�.imports
  
@Device
public class �controlNode.name.toFirstUpper� {

	public static final Logger logger = LoggerFactory.getLogger(�controlNode.name.toFirstUpper�.class);
	private static final XLogger xlogger = XLoggerFactory
			.getXLogger(�controlNode.name.toFirstUpper�.class);
	// private static String instance;
	// ========================================================
	// Programmer's data members
	// ========================================================
	/*----- PROTECTED REGION interfaceDescription(�controlNode.name.toFirstUpper�.variables) ENABLED START -----*/

	// Put static variables here

	/*----- PROTECTED REGION END -----*/// �controlNode.name.toFirstUpper�.variables
	/*----- PROTECTED REGION interfaceDescription(�controlNode.name.toFirstUpper�.private) ENABLED START -----*/


	// Put private variables here
	private Map<Integer, Alarm> alarmMap;
	private PipeBlob responsePipeBlob;
    private PipeBlob alarmPipeBlob;
	private static String deviceName = "nodes/�controlNode.name.toFirstUpper�/test";
	
	//========================================================
	//	Property data members and related methods
	//========================================================
	�ides.declareDevicePropertiesAndDynamicAttributes(fsa)�
	
	//========================================================
	//	Miscellaneous methods
	//========================================================
	/**
	 * Initialize the device.
	 * 
	 * @throws DevFailed if something fails during the device initialization.
	 */
	@Init(lazyLoading = false)
	public void initDevice() throws DevFailed {
		xlogger.entry();
	 	logger.debug("init device " + deviceManager.getName());
		/*----- PROTECTED REGION interfaceDescription(LeafNode.initDevice) ENABLED START -----*/

        //	Put your device initialization code here
        /*----- PROTECTED REGION END -----*/	//	LeafNode.initDevice
		xlogger.exit();
	}
	
	
        //	Put your code here
		
		/*----- PROTECTED REGION END -----*/	//	LeafNode.setDynamicManager

	
	/**
	 * Device management. Will be injected by the framework.
	 */
	@DeviceManagement
	DeviceManager deviceManager;
	public void setDeviceManager(DeviceManager deviceManager){
		this.deviceManager= deviceManager ;
	}      
	
	// ========================================================
	// Attribute data members and related methods
	// ========================================================
	�declareAttributes(controlNode)�

	
	//========================================================
	//	Pipe data members and related methods
	//========================================================
	/**
	 * Pipe Response
	 * description:
	 *     
	 */
	@Pipe(description="", displayLevel=DispLevel._OPERATOR, label="Response")
	private PipeValue response;
	/**
	 * Read Pipe Response
	 * 
	 * @return attribute value
	 * @throws DevFailed if read pipe failed.
	 */
	public PipeValue getResponse() throws DevFailed {
		xlogger.entry();
		/*----- PROTECTED REGION interfaceDescription(LeafNode.getResponse) ENABLED START -----*/

        //	Put read attribute code here
		
		/*----- PROTECTED REGION END -----*/	//	LeafNode.getResponse
		xlogger.exit();
		return response;
	}
	/**
	 * Pipe Alarm
	 * description:
	 *     
	 */
	@Pipe(description="", displayLevel=DispLevel._OPERATOR, label="Alarm")
	private PipeValue alarm;
	/**
	 * Read Pipe Alarm
	 * 
	 * @return attribute value
	 * @throws DevFailed if read pipe failed.
	 */
	public PipeValue getAlarm() throws DevFailed {
		xlogger.entry();
		/*----- PROTECTED REGION interfaceDescription(LeafNode.getAlarm) ENABLED START -----*/
        //	Put read attribute code here
		
		/*----- PROTECTED REGION END -----*/	//	LeafNode.getAlarm
		xlogger.exit();
		return alarm;
	}
	
	//========================================================
	//	Command data members and related methods
	//========================================================
	/**
	 * The state of the device
	*/
	@State
	private DevState state = DevState.UNKNOWN;
	/**
	 * Execute command "State".
	 * description: This command gets the device state (stored in its 'state' data member) and returns it to the caller.
	 * @return Device state
	 * @throws DevFailed if command execution failed.
	 */
	public final DevState getState() throws DevFailed {
		/*----- PROTECTED REGION interfaceDescription(LeafNode.getState) ENABLED START -----*/

        //	Put state code here
		
		/*----- PROTECTED REGION END -----*/	//	LeafNode.getState
		return state;
	}
	/**
	 * Set the device state
	 * @param state the new device state
	 */
	public void setState(final DevState state) {
		this.state = state;
	}
	
	/**
	 * The status of the device
	 */
	@Status
	private String status = "Server is starting. The device state is unknown";
	/**
	 * Execute command "Status".
	 * description: This command gets the device status (stored in its 'status' data member) and returns it to the caller.
	 * @return Device status
	 * @throws DevFailed if command execution failed.
	 */
	public final String getStatus() throws DevFailed {
		/*----- PROTECTED REGION interfaceDescription(LeafNode.getStatus) ENABLED START -----*/

        //	Put status code here
		
		/*----- PROTECTED REGION END -----*/	//	LeafNode.getStatus
		return status;
	}
	/**
	 * Set the device status
	 * @param status the new device status
	 */
	public void setStatus(final String status) {
		this.status = status;
	}
	�declareCommand(controlNode)�


	
	/*----- PROTECTED REGION END -----*/// �controlNode.name.toFirstUpper�.methods

	/**
	 * Starts the server.
	 * 
	 * @param args
	 *            program arguments (instance_name [-v[trace level]] [-nodb
	 *            [-dlist <device name list>] [-file=fileName]])
	 */
	public static void main(final String[] args) {
		//DataBaseHandler.addDevice(deviceName); // Uncomment this to enter the device into the database
		ServerManager.getInstance().start(args, �controlNode.name.toFirstUpper�.class);
		logger.info("------- Started -------------");

	}

}

'''
	}
	
	def declareDevicePropertiesAndDynamicAttributes(InterfaceDescription ides,IFileSystemAccess fsa) {
		{
			var List<String> devicePropertiesAsPerTango =newArrayList()
			devicePropertiesAsPerTango.add("description")
			devicePropertiesAsPerTango.add("default")
	 		devicePropertiesAsPerTango.add("isMandatory")
		'''
		�IF ides!==null�
		�var dataPointBlock = ides.dataPoints�
		�IF dataPointBlock!==null�
		�var dataPointList = dataPointBlock.dataPoints�
		�IF dataPointList!==null�
		�FOR dataPoint : dataPointList�
  		�IF dataPoint.name.contains("deviceProperty")�
	/** 
	 * Device Property �dataPoint.name.replace("deviceProperty_","").toFirstUpper�
	 * Name of the LMC to which leaf node should connect
	 */
 	@DeviceProperty(name="�dataPoint.name.toFirstUpper�"
�IF dataPoint.parameters!==null� 
�var dataPointAllParams =   dataPoint.parameters�
�FOR param : dataPointAllParams�
�var simpleType = param as SimpleType�
�IF devicePropertiesAsPerTango.contains(simpleType.name)�
�IF !param.equals(dataPointAllParams.last)�,�ENDIF� 
�simpleType.name�=�IF simpleType.type.equals(PrimitiveValueType.STRING)�"�ParameterValueFinder.findValue(simpleType.value)�"
�ELSE��ParameterValueFinder.findValue(simpleType.value)��ENDIF�
�IF !param.equals(dataPointAllParams.last)�,�ENDIF� 
�ENDIF�
�ENDFOR�
)
 �ENDIF� 
	private �ParameterValueFinder.findValueType(dataPoint)� �dataPoint.name.toFirstLower�;
	/**
	 * set property �dataPoint.name.toFirstUpper�
	 * @param  �dataPoint.name.toFirstLower�  see description above.
	 */
	public void set�dataPoint.name.toFirstUpper�(String �dataPoint.name.toFirstLower�) {
	   this.�dataPoint.name.toFirstLower� = �dataPoint.name.toFirstLower�;
	/*----- PROTECTED REGION interfaceDescription(LeafNode.set�dataPoint.name.toFirstUpper�) ENABLED START -----*/

	//	Check property value here
		
		/*----- PROTECTED REGION END -----*/	//	LeafNode.setAssignedLMC
	}
	�ELSEIF dataPoint.name.contains("dynamicAttribute")� 
	�fsa.generateFile(dataPoint.name.toFirstUpper+".java",createDynamicAttributeFile(dataPoint,controlNode))�
		�ENDIF�
		�ENDFOR�
		�ENDIF�
		�ENDIF�
		�ENDIF�
		'''
	} 
	
	}
	
	def createDynamicAttributeFile(DataPoint dataPoint,ControlNode controlNode) {
'''
/*----- PROTECTED REGION END -----*/	//	LeafNode.DynamicBooleanAttribute.java

package org.tango.nodes;

import org.tango.DeviceState;
import org.tango.server.StateMachineBehavior;
import org.tango.server.attribute.IAttributeBehavior;
import org.tango.server.attribute.AttributeValue;
import org.tango.server.attribute.AttributeConfiguration;
import org.tango.server.attribute.AttributePropertiesImpl;

//	Import Tango IDL types
import fr.esrf.Tango.*;


/*----- PROTECTED REGION interfaceDescription(LeafNode.DynamicBooleanAttribute.addImports) ENABLED START -----*/

/*----- PROTECTED REGION END -----*/	//	�controlNode.name.toFirstUpper�.�dataPoint.name.toFirstUpper�.addImports


/**
 * Dynamic attribute �dataPoint.name.toFirstUpper�, �ParameterValueFinder.findValueType(dataPoint)�, Scalar, READ
 * description: �IF dataPoint.parameters!==null��FOR param : dataPoint.parameters�
 �var simpleType = dataPoint as SimpleType�
 �IF simpleType.name.contains("description") && simpleType.value!==null��ParameterValueFinder.findValue(simpleType.value)��ENDIF�
 �ENDFOR��ENDIF�
 *     
 */
public class �dataPoint.name.toFirstUpper� implements IAttributeBehavior {

	/**	The attribute name */
	private String  attributeName;

	/*----- PROTECTED REGION interfaceDescription(�controlNode.name.toFirstUpper�.�dataPoint.name.toFirstUpper�.dataMembers) ENABLED START -----*/
	
	//	Put your data member declarations
	private boolean attrValue;
	
	/*----- PROTECTED REGION END -----*/	//	�controlNode.name.toFirstUpper�.�dataPoint.name.toFirstUpper�.dataMembers

	/**
	 * Dynamic attribute �dataPoint.name.toFirstUpper� constructor
	 * @param attributeName The dynamic attribute name
	 */
	public �dataPoint.name.toFirstUpper�(String attributeName) {
		this.attributeName = attributeName;
	}

	/**
	 * Build and return the configuration for dynamic attribute �dataPoint.name.toFirstUpper�.
	 * @return the configuration for dynamic attribute �dataPoint.name.toFirstUpper�.
	 * @throws DevFailed in case of configuration error.
	 */
	@Override
	public AttributeConfiguration getConfiguration() throws DevFailed {
		AttributeConfiguration  config = new AttributeConfiguration();
		config.setName(attributeName);
		config.setType(�ParameterValueFinder.findValueType(dataPoint)�.class);
		config.setFormat(AttrDataFormat.SCALAR);
		config.setWritable(AttrWriteType.READ);
		config.setDispLevel(DispLevel.OPERATOR);
	
		//	Set attribute properties
		AttributePropertiesImpl	properties = new AttributePropertiesImpl();
		config.setAttributeProperties(properties);
		return config;
	}

	/**
	 * Get dynamic attribute state machine
	 * @return the attribute state machine
	 * @throws DevFailed if the state machine computation failed.
	 */
	@Override
	public StateMachineBehavior getStateMachine() throws DevFailed {
		StateMachineBehavior stateMachine = new StateMachineBehavior();
		/*----- PROTECTED REGION interfaceDescription(�controlNode.name.toFirstUpper�.�dataPoint.name.toFirstUpper�) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	�controlNode.name.toFirstUpper�.�dataPoint.name.toFirstUpper�.getStateMachine
		return stateMachine;
	}

	/**
	 * Get dynamic attribute value
	 * @return the attribute value
	 * @throws DevFailed if the read value failed.
	 */
	@Override
	public AttributeValue getValue() throws DevFailed {
		boolean	readValue;
		/*----- PROTECTED REGION interfaceDescription(�controlNode.name.toFirstUpper�.�dataPoint.name.toFirstUpper�.getValue) ENABLED START -----*/
		readValue = attrValue;
		/*----- PROTECTED REGION END -----*/	//	LeafNode.DynamicBooleanAttribute.getValue
		return new AttributeValue(readValue);
	}

	/**
	 * Set dynamic attribute value
	 * @param writeValue the attribute value
	 * @throws DevFailed if the set value failed.
	 */
	@Override
	public void setValue(AttributeValue writeValue) throws DevFailed {
		/*----- PROTECTED REGION interfaceDescription(�controlNode.name.toFirstUpper�.�dataPoint.name.toFirstUpper�.setValue) ENABLED START -----*/
		attrValue = (Boolean) writeValue.getValue();
		/*----- PROTECTED REGION END -----*/	//	�controlNode.name.toFirstUpper�.�dataPoint.name.toFirstUpper�.setValue
	}

	/*----- PROTECTED REGION interfaceDescription(�controlNode.name.toFirstUpper�.�dataPoint.name.toFirstUpper�.methods) ENABLED START -----*/
	
	//	Put your own methods
	
	/*----- PROTECTED REGION END -----*/	//	LeafNode.DynamicBooleanAttribute.methods
}
		
		'''
}
	
 
def declareCommand(ControlNode controlNode){
 var List<String> commandPropertiesAsPerTango = newArrayList()
 commandPropertiesAsPerTango.add("inTypeDesc")
  commandPropertiesAsPerTango.add("outTypeDesc")
 '''
 �IF controlNode!==null�
 �var commandBlockUtil = controlNode.commandResponseBlocks�
 �IF commandBlockUtil!==null�
 �var commandBlockList = controlNode.commandResponseBlocks.commandResponseBlocks�
 �IF commandBlockList!==null�
 �FOR commandBlock : commandBlockList�
 �var command = commandBlock.command�
 �var commandParameters = command.parameters�
@Command(name="�command.name�" 
�IF commandParameters!==null� 
�FOR param : commandParameters�
�var simpleType = param as SimpleType�
�IF commandPropertiesAsPerTango.contains(simpleType.name)�
�IF !param.equals(commandParameters.last)�,�ENDIF�
�simpleType.name�=�IF simpleType.type.equals(PrimitiveValueType.STRING)�"�ParameterValueFinder.findValue(simpleType.value)�"
�ELSE��ParameterValueFinder.findValue(simpleType.value)��ENDIF�
�IF !param.equals(commandParameters.last)�,�ENDIF�
�ENDIF�
�ENDFOR�
 �ENDIF�  
 )
 public String �command.name�(String �command.name�Params) throws DevFailed {
xlogger.entry();
		String �command.name�Out = null;
		/*----- PROTECTED REGION interfaceDescription(�controlNode.name.toFirstUpper�.�command.name�) ENABLED START -----*/
        //	Put command code here
		xlogger.exit();
		�IF this.connect�
		�command.name�Out = new �controlNode.name.toFirstUpper�Simulator().simulateResponse("�command.name�",�command.name�Params);
		�ENDIF�
		return �command.name�Out;
	} 
 �ENDFOR�
 �ENDIF�
 �ENDIF�
 �ENDIF� 
 
	'''
}
	def declareAttributes(ControlNode controlNode) 
	'''  
	�IF controlNode!==null�
	�IF controlNode.dataPointBlocks!==null�
	�IF controlNode.dataPointBlocks.dataPointBlocks!==null�
	�var EList<DataPointBlock> dataPointList = controlNode.dataPointBlocks.dataPointBlocks� 
	�IF dataPointList !== null� 
			�FOR dataPoint : dataPointList� 
				@Attribute(name="�dataPoint.dataPoint.name.toFirstUpper�")
		 		�IF dataPoint.dataPointHandling.checkDataPoint!==null�
				@AttributeProperties(minAlarm="�dataPoint.dataPointHandling.checkDataPoint.checkMinValue.dataPointValue�",maxAlarm="�dataPoint.dataPointHandling.checkDataPoint.checkMaxValue.dataPointValue�")
				�ENDIF�
				private �dataPoint.dataPoint.type� �dataPoint.dataPoint.name� �IF dataPoint.dataPoint.value!==null�= �dataPoint.dataPoint.value.getDataPointValue��ENDIF�;
				public synchronized �dataPoint.dataPoint.type� get�dataPoint.dataPoint.name�(){
					return this.�dataPoint.dataPoint.name�;
				}
				public synchronized void set�dataPoint.dataPoint.name�(�dataPoint.dataPoint.type� �dataPoint.dataPoint.name�) throws DevFailed{
					this.�dataPoint.dataPoint.name�  = �dataPoint.dataPoint.name�;
				} 
			�ENDFOR�
		�ENDIF�	 
		�ENDIF�	
		�ENDIF�	
		�ENDIF�		
	'''
	
	def getDataPointValue(PrimitiveValue value) {
		if(value !==null){
		if (value instanceof IntValueImpl) {
			return value.intValue
	 	}
		if (value instanceof FloatValueImpl) {
			return value.floatValue
		}
		if (value instanceof StringValueImpl) {
			return value.stringValue
		} 
		if (value instanceof BoolValueImpl) {
			return value.boolValue
		}
		
		}else {
			return null;
		}
	}  


	
	

}
