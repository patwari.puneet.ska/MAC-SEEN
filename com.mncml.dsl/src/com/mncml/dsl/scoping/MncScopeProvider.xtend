
package com.mncml.dsl.scoping

import com.google.common.base.Function
import com.google.inject.Inject
import java.util.List
import mncModel.Action
import mncModel.Alarm
import mncModel.AlarmBlock
import mncModel.CheckParameterCondition
import mncModel.Command
import mncModel.CommandDistribution
import mncModel.CommandResponseBlock
import mncModel.CommandTranslation
import mncModel.CommandValidation
import mncModel.ControlNode
import mncModel.DataPoint
import mncModel.DataPointBlock
import mncModel.Event
import mncModel.EventBlock
import mncModel.InterfaceDescription
import mncModel.Model
import mncModel.OperatingState
import mncModel.Parameter
import mncModel.ParameterTranslation
import mncModel.Response
import mncModel.ResponseBlock
import mncModel.ResponseTranslationRule
import mncModel.ResponseValidation
import mncModel.ResponsibleItemList
import mncModel.Transition
import org.eclipse.emf.ecore.EReference
import org.eclipse.xtext.naming.IQualifiedNameProvider
import org.eclipse.xtext.naming.QualifiedName
import org.eclipse.xtext.scoping.IScope
import org.eclipse.xtext.scoping.Scopes
import org.eclipse.xtext.scoping.impl.AbstractDeclarativeScopeProvider

import static extension org.eclipse.xtext.EcoreUtil2.*

class MncScopeProvider extends AbstractDeclarativeScopeProvider {

	@Inject
	IQualifiedNameProvider qualifiedNameProvider

	def getCandidateOperatingStatesForTransitions(Transition transition) {
		var List<OperatingState> listOfOperatingStates = newArrayList()
		var interfaceDescription = transition.getContainerOfType(ControlNode).interfaceDescription
		if (interfaceDescription.operatingStates !== null)
			listOfOperatingStates.addAll(interfaceDescription.operatingStates.operatingStates)
		for (interfaceDescriptionInUses : interfaceDescription.uses)
			if (interfaceDescriptionInUses.operatingStates !== null)
				listOfOperatingStates.addAll(interfaceDescriptionInUses.operatingStates.operatingStates)
		listOfOperatingStates
	} 

	def getCandidateCommands(ControlNode controlNode) {
		var interfaceDescription = controlNode.interfaceDescription
		var List<Command> listOfCommands = newArrayList()
		if (interfaceDescription.commands !== null)
			listOfCommands.addAll(interfaceDescription.commands.commands)

		for (interfaceDescriptionInUses : interfaceDescription.uses) {
			if(interfaceDescriptionInUses.commands!==null)
				listOfCommands.addAll(interfaceDescriptionInUses.commands.commands)
		}
		listOfCommands
	}

	def getCandidateAlarms(InterfaceDescription interfaceDescription) {
		var List<Alarm> listOfAlarms = newArrayList()
		if (interfaceDescription.alarms !== null)
			listOfAlarms.addAll(interfaceDescription.alarms.alarms)
		
		if (interfaceDescription.subscribedItems !== null)
		{
			if (interfaceDescription.subscribedItems.subscribedAlarms !== null)
				listOfAlarms.addAll(interfaceDescription.subscribedItems.subscribedAlarms)
		}
		
		for (interfaceDescriptionInUses : interfaceDescription.uses)
			if (interfaceDescriptionInUses.alarms !== null)
				listOfAlarms.addAll(interfaceDescriptionInUses.alarms.alarms)
		listOfAlarms
	}

	def getCandidateEvents(InterfaceDescription interfaceDescription) {
		var List<Event> listOfEvents = newArrayList()
		if (interfaceDescription.events !== null)
			listOfEvents.addAll(interfaceDescription.events.events)
			
		if (interfaceDescription.subscribedItems !== null)
			if (interfaceDescription.subscribedItems.subscribedEvents !== null)
				listOfEvents.addAll(interfaceDescription.subscribedItems.subscribedEvents)
				
		for (interfaceDescriptionInUses : interfaceDescription.uses)
			if (interfaceDescriptionInUses.events !== null)
				listOfEvents.addAll(interfaceDescriptionInUses.events.events)
		listOfEvents
	}

	def getCandidateResponses(InterfaceDescription interfaceDescription) {
		var List<Response> listOfResponses = newArrayList()
		if (interfaceDescription.responses !== null)
			listOfResponses.addAll(interfaceDescription.responses.responses)
		for (interfaceDescriptionInUses : interfaceDescription.uses)
			if (interfaceDescriptionInUses.responses !== null)
				listOfResponses.addAll(interfaceDescriptionInUses.responses.responses)
		listOfResponses
	}

	def getCandidateDataPoints(InterfaceDescription interfaceDescription) {
		var List<DataPoint> listOfDataPoints = newArrayList()
		if (interfaceDescription.dataPoints !== null)
			listOfDataPoints.addAll(interfaceDescription.dataPoints.dataPoints)
		if (interfaceDescription.subscribedItems !== null)
		{
			if (interfaceDescription.subscribedItems.subscribedDataPoints !== null)
				listOfDataPoints.addAll(interfaceDescription.subscribedItems.subscribedDataPoints)
		}
		for (interfaceDescriptionInUses : interfaceDescription.uses)
			if (interfaceDescriptionInUses.dataPoints !== null)
				listOfDataPoints.addAll(interfaceDescriptionInUses.dataPoints.dataPoints)
		listOfDataPoints
	}

	def getCandidateChildNodes(ControlNode controlNode) {
		var List<ControlNode> listOfControlNodes = newArrayList()
		var model = controlNode.getContainerOfType(Model)
		for (system : model.systems) {
			if (system instanceof ControlNode && system !== controlNode)
				listOfControlNodes.add(system as ControlNode)
		}
		listOfControlNodes
	}

	def getCandidateResponsesFromChildNode(ControlNode controlNode) {
		var List<Response> listOfResponses = newArrayList()
		for (ccn : controlNode.childNodes) {
			if (ccn.interfaceDescription.responses !== null)
				listOfResponses.addAll(ccn.interfaceDescription.responses.responses)
			for (interfaceDescriptionInUses : ccn.interfaceDescription.uses)
				listOfResponses.addAll(interfaceDescriptionInUses.responses.responses)
		}
		listOfResponses
	}

	def getCandidateCommandsFromChildNode(ControlNode controlNode) {
		var List<Command> listOfCommands = newArrayList()
		for (childControlNode : controlNode.childNodes) {
			if (childControlNode.interfaceDescription.commands !== null)
				listOfCommands.addAll(childControlNode.interfaceDescription.commands.commands)
			for (interfaceDescriptionInUses : childControlNode.interfaceDescription.uses)
				listOfCommands.addAll(interfaceDescriptionInUses.commands.commands)
		}
		listOfCommands
	}

	def scope_Transition_currentState(Transition transition, EReference ref) {
		Scopes.scopeFor(
			getCandidateOperatingStatesForTransitions(transition),
			new Function<OperatingState, QualifiedName> {

				override apply(OperatingState input) {
					qualifiedNameProvider.getFullyQualifiedName(input)
				}

				override equals(Object object) {
					throw new UnsupportedOperationException("TODO: auto-generated method stub")
				}

			},
			IScope.NULLSCOPE
		)
	}

	def scope_Transition_nextState(Transition transition, EReference ref) {
		Scopes.scopeFor(
			getCandidateOperatingStatesForTransitions(transition),
			new Function<OperatingState, QualifiedName> {

				override apply(OperatingState input) {
					qualifiedNameProvider.getFullyQualifiedName(input)
				}

				override equals(Object object) {
					throw new UnsupportedOperationException("TODO: auto-generated method stub")
				}

			},
			IScope.NULLSCOPE
		)
	}

	def scope_Action_command(Action action, EReference ref) {
		Scopes.scopeFor(getCandidateCommands(action.getContainerOfType(ControlNode)),
			new Function<Command, QualifiedName> {
 
				override apply(Command input) {
					qualifiedNameProvider.getFullyQualifiedName(input)
				} 

				override equals(Object object) {
					throw new UnsupportedOperationException("TODO: auto-generated method stub")
				}

			},
			IScope.NULLSCOPE
		)
	}

	def scope_ResponseBlock_response(ResponseBlock responseBlock, EReference ref) {
		var controlNode = responseBlock.getContainerOfType((ControlNode))
		Scopes.scopeFor(
			getCandidateResponses(controlNode.interfaceDescription),
			new Function<Response, QualifiedName> {

				override apply(Response input) {
					qualifiedNameProvider.getFullyQualifiedName(input)
				}

				override equals(Object object) {
					throw new UnsupportedOperationException("TODO: auto-generated method stub")
				}

			},
			IScope.NULLSCOPE
		)
	}

	def scope_CheckParameterCondition_parameter(CheckParameterCondition checkParameterCondition, EReference ref) {
		if (checkParameterCondition.getContainerOfType(CommandValidation) !== null)
			Scopes.scopeFor(checkParameterCondition.getContainerOfType(CommandResponseBlock).command.parameters,
			new Function<Parameter, QualifiedName> {

				override apply(Parameter input) {
					qualifiedNameProvider.getFullyQualifiedName(input)
				}

				override equals(Object object) {
					throw new UnsupportedOperationException("TODO: auto-generated method stub")
				}

			},
			IScope.NULLSCOPE
			)
		else
			Scopes.scopeFor(checkParameterCondition.getContainerOfType(ResponseValidation).response.parameters,
				new Function<Parameter, QualifiedName> {

				override apply(Parameter input) {
					qualifiedNameProvider.getFullyQualifiedName(input)
				}

				override equals(Object object) {
		 			throw new UnsupportedOperationException("TODO: auto-generated method stub")
				}

			},
			IScope.NULLSCOPE
			)
	}

	def scope_CommandTranslation_translatedCommands(CommandTranslation commandTranslation, EReference ref) {
		Scopes.scopeFor(
			getCandidateCommandsFromChildNode(commandTranslation.getContainerOfType(ControlNode)),
			new Function<Command, QualifiedName> {

				override apply(Command input) {
					qualifiedNameProvider.getFullyQualifiedName(input)
				}

				override equals(Object object) {
					throw new UnsupportedOperationException("TODO: auto-generated method stub")
				}

			},
			IScope.NULLSCOPE
		)
	}


	def scope_CommandResponseBlock_command(CommandResponseBlock commandResponseBlock, EReference ref) {
		Scopes.scopeFor(
			getCandidateCommands(commandResponseBlock.getContainerOfType(ControlNode)),
			new Function<Command, QualifiedName> {

				override apply(Command input) {
					qualifiedNameProvider.getFullyQualifiedName(input)
				}

				override equals(Object object) {
					throw new UnsupportedOperationException("TODO: auto-generated method stub")
				}

			},
			IScope.NULLSCOPE
		)

	}
	
	def scope_EventBlock_event(EventBlock eventBlock, EReference ref) {
		Scopes.scopeFor(
			getCandidateEvents(eventBlock.getContainerOfType(ControlNode).interfaceDescription),
			new Function<Event, QualifiedName> {

				override apply(Event input) {
					qualifiedNameProvider.getFullyQualifiedName(input)
				}

				override equals(Object object) {
					throw new UnsupportedOperationException("TODO: auto-generated method stub")
				}

			},
			IScope.NULLSCOPE
		)
	}
	
	def scope_AlarmBlock_alarm (AlarmBlock alarmBlock, EReference ref) {
		Scopes.scopeFor(
			getCandidateAlarms(alarmBlock.getContainerOfType(ControlNode).interfaceDescription),
			new Function<Alarm, QualifiedName> {

				override apply(Alarm input) {
					qualifiedNameProvider.getFullyQualifiedName(input)
				}

				override equals(Object object) {
					throw new UnsupportedOperationException("TODO: auto-generated method stub")
				}

			},
			IScope.NULLSCOPE
		)
	}

	def scope_CommandResponseBlock_triggeredEvents(CommandResponseBlock commandResponseBlock, EReference ref) {
		Scopes.scopeFor(
			getCandidateEvents(commandResponseBlock.getContainerOfType(ControlNode).interfaceDescription),
			new Function<Event, QualifiedName> {

				override apply(Event input) {
					qualifiedNameProvider.getFullyQualifiedName(input)
				}

				override equals(Object object) {
					throw new UnsupportedOperationException("TODO: auto-generated method stub")
				}

			},
			IScope.NULLSCOPE
		)
	}

	def scope_ParameterTranslation_inputParameters(ParameterTranslation parameterTranslation, EReference ref) {
		if (parameterTranslation.getContainerOfType(CommandTranslation) !== null) {
			Scopes.scopeFor(parameterTranslation.getContainerOfType(CommandResponseBlock).command.parameters) //Use Hyperlink to link to actual element
		} else {
			var List<Parameter> listOfParameters = newArrayList()
			for (response : parameterTranslation.getContainerOfType(ResponseTranslationRule).inputResponses) {
				listOfParameters.addAll(response.parameters)
			}
			Scopes.scopeFor(
				listOfParameters,
				new Function<Parameter, QualifiedName> {

					override apply(Parameter input) {
						qualifiedNameProvider.getFullyQualifiedName(input)
					}

					override equals(Object object) {
						throw new UnsupportedOperationException("TODO: auto-generated method stub")
					}

				},
				IScope.NULLSCOPE
			)
		}
	}

	def scope_ParameterTranslation_translatedParameters(ParameterTranslation parameterTranslation, EReference ref) {
		var List<Parameter> listOfParameters = newArrayList()
		if (parameterTranslation.getContainerOfType(CommandTranslation) !== null) {
			for (command : parameterTranslation.getContainerOfType(CommandTranslation).translatedCommands) {
				listOfParameters.addAll(command.parameters)
			}
			Scopes.scopeFor(
				listOfParameters,
				new Function<Parameter, QualifiedName> {

					override apply(Parameter input) {
						qualifiedNameProvider.getFullyQualifiedName(input)
					}

					override equals(Object object) {
						throw new UnsupportedOperationException("TODO: auto-generated method stub")
					}

				},
				IScope.NULLSCOPE
			)
		} else {
			Scopes.scopeFor(
				listOfParameters,
				new Function<Parameter, QualifiedName> {

					override apply(Parameter input) {
						qualifiedNameProvider.getFullyQualifiedName(input)
					}

					override equals(Object object) {
						throw new UnsupportedOperationException("TODO: auto-generated method stub")
					}

				},
				IScope.NULLSCOPE
			)

		}
	}

	def scope_CommandDistribution_command(CommandDistribution oommandDistribution, EReference ref) {
		Scopes.scopeFor(
			oommandDistribution.getContainerOfType(CommandResponseBlock).commandTranslation.translatedCommands,
			new Function<Command, QualifiedName> {

				override apply(Command input) {
					qualifiedNameProvider.getFullyQualifiedName(input)
				}

				override equals(Object object) {
					throw new UnsupportedOperationException("TODO: auto-generated method stub")
				}

			},
			IScope.NULLSCOPE
		)
	}

	def scope_CommandDistribution_destinationNodes(CommandDistribution commandDistribution, EReference ref) {
		Scopes.scopeFor(commandDistribution.getContainerOfType(ControlNode).childNodes)
	}

	def scope_ResponseTranslationRule_inputResponses(ResponseTranslationRule responseTranslationRule, EReference ref) {
		Scopes.scopeFor(
			getCandidateResponsesFromChildNode(responseTranslationRule.getContainerOfType(ControlNode)),
			new Function<Response, QualifiedName> {

				override apply(Response input) {
					qualifiedNameProvider.getFullyQualifiedName(input)
				}

				override equals(Object object) {
					throw new UnsupportedOperationException("TODO: auto-generated method stub")
				}

			},
			IScope.NULLSCOPE
		)
	}
	
	def scope_ResponsibleItemList_responsibleCommands(ResponsibleItemList responsibleItemList, EReference ref){
		Scopes.scopeFor(getCandidateCommands(responsibleItemList.getContainerOfType(ControlNode)),
			new Function<Command, QualifiedName> {
 
				override apply(Command input) {
					qualifiedNameProvider.getFullyQualifiedName(input)
				} 

				override equals(Object object) {
					throw new UnsupportedOperationException("TODO: auto-generated method stub")
				}

			},
			IScope.NULLSCOPE
		)
	}
	
	def scope_ResponsibleItemList_responsibleAlarms(ResponsibleItemList responsibleItemList, EReference ref){
		var controlNode = responsibleItemList.getContainerOfType(ControlNode)
		var List<Alarm> listOfAlarms = getCandidateAlarms(controlNode.interfaceDescription)
		if(responsibleItemList.getContainerOfType(AlarmBlock)!==null)
		{
			for(childControlNode: responsibleItemList.getContainerOfType(ControlNode).childNodes)
				listOfAlarms.addAll(getCandidateAlarms(childControlNode.interfaceDescription))
		}
			Scopes.scopeFor(listOfAlarms,
			new Function<Alarm, QualifiedName> {
 
				override apply(Alarm input) {
					qualifiedNameProvider.getFullyQualifiedName(input)
				} 

				override equals(Object object) {
					throw new UnsupportedOperationException("TODO: auto-generated method stub")
				}

			},
			IScope.NULLSCOPE
		)
		
		
	}
	
	def scope_ResponsibleItemList_responsibleEvents(ResponsibleItemList responsibleItemList, EReference ref){
		var List<Event> listOfEvents = getCandidateEvents(responsibleItemList.getContainerOfType(ControlNode).interfaceDescription)
		if(responsibleItemList.getContainerOfType(EventBlock)!==null)
		{
			for(childControlNode: responsibleItemList.getContainerOfType(ControlNode).childNodes)
				listOfEvents.addAll(getCandidateEvents(childControlNode.interfaceDescription))
		}
		
		Scopes.scopeFor(listOfEvents,
			new Function<Event, QualifiedName> {
 
				override apply(Event input) {
					qualifiedNameProvider.getFullyQualifiedName(input)
				} 

				override equals(Object object) {
					throw new UnsupportedOperationException("TODO: auto-generated method stub")
				}

			},
			IScope.NULLSCOPE
		)
	}
	
	def scope_ResponsibleItemList_responsibleDataPoints(ResponsibleItemList responsibleItemList, EReference ref){
		var List<DataPoint> listOfDatapoints = getCandidateDataPoints(responsibleItemList.getContainerOfType(ControlNode).interfaceDescription)
		if(responsibleItemList.getContainerOfType(DataPointBlock)!==null)
		{
			for(ccn: responsibleItemList.getContainerOfType(ControlNode).childNodes)
				listOfDatapoints.addAll(getCandidateDataPoints(ccn.interfaceDescription))
		}
		Scopes.scopeFor(listOfDatapoints,
			new Function<DataPoint, QualifiedName> {
 
				override apply(DataPoint input) {
					qualifiedNameProvider.getFullyQualifiedName(input)
				} 

				override equals(Object object) {
					throw new UnsupportedOperationException("TODO: auto-generated method stub")
				}

			},
			IScope.NULLSCOPE
		)
	}
	
}
