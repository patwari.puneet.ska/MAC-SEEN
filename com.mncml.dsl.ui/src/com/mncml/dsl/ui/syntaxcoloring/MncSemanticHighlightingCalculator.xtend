package com.mncml.dsl.ui.syntaxcoloring

import java.util.List
import mncModel.CommandResponseBlock
import org.eclipse.xtext.CrossReference
import org.eclipse.xtext.nodemodel.util.NodeModelUtils
import org.eclipse.xtext.resource.XtextResource
import org.eclipse.xtext.ui.editor.syntaxcoloring.DefaultSemanticHighlightingCalculator
import org.eclipse.xtext.ui.editor.syntaxcoloring.IHighlightedPositionAcceptor
import org.eclipse.xtext.ui.editor.syntaxcoloring.ISemanticHighlightingCalculator

class MncSemanticHighlightingCalculator extends DefaultSemanticHighlightingCalculator implements ISemanticHighlightingCalculator {
	
	override provideHighlightingFor(XtextResource resource, IHighlightedPositionAcceptor acceptor) {	
		if (resource == null || resource.getParseResult() == null)
    		return;
    	var List<CommandResponseBlock> listOfCommandResponseBlocks = resource.allContents.filter(CommandResponseBlock).toList
		for(commandResponseBlock : listOfCommandResponseBlocks){
			var compositeNode = NodeModelUtils.getNode(commandResponseBlock)
			for(node : compositeNode.asTreeIterable)
			{
				if(node.grammarElement instanceof CrossReference && node.semanticElement instanceof CommandResponseBlock) //n.semanticElemnt is a better way (15/12/15)
				{ 
					highlightNode(acceptor, node, MncHighlightingConfiguration.CRB_COMMAND )
				}
			
			}
			
		}
	}
	
}