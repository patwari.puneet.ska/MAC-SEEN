
package com.mncml.dsl.ui;

import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.eclipse.xtext.ui.editor.hyperlinking.IHyperlinkHelper;
import org.eclipse.xtext.ui.editor.syntaxcoloring.IHighlightingConfiguration;
import org.eclipse.xtext.ui.editor.syntaxcoloring.ISemanticHighlightingCalculator;

import com.mncml.dsl.ui.hyperlink.MncHyperlinkHelper;
import com.mncml.dsl.ui.syntaxcoloring.MncHighlightingConfiguration;
import com.mncml.dsl.ui.syntaxcoloring.MncSemanticHighlightingCalculator;

/**
 * Use this class to register components to be used within the IDE.
 */
@SuppressWarnings("deprecation")
public class MncUiModule extends com.mncml.dsl.ui.AbstractMncUiModule {
	public MncUiModule(AbstractUIPlugin plugin) {
		super(plugin);
	}
	
	public Class<? extends ISemanticHighlightingCalculator> bindISemanticHighlightingCalculator(){
		return MncSemanticHighlightingCalculator.class;
	}
	
	public Class<? extends IHighlightingConfiguration> bindIHighlightingConfiguration(){
		return MncHighlightingConfiguration.class;
	}
	
	@Override
	public Class<? extends IHyperlinkHelper> bindIHyperlinkHelper() {
		return MncHyperlinkHelper.class;
	}
}
