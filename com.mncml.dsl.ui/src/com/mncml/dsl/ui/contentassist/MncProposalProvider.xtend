package com.mncml.dsl.ui.contentassist

import com.google.common.base.Function
import com.google.inject.Inject
import java.util.List
import mncModel.Action
import mncModel.Alarm
import mncModel.AlarmBlock
import mncModel.AlarmTriggerCondition
import mncModel.CheckParameterCondition
import mncModel.Command
import mncModel.CommandResponseBlock
import mncModel.CommandTriggerCondition
import mncModel.CommandValidation
import mncModel.ControlNode
import mncModel.DataPoint
import mncModel.DataPointTriggerCondition
import mncModel.Event
import mncModel.EventBlock
import mncModel.EventTriggerCondition
import mncModel.InterfaceDescription
import mncModel.MncModelFactory
import mncModel.OperatingState
import mncModel.Operation
import mncModel.Parameter
import mncModel.ResponseBlock
import mncModel.ResponseTranslationRule
import mncModel.ResponsibleItemList
import mncModel.SimpleType
import mncModel.utility.StateUtility
import org.eclipse.emf.common.ui.dialogs.ResourceDialog
import org.eclipse.emf.ecore.EObject
import org.eclipse.jface.viewers.StyledString
import org.eclipse.jface.viewers.StyledString.Styler
import org.eclipse.swt.SWT
import org.eclipse.swt.widgets.Display
import org.eclipse.ui.PlatformUI
import org.eclipse.xtext.Assignment
import org.eclipse.xtext.EcoreUtil2
import org.eclipse.xtext.Keyword
import org.eclipse.xtext.naming.IQualifiedNameProvider
import org.eclipse.xtext.naming.QualifiedName
import org.eclipse.xtext.resource.XtextResource
import org.eclipse.xtext.ui.editor.contentassist.ContentAssistContext
import org.eclipse.xtext.ui.editor.contentassist.ICompletionProposalAcceptor
import org.eclipse.xtext.ui.editor.utils.EditorUtils
import org.eclipse.xtext.util.concurrent.IUnitOfWork

import static extension org.eclipse.xtext.EcoreUtil2.*

class MncProposalProvider extends AbstractMncProposalProvider {

	@Inject
	IQualifiedNameProvider qualifiedNameProvider

	public static val INT = "int"
	public static val STRING = "string"
	public static val BOOLEAN = "boolean"
	public static val FLOAT = "float"

	override completeModel_Name(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		var string = "Enter Model Name"
		var styledString = new StyledString
		var Styler styler = StyledString.QUALIFIER_STYLER
		styledString.append(string, styler)
		var completionProposal = createCompletionProposal("", styledString, null, context);
		acceptor.accept(completionProposal);
	}

	override completeCommand_Name(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		var string = "Enter Command Name"
		var styledString = new StyledString
		var Styler styler = StyledString.QUALIFIER_STYLER
		styledString.append(string, styler)
		var completionProposal = createCompletionProposal("", styledString, null, context);
		acceptor.accept(completionProposal);
	}

	override completeAlarm_Name(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		var string = "Enter Alarm Name"
		var styledString = new StyledString
		var Styler styler = StyledString.QUALIFIER_STYLER
		styledString.append(string, styler)
		var completionProposal = createCompletionProposal("", styledString, null, context);
		acceptor.accept(completionProposal);
	}

	override completeEvent_Name(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		var string = "Enter Event Name"
		var styledString = new StyledString
		var Styler styler = StyledString.QUALIFIER_STYLER
		styledString.append(string, styler)
		var completionProposal = createCompletionProposal("", styledString, null, context);
		acceptor.accept(completionProposal);
	}

	override completeDataPoint_Name(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		var string = "Enter DataPoint Name"
		var styledString = new StyledString
		var Styler styler = StyledString.QUALIFIER_STYLER
		styledString.append(string, styler)
		var completionProposal = createCompletionProposal("", styledString, null, context);
		acceptor.accept(completionProposal);
	}

	override completeInterfaceDescription_Name(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		var string = "Enter InterfaceDescription Name"
		var styledString = new StyledString
		var Styler styler = StyledString.QUALIFIER_STYLER
		styledString.append(string, styler)
		var completionProposal = createCompletionProposal("", styledString, null, context);
		acceptor.accept(completionProposal);
	}

	override completeSimpleType_Name(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		var string = "Enter Parameter Name"
		var styledString = new StyledString
		var Styler styler = StyledString.QUALIFIER_STYLER
		styledString.append(string, styler)
		var completionProposal = createCompletionProposal("", styledString, null, context);
		acceptor.accept(completionProposal)
	}

	override completeStateUtility_OperatingStates(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		var string = "Enter State Name"
		var styledString = new StyledString
		var Styler styler = StyledString.QUALIFIER_STYLER
		styledString.append(string, styler)
		var completionProposal = createCompletionProposal("", styledString, null, context);
		acceptor.accept(completionProposal)
	}

	override completeResponse_Name(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		var string = "Enter Response Name"
		var styledString = new StyledString
		var Styler styler = StyledString.QUALIFIER_STYLER
		styledString.append(string, styler)
		var completionProposal = createCompletionProposal("", styledString, null, context);
		acceptor.accept(completionProposal)
	}

	override completeControlNode_Name(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		var string = "Enter Control Node Name"
		var styledString = new StyledString
		var Styler styler = StyledString.QUALIFIER_STYLER
		styledString.append(string, styler)
		var completionProposal = createCompletionProposal("", styledString, null, context);
		acceptor.accept(completionProposal)
	}

	override completeSimpleType_Value(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		if (!(model instanceof SimpleType))
			return;

		val parameter = model as SimpleType
		var type = parameter.type.getName
		switch (type) {
			case INT: acceptor.accept(createCompletionProposal("", "Enter Integer Value", null, context))
			case BOOLEAN: acceptor.accept(createCompletionProposal("", "Enter 'true' or 'false'", null, context))
			case FLOAT: acceptor.accept(createCompletionProposal("", "Enter Float Value", null, context))
			case STRING: acceptor.accept(createCompletionProposal("", "Enter String", null, context))
		}
	}

	override completeDataPoint_Value(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		if (!(model instanceof DataPoint))
			return;

		val dataPoint = model as DataPoint
		var type = dataPoint.type.getName
		switch (type) {
			case INT: acceptor.accept(createCompletionProposal("", "Enter Integer Value", null, context))
			case BOOLEAN: acceptor.accept(createCompletionProposal("", "Enter 'true' or 'false'", null, context))
			case FLOAT: acceptor.accept(createCompletionProposal("", "Enter Float Value", null, context))
			case STRING: acceptor.accept(createCompletionProposal("", "Enter String", null, context))
		}
	}

	override completeKeyword(Keyword keyword, ContentAssistContext contentAssistContext,
		ICompletionProposalAcceptor acceptor) {
		super.completeKeyword(keyword, contentAssistContext, acceptor)
	}

	def getCandidateOperatingStates(InterfaceDescription interfaceDescription) {
		var List<OperatingState> listOfOperatingStates = newArrayList()
		if (interfaceDescription.operatingStates != null)
			listOfOperatingStates.addAll(interfaceDescription.operatingStates.operatingStates)
		for (interfaceDescriptionInUses : interfaceDescription.uses)
			listOfOperatingStates.addAll(interfaceDescriptionInUses.operatingStates.operatingStates)
		listOfOperatingStates
	}

	def getCandidateAlarms(InterfaceDescription interfaceDescription) {
		var List<Alarm> listOfAlarms = newArrayList()
		if (interfaceDescription.alarms != null)
			listOfAlarms.addAll(interfaceDescription.alarms.alarms)
		for (interfaceDescriptionInUses : interfaceDescription.uses)
			if (interfaceDescriptionInUses.alarms != null)
				listOfAlarms.addAll(interfaceDescriptionInUses.alarms.alarms)
		listOfAlarms
	}

	override completeCommandResponseBlock_Command(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		var controlNode = EcoreUtil2.getContainerOfType(model, ControlNode)
		var interfaceDescription = controlNode.interfaceDescription
		var List<Command> listOfCommandsInCommandBlock = newArrayList()
		for (commRespBlock : controlNode.commandResponseBlocks.commandResponseBlocks) {
			listOfCommandsInCommandBlock.add(commRespBlock.command)
		}
		if (interfaceDescription.commands != null) {
			for (command : interfaceDescription.commands.commands) {
				if (!listOfCommandsInCommandBlock.contains(command)) {
					var proposal = new Function<Command, QualifiedName> {

						override apply(Command input) {
							qualifiedNameProvider.getFullyQualifiedName(input)
						}

						override equals(Object object) {
							throw new UnsupportedOperationException("TODO: auto-generated method stub")
						}

					}.apply(command).toString

					var completionProposal = createCompletionProposal(proposal, context);
					acceptor.accept(completionProposal);

				}

			}

			for (interfaceDescriptionInUses : interfaceDescription.uses) {
				for (command : interfaceDescriptionInUses.commands.commands) {

					if (!listOfCommandsInCommandBlock.contains(interfaceDescriptionInUses)) {
						var proposal = new Function<Command, QualifiedName> {

							override apply(Command input) {
								qualifiedNameProvider.getFullyQualifiedName(input)
							}

							override equals(Object object) {
								throw new UnsupportedOperationException("TODO: auto-generated method stub")
							}

						}.apply(command).toString

						var completionProposal = createCompletionProposal(proposal, context);
						acceptor.accept(completionProposal);
					}

				}
			}
		}
	}

	override public void completeAlarmBlock_Alarm(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		var controlNode = EcoreUtil2.getContainerOfType(model, ControlNode)
		var interfaceDescription = controlNode.interfaceDescription
		var List<Alarm> listOfAlarmsInAlarmBlock = newArrayList()
		for (alarmBlock : controlNode.alarmBlocks.alarmBlocks) {
			listOfAlarmsInAlarmBlock.add(alarmBlock.alarm)
		}
		if (interfaceDescription.alarms != null) {
			for (alarm : interfaceDescription.alarms.alarms) {
				if (!listOfAlarmsInAlarmBlock.contains(alarm)) {
					var proposal = new Function<Alarm, QualifiedName> {

						override apply(Alarm input) {
							qualifiedNameProvider.getFullyQualifiedName(input)
						}

						override equals(Object object) {
							throw new UnsupportedOperationException("TODO: auto-generated method stub")
						}

					}.apply(alarm).toString

					var completionProposal = createCompletionProposal(proposal, context);
					acceptor.accept(completionProposal);

					}
					
				}
			}
			
			if (interfaceDescription.subscribedItems !== null 
				&& interfaceDescription.subscribedItems.subscribedAlarms !== null) {
			for (alarm : interfaceDescription.subscribedItems.subscribedAlarms) {
				if (!listOfAlarmsInAlarmBlock.contains(alarm)) {
					var proposal = new Function<Alarm, QualifiedName> {

						override apply(Alarm input) {
							qualifiedNameProvider.getFullyQualifiedName(input)
						}

						override equals(Object object) {
							throw new UnsupportedOperationException("TODO: auto-generated method stub")
						}

					}.apply(alarm).toString

					var completionProposal = createCompletionProposal(proposal, context);
					acceptor.accept(completionProposal);

					}

				}
			
			}

			for (interfaceDescriptionInUses : interfaceDescription.uses) {
				for (alarm : interfaceDescriptionInUses.alarms.alarms) {

					if (!listOfAlarmsInAlarmBlock.contains(interfaceDescriptionInUses)) {
						var proposal = new Function<Alarm, QualifiedName> {

							override apply(Alarm input) {
								qualifiedNameProvider.getFullyQualifiedName(input)
							}

							override equals(Object object) {
								throw new UnsupportedOperationException("TODO: auto-generated method stub")
							}

						}.apply(alarm).toString

						var completionProposal = createCompletionProposal(proposal, context);
						acceptor.accept(completionProposal);
					}

				}
			}
	}

	override public void completeDataPointBlock_DataPoint(EObject model, Assignment assignment,
		ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		var controlNode = EcoreUtil2.getContainerOfType(model, ControlNode)
		var interfaceDescription = controlNode.interfaceDescription
		var List<DataPoint> listOfDatapointsInDatapointBlock = newArrayList()
		for (dataPointBlock : controlNode.dataPointBlocks.dataPointBlocks) {
			listOfDatapointsInDatapointBlock.add(dataPointBlock.dataPoint)
		}
		if (interfaceDescription.dataPoints != null) {
			for (dataPoint : interfaceDescription.dataPoints.dataPoints) {
				if (!listOfDatapointsInDatapointBlock.contains(dataPoint)) {
					var proposal = new Function<DataPoint, QualifiedName> {

						override apply(DataPoint input) {
							qualifiedNameProvider.getFullyQualifiedName(input)
						}

						override equals(Object object) {
							throw new UnsupportedOperationException("TODO: auto-generated method stub")
						}

					}.apply(dataPoint).toString

					var completionProposal = createCompletionProposal(proposal, context);
					acceptor.accept(completionProposal);

					}
					
				}
	
			}
			
			if (interfaceDescription.subscribedItems !== null 
				&& interfaceDescription.subscribedItems.subscribedDataPoints !== null) {
			for (dataPoint : interfaceDescription.subscribedItems.subscribedDataPoints) {
				if (!listOfDatapointsInDatapointBlock.contains(dataPoint)) {
					var proposal = new Function<DataPoint, QualifiedName> {

						override apply(DataPoint input) {
							qualifiedNameProvider.getFullyQualifiedName(input)
						}

						override equals(Object object) {
							throw new UnsupportedOperationException("TODO: auto-generated method stub")
						}

					}.apply(dataPoint).toString

					var completionProposal = createCompletionProposal(proposal, context);
					acceptor.accept(completionProposal);

					}
				}
			}

			for (interfaceDescriptionInUses : interfaceDescription.uses) {
				for (dataPoint : interfaceDescriptionInUses.dataPoints.dataPoints) {

					if (!listOfDatapointsInDatapointBlock.contains(interfaceDescriptionInUses)) {
						var proposal = new Function<DataPoint, QualifiedName> {

							override apply(DataPoint input) {
								qualifiedNameProvider.getFullyQualifiedName(input)
							}

							override equals(Object object) {
								throw new UnsupportedOperationException("TODO: auto-generated method stub")
							}

						}.apply(dataPoint).toString

						var completionProposal = createCompletionProposal(proposal, context);
						acceptor.accept(completionProposal);
					}

				}
			}
		
	}

	override public void completeEventBlock_Event(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		var controlNode = EcoreUtil2.getContainerOfType(model, ControlNode)
		var interfaceDescription = controlNode.interfaceDescription
		var List<Event> listOfEventsInEventBlock = newArrayList()
		for (eventBlock : controlNode.eventBlocks.eventBlocks) {
			listOfEventsInEventBlock.add(eventBlock.event)
		}
		if (interfaceDescription.events != null) {
			for (event : interfaceDescription.events.events) {
				if (!listOfEventsInEventBlock.contains(event)) {
					var proposal = new Function<Event, QualifiedName> {

						override apply(Event input) {
							qualifiedNameProvider.getFullyQualifiedName(input)
						}

						override equals(Object object) {
							throw new UnsupportedOperationException("TODO: auto-generated method stub")
						}

					}.apply(event).toString

					var completionProposal = createCompletionProposal(proposal, context);
					acceptor.accept(completionProposal);

					}

				}
			}
			
			if (interfaceDescription.subscribedItems !== null 
				&& interfaceDescription.subscribedItems.subscribedEvents !== null) {
			for (event : interfaceDescription.subscribedItems.subscribedEvents) {
				if (!listOfEventsInEventBlock.contains(event)) {
					var proposal = new Function<Event, QualifiedName> {

						override apply(Event input) {
							qualifiedNameProvider.getFullyQualifiedName(input)
						}

						override equals(Object object) {
							throw new UnsupportedOperationException("TODO: auto-generated method stub")
						}

					}.apply(event).toString

					var completionProposal = createCompletionProposal(proposal, context);
					acceptor.accept(completionProposal);

					}

				}
			
			}

			for (interfaceDescriptionInUses : interfaceDescription.uses) {
				for (event : interfaceDescriptionInUses.events.events) {

					if (!listOfEventsInEventBlock.contains(interfaceDescriptionInUses)) {
						var proposal = new Function<Event, QualifiedName> {

							override apply(Event input) {
								qualifiedNameProvider.getFullyQualifiedName(input)
							}

							override equals(Object object) {
								throw new UnsupportedOperationException("TODO: auto-generated method stub")
							}

						}.apply(event).toString

						var completionProposal = createCompletionProposal(proposal, context);
						acceptor.accept(completionProposal);
				}

			}
		}
	}

	override completeAction_Alarm(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		var action = model as Action
		var alarmBlock = EcoreUtil2.getContainerOfType(action, AlarmBlock)
		var interfaceDescription = EcoreUtil2.getContainerOfType(action, ControlNode).interfaceDescription
		var actionAlarm = action.alarm
		var List<Alarm> listOfAlarms = newArrayList()
		listOfAlarms.addAll(interfaceDescription.alarms.alarms)
		listOfAlarms.removeAll(actionAlarm)
		if (alarmBlock != null) {
			listOfAlarms.remove(alarmBlock.alarm)
		}
		for (alarm : listOfAlarms) {
			var proposal = new Function<Alarm, QualifiedName> {

				override apply(Alarm input) {
					qualifiedNameProvider.getFullyQualifiedName(input)
				}

				override equals(Object object) {
					throw new UnsupportedOperationException("TODO: auto-generated method stub")
				}

			}.apply(alarm).toString
			var completionProposal = createCompletionProposal(proposal, context);
			acceptor.accept(completionProposal);
		}

	}

	override completeAction_Command(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		var action = model as Action
		var ifCommRespBlock = EcoreUtil2.getContainerOfType(action, CommandResponseBlock)
		var controlNode = EcoreUtil2.getContainerOfType(action, ControlNode)
		var interfaceDescription = controlNode.interfaceDescription
		var actionCommand = action.command
		var List<Command> listOfCommands = newArrayList()
		listOfCommands.addAll(interfaceDescription.commands.commands)
		for (childNode : controlNode.childNodes) {
			listOfCommands.addAll(childNode.interfaceDescription.commands.commands)
		}
		listOfCommands.removeAll(actionCommand)
		if (ifCommRespBlock != null) {
			listOfCommands.remove(ifCommRespBlock.command)
		}
		for (command : listOfCommands) {
			var proposal = new Function<Command, QualifiedName> {

				override apply(Command input) {
					qualifiedNameProvider.getFullyQualifiedName(input)
				}

				override equals(Object object) {
					return false;
				}

			}.apply(command).toString
			var completionProposal = createCompletionProposal(proposal, context);
			acceptor.accept(completionProposal);
		}

	}

	override completeAction_Event(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		var action = model as Action
		var eventBlock = EcoreUtil2.getContainerOfType(action, EventBlock)
		var interfaceDescription = EcoreUtil2.getContainerOfType(action, ControlNode).interfaceDescription
		var actionEvent = action.event
		var List<Event> listOfEvents = newArrayList()
		listOfEvents.addAll(interfaceDescription.events.events)

		listOfEvents.removeAll(actionEvent)
		if (eventBlock != null) {
			listOfEvents.remove(eventBlock.event)
		}
		for (event : listOfEvents) {
			var proposal = new Function<Event, QualifiedName> {

				override apply(Event input) {
					qualifiedNameProvider.getFullyQualifiedName(input)
				}

				override equals(Object object) {
					throw new UnsupportedOperationException("TODO: auto-generated method stub")
				}

			}.apply(event).toString
			var completionProposal = createCompletionProposal(proposal, context);
			acceptor.accept(completionProposal);
		}

	}

	// Validation Left
	override completeStateUtility_EndState(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		if (!(model instanceof StateUtility))
			return
		var stateUtility = model as StateUtility
		var List<OperatingState> istOfOperatingStates = getCandidateOperatingStates(
			stateUtility.getContainerOfType(InterfaceDescription))
		if (stateUtility.startState != null)
			istOfOperatingStates.remove(stateUtility.startState)
		istOfOperatingStates.forEach [
			acceptor.accept(createCompletionProposal(it.name, context))
		]
	}

	// Validation Left
	override completeResponsibleItemList_ResponsibleAlarms(EObject model, Assignment assignment,
		ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		if (!(model instanceof ResponsibleItemList))
			return
		var responsibleItemList = model as ResponsibleItemList
		if (model.getContainerOfType(AlarmBlock) != null) {
			var List<Alarm> listOfAlarms = getCandidateAlarms(
				responsibleItemList.getContainerOfType(ControlNode).interfaceDescription)
			for (childControlNode : responsibleItemList.getContainerOfType(ControlNode).childNodes)
				listOfAlarms.addAll(getCandidateAlarms(childControlNode.interfaceDescription))
			listOfAlarms.remove(responsibleItemList.getContainerOfType(AlarmBlock).alarm)
			listOfAlarms.forEach [
				var proposal = new Function<Alarm, QualifiedName> {

					override apply(Alarm input) {
						qualifiedNameProvider.getFullyQualifiedName(input)
					}

					override equals(Object object) {
						throw new UnsupportedOperationException("TODO: auto-generated method stub")
					}

				}.apply(it).toString
				acceptor.accept(createCompletionProposal(proposal, context))
			]
		}
	}

	override completeStateUtility_StartState(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		if (!(model instanceof StateUtility))
			return
		var stateUtility = model as StateUtility
		var List<OperatingState> listOfOperatingStates = getCandidateOperatingStates(
			stateUtility.getContainerOfType(InterfaceDescription))
		if (stateUtility.endState != null)
			listOfOperatingStates.remove(stateUtility.endState)
		listOfOperatingStates.forEach [
			acceptor.accept(createCompletionProposal(it.name, context))
		]
	}

	override completeOperation_Script(EObject model, Assignment assignment, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {

		if (!(model instanceof Operation))
			return
		val resourceDialog = new ResourceDialog(PlatformUI.getWorkbench.activeWorkbenchWindow.shell, "Choose File",
			SWT.NONE)
		resourceDialog.open
		val operation = model as Operation
		val xtextDocument = EditorUtils.getActiveXtextEditor.document
		var currentUiThread = Display.getCurrent()
		currentUiThread.asyncExec(
			new Runnable() {
				override run() {
					xtextDocument.modify(new IUnitOfWork.Void<XtextResource> {
						override process(XtextResource state) throws Exception 
						{
							var newOperation = MncModelFactory.eINSTANCE.createOperation => [
								name = operation.name
								inputParameters.addAll(operation.inputParameters)
								outputParameters.addAll(operation.outputParameters)
								script = resourceDialog.URIText
							]
							var eParent = operation.eContainer
							switch (eParent) {
								CommandValidation: {
									(eParent as CommandValidation).operation = newOperation
								}
								CommandTriggerCondition: {
									(eParent as CommandTriggerCondition).operation = newOperation
								}
								EventTriggerCondition: {
									(eParent as EventTriggerCondition).operation = newOperation
								}
								AlarmTriggerCondition: {
									(eParent as AlarmTriggerCondition).operation = newOperation
								}
								DataPointTriggerCondition: {
									(eParent as DataPointTriggerCondition).operation = newOperation
								}
								ResponseTranslationRule: {
									(eParent as ResponseTranslationRule).operation = newOperation
								}
								Action: {
									(eParent as Action).operation = newOperation
								}
							}
						}

					}) // modify closure
				} // run() closure
			}
		) // asyncExec closure
	}

	override completeCheckParameterCondition_Parameter(EObject model, Assignment assignment,
		ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		var validation = model as CheckParameterCondition
		var responseBlock = EcoreUtil2.getContainerOfType(validation, ResponseBlock)
		if (responseBlock != null && responseBlock instanceof ResponseBlock) {
			var interfaceDescription = EcoreUtil2.getContainerOfType(validation, ControlNode).interfaceDescription
			var responseValidationRules = responseBlock.responseValidation.validationRules
			var List<Parameter> responseValidationParameters = newArrayList()
			for (respValidRule : responseValidationRules) {
				responseValidationParameters.add(respValidRule.parameter)
			}
			var List<Parameter> listOfParameters = newArrayList()
			for (response : interfaceDescription.responses.responses) {
				if (response.equals(responseBlock.response)) {
				}

			}

			listOfParameters.removeAll(responseValidationParameters)
			for (parameter : listOfParameters) {
				var proposal = new Function<Parameter, QualifiedName> {

					override apply(Parameter input) {
						qualifiedNameProvider.getFullyQualifiedName(input)
					}

					override equals(Object object) {
						throw new UnsupportedOperationException("TODO: auto-generated method stub")
					}

				}.apply(parameter).toString
				var completionProposal = createCompletionProposal(proposal, context);
				acceptor.accept(completionProposal);
			}
		} else {
			var commandBlock = EcoreUtil2.getContainerOfType(model, CommandResponseBlock)
			var interfaceDescription = EcoreUtil2.getContainerOfType(validation, ControlNode).interfaceDescription
			var commandValidationRules = commandBlock.commandValidation.validationRules
			var List<Parameter> commandValidationParameters = newArrayList()
			for (commValidRule : commandValidationRules) {
				commandValidationParameters.add(commValidRule.parameter)
			}
			var List<Parameter> listOfParameters = newArrayList()
			for (command : interfaceDescription.commands.commands) {
				if (command.equals(commandBlock.command)) {
					listOfParameters.addAll(command.parameters)
				}

			}
			listOfParameters.removeAll(commandValidationParameters)
			for (parameter : listOfParameters) {
				var proposal = new Function<Parameter, QualifiedName> {

					override apply(Parameter input) {
						qualifiedNameProvider.getFullyQualifiedName(input)
					}

					override equals(Object object) {
						throw new UnsupportedOperationException("TODO: auto-generated method stub")
					}

				}.apply(parameter).toString
				var completionProposal = createCompletionProposal(proposal, context);
				acceptor.accept(completionProposal);
			}

		}

	}

}
