/**
 */
package mncModel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Point Block</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mncModel.DataPointBlock#getDataPoint <em>Data Point</em>}</li>
 *   <li>{@link mncModel.DataPointBlock#getDataPointCondition <em>Data Point Condition</em>}</li>
 *   <li>{@link mncModel.DataPointBlock#getDataPointHandling <em>Data Point Handling</em>}</li>
 * </ul>
 *
 * @see mncModel.MncModelPackage#getDataPointBlock()
 * @model
 * @generated
 */
public interface DataPointBlock extends EObject {
	/**
	 * Returns the value of the '<em><b>Data Point</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Point</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Point</em>' reference.
	 * @see #setDataPoint(DataPoint)
	 * @see mncModel.MncModelPackage#getDataPointBlock_DataPoint()
	 * @model
	 * @generated
	 */
	DataPoint getDataPoint();

	/**
	 * Sets the value of the '{@link mncModel.DataPointBlock#getDataPoint <em>Data Point</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Point</em>' reference.
	 * @see #getDataPoint()
	 * @generated
	 */
	void setDataPoint(DataPoint value);

	/**
	 * Returns the value of the '<em><b>Data Point Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Point Condition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Point Condition</em>' containment reference.
	 * @see #setDataPointCondition(DataPointTriggerCondition)
	 * @see mncModel.MncModelPackage#getDataPointBlock_DataPointCondition()
	 * @model containment="true"
	 * @generated
	 */
	DataPointTriggerCondition getDataPointCondition();

	/**
	 * Sets the value of the '{@link mncModel.DataPointBlock#getDataPointCondition <em>Data Point Condition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Point Condition</em>' containment reference.
	 * @see #getDataPointCondition()
	 * @generated
	 */
	void setDataPointCondition(DataPointTriggerCondition value);

	/**
	 * Returns the value of the '<em><b>Data Point Handling</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Point Handling</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Point Handling</em>' containment reference.
	 * @see #setDataPointHandling(DataPointHandling)
	 * @see mncModel.MncModelPackage#getDataPointBlock_DataPointHandling()
	 * @model containment="true"
	 * @generated
	 */
	DataPointHandling getDataPointHandling();

	/**
	 * Sets the value of the '{@link mncModel.DataPointBlock#getDataPointHandling <em>Data Point Handling</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Point Handling</em>' containment reference.
	 * @see #getDataPointHandling()
	 * @generated
	 */
	void setDataPointHandling(DataPointHandling value);

} // DataPointBlock
