/**
 */
package mncModel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Point Handling</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mncModel.DataPointHandling#getCheckDataPoint <em>Check Data Point</em>}</li>
 *   <li>{@link mncModel.DataPointHandling#getAction <em>Action</em>}</li>
 * </ul>
 *
 * @see mncModel.MncModelPackage#getDataPointHandling()
 * @model
 * @generated
 */
public interface DataPointHandling extends EObject {
	/**
	 * Returns the value of the '<em><b>Check Data Point</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Check Data Point</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Check Data Point</em>' containment reference.
	 * @see #setCheckDataPoint(DataPointValidCondition)
	 * @see mncModel.MncModelPackage#getDataPointHandling_CheckDataPoint()
	 * @model containment="true"
	 * @generated
	 */
	DataPointValidCondition getCheckDataPoint();

	/**
	 * Sets the value of the '{@link mncModel.DataPointHandling#getCheckDataPoint <em>Check Data Point</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Check Data Point</em>' containment reference.
	 * @see #getCheckDataPoint()
	 * @generated
	 */
	void setCheckDataPoint(DataPointValidCondition value);

	/**
	 * Returns the value of the '<em><b>Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Action</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Action</em>' containment reference.
	 * @see #setAction(Action)
	 * @see mncModel.MncModelPackage#getDataPointHandling_Action()
	 * @model containment="true"
	 * @generated
	 */
	Action getAction();

	/**
	 * Sets the value of the '{@link mncModel.DataPointHandling#getAction <em>Action</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Action</em>' containment reference.
	 * @see #getAction()
	 * @generated
	 */
	void setAction(Action value);

} // DataPointHandling
