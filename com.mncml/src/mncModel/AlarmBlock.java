/**
 */
package mncModel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Alarm Block</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mncModel.AlarmBlock#getAlarm <em>Alarm</em>}</li>
 *   <li>{@link mncModel.AlarmBlock#getAlarmCondition <em>Alarm Condition</em>}</li>
 *   <li>{@link mncModel.AlarmBlock#getAlarmHandling <em>Alarm Handling</em>}</li>
 * </ul>
 *
 * @see mncModel.MncModelPackage#getAlarmBlock()
 * @model
 * @generated
 */
public interface AlarmBlock extends EObject {
	/**
	 * Returns the value of the '<em><b>Alarm</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Alarm</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Alarm</em>' reference.
	 * @see #setAlarm(Alarm)
	 * @see mncModel.MncModelPackage#getAlarmBlock_Alarm()
	 * @model
	 * @generated
	 */
	Alarm getAlarm();

	/**
	 * Sets the value of the '{@link mncModel.AlarmBlock#getAlarm <em>Alarm</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Alarm</em>' reference.
	 * @see #getAlarm()
	 * @generated
	 */
	void setAlarm(Alarm value);

	/**
	 * Returns the value of the '<em><b>Alarm Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Alarm Condition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Alarm Condition</em>' containment reference.
	 * @see #setAlarmCondition(AlarmTriggerCondition)
	 * @see mncModel.MncModelPackage#getAlarmBlock_AlarmCondition()
	 * @model containment="true"
	 * @generated
	 */
	AlarmTriggerCondition getAlarmCondition();

	/**
	 * Sets the value of the '{@link mncModel.AlarmBlock#getAlarmCondition <em>Alarm Condition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Alarm Condition</em>' containment reference.
	 * @see #getAlarmCondition()
	 * @generated
	 */
	void setAlarmCondition(AlarmTriggerCondition value);

	/**
	 * Returns the value of the '<em><b>Alarm Handling</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Alarm Handling</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Alarm Handling</em>' containment reference.
	 * @see #setAlarmHandling(AlarmHandling)
	 * @see mncModel.MncModelPackage#getAlarmBlock_AlarmHandling()
	 * @model containment="true"
	 * @generated
	 */
	AlarmHandling getAlarmHandling();

	/**
	 * Sets the value of the '{@link mncModel.AlarmBlock#getAlarmHandling <em>Alarm Handling</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Alarm Handling</em>' containment reference.
	 * @see #getAlarmHandling()
	 * @generated
	 */
	void setAlarmHandling(AlarmHandling value);

} // AlarmBlock
