/**
 */
package mncModel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Command Validation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mncModel.CommandValidation#getValidationRules <em>Validation Rules</em>}</li>
 *   <li>{@link mncModel.CommandValidation#getOperation <em>Operation</em>}</li>
 * </ul>
 *
 * @see mncModel.MncModelPackage#getCommandValidation()
 * @model
 * @generated
 */
public interface CommandValidation extends EObject {
	/**
	 * Returns the value of the '<em><b>Validation Rules</b></em>' containment reference list.
	 * The list contents are of type {@link mncModel.CheckParameterCondition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Validation Rules</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Validation Rules</em>' containment reference list.
	 * @see mncModel.MncModelPackage#getCommandValidation_ValidationRules()
	 * @model containment="true"
	 * @generated
	 */
	EList<CheckParameterCondition> getValidationRules();

	/**
	 * Returns the value of the '<em><b>Operation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operation</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operation</em>' containment reference.
	 * @see #setOperation(AbstractOperationableItems)
	 * @see mncModel.MncModelPackage#getCommandValidation_Operation()
	 * @model containment="true"
	 * @generated
	 */
	AbstractOperationableItems getOperation();

	/**
	 * Sets the value of the '{@link mncModel.CommandValidation#getOperation <em>Operation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operation</em>' containment reference.
	 * @see #getOperation()
	 * @generated
	 */
	void setOperation(AbstractOperationableItems value);

} // CommandValidation
