/**
 */
package mncModel.impl;

import java.util.Collection;

import mncModel.ControlNode;
import mncModel.MncModelPackage;
import mncModel.Response;
import mncModel.ResponseBlock;
import mncModel.ResponseTranslation;
import mncModel.ResponseValidation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Response Block</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link mncModel.impl.ResponseBlockImpl#getResponse <em>Response</em>}</li>
 *   <li>{@link mncModel.impl.ResponseBlockImpl#getResponseValidation <em>Response Validation</em>}</li>
 *   <li>{@link mncModel.impl.ResponseBlockImpl#getResponseTranslation <em>Response Translation</em>}</li>
 *   <li>{@link mncModel.impl.ResponseBlockImpl#getDestinationNodes <em>Destination Nodes</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ResponseBlockImpl extends MinimalEObjectImpl.Container implements ResponseBlock {
	/**
	 * The cached value of the '{@link #getResponse() <em>Response</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResponse()
	 * @generated
	 * @ordered
	 */
	protected Response response;

	/**
	 * The cached value of the '{@link #getResponseValidation() <em>Response Validation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResponseValidation()
	 * @generated
	 * @ordered
	 */
	protected ResponseValidation responseValidation;

	/**
	 * The cached value of the '{@link #getResponseTranslation() <em>Response Translation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResponseTranslation()
	 * @generated
	 * @ordered
	 */
	protected ResponseTranslation responseTranslation;

	/**
	 * The cached value of the '{@link #getDestinationNodes() <em>Destination Nodes</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDestinationNodes()
	 * @generated
	 * @ordered
	 */
	protected EList<ControlNode> destinationNodes;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ResponseBlockImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MncModelPackage.Literals.RESPONSE_BLOCK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Response getResponse() {
		if (response != null && response.eIsProxy()) {
			InternalEObject oldResponse = (InternalEObject)response;
			response = (Response)eResolveProxy(oldResponse);
			if (response != oldResponse) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MncModelPackage.RESPONSE_BLOCK__RESPONSE, oldResponse, response));
			}
		}
		return response;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Response basicGetResponse() {
		return response;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setResponse(Response newResponse) {
		Response oldResponse = response;
		response = newResponse;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MncModelPackage.RESPONSE_BLOCK__RESPONSE, oldResponse, response));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResponseValidation getResponseValidation() {
		return responseValidation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetResponseValidation(ResponseValidation newResponseValidation, NotificationChain msgs) {
		ResponseValidation oldResponseValidation = responseValidation;
		responseValidation = newResponseValidation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MncModelPackage.RESPONSE_BLOCK__RESPONSE_VALIDATION, oldResponseValidation, newResponseValidation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setResponseValidation(ResponseValidation newResponseValidation) {
		if (newResponseValidation != responseValidation) {
			NotificationChain msgs = null;
			if (responseValidation != null)
				msgs = ((InternalEObject)responseValidation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.RESPONSE_BLOCK__RESPONSE_VALIDATION, null, msgs);
			if (newResponseValidation != null)
				msgs = ((InternalEObject)newResponseValidation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.RESPONSE_BLOCK__RESPONSE_VALIDATION, null, msgs);
			msgs = basicSetResponseValidation(newResponseValidation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MncModelPackage.RESPONSE_BLOCK__RESPONSE_VALIDATION, newResponseValidation, newResponseValidation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResponseTranslation getResponseTranslation() {
		return responseTranslation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetResponseTranslation(ResponseTranslation newResponseTranslation, NotificationChain msgs) {
		ResponseTranslation oldResponseTranslation = responseTranslation;
		responseTranslation = newResponseTranslation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MncModelPackage.RESPONSE_BLOCK__RESPONSE_TRANSLATION, oldResponseTranslation, newResponseTranslation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setResponseTranslation(ResponseTranslation newResponseTranslation) {
		if (newResponseTranslation != responseTranslation) {
			NotificationChain msgs = null;
			if (responseTranslation != null)
				msgs = ((InternalEObject)responseTranslation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.RESPONSE_BLOCK__RESPONSE_TRANSLATION, null, msgs);
			if (newResponseTranslation != null)
				msgs = ((InternalEObject)newResponseTranslation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.RESPONSE_BLOCK__RESPONSE_TRANSLATION, null, msgs);
			msgs = basicSetResponseTranslation(newResponseTranslation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MncModelPackage.RESPONSE_BLOCK__RESPONSE_TRANSLATION, newResponseTranslation, newResponseTranslation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ControlNode> getDestinationNodes() {
		if (destinationNodes == null) {
			destinationNodes = new EObjectResolvingEList<ControlNode>(ControlNode.class, this, MncModelPackage.RESPONSE_BLOCK__DESTINATION_NODES);
		}
		return destinationNodes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MncModelPackage.RESPONSE_BLOCK__RESPONSE_VALIDATION:
				return basicSetResponseValidation(null, msgs);
			case MncModelPackage.RESPONSE_BLOCK__RESPONSE_TRANSLATION:
				return basicSetResponseTranslation(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MncModelPackage.RESPONSE_BLOCK__RESPONSE:
				if (resolve) return getResponse();
				return basicGetResponse();
			case MncModelPackage.RESPONSE_BLOCK__RESPONSE_VALIDATION:
				return getResponseValidation();
			case MncModelPackage.RESPONSE_BLOCK__RESPONSE_TRANSLATION:
				return getResponseTranslation();
			case MncModelPackage.RESPONSE_BLOCK__DESTINATION_NODES:
				return getDestinationNodes();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MncModelPackage.RESPONSE_BLOCK__RESPONSE:
				setResponse((Response)newValue);
				return;
			case MncModelPackage.RESPONSE_BLOCK__RESPONSE_VALIDATION:
				setResponseValidation((ResponseValidation)newValue);
				return;
			case MncModelPackage.RESPONSE_BLOCK__RESPONSE_TRANSLATION:
				setResponseTranslation((ResponseTranslation)newValue);
				return;
			case MncModelPackage.RESPONSE_BLOCK__DESTINATION_NODES:
				getDestinationNodes().clear();
				getDestinationNodes().addAll((Collection<? extends ControlNode>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MncModelPackage.RESPONSE_BLOCK__RESPONSE:
				setResponse((Response)null);
				return;
			case MncModelPackage.RESPONSE_BLOCK__RESPONSE_VALIDATION:
				setResponseValidation((ResponseValidation)null);
				return;
			case MncModelPackage.RESPONSE_BLOCK__RESPONSE_TRANSLATION:
				setResponseTranslation((ResponseTranslation)null);
				return;
			case MncModelPackage.RESPONSE_BLOCK__DESTINATION_NODES:
				getDestinationNodes().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MncModelPackage.RESPONSE_BLOCK__RESPONSE:
				return response != null;
			case MncModelPackage.RESPONSE_BLOCK__RESPONSE_VALIDATION:
				return responseValidation != null;
			case MncModelPackage.RESPONSE_BLOCK__RESPONSE_TRANSLATION:
				return responseTranslation != null;
			case MncModelPackage.RESPONSE_BLOCK__DESTINATION_NODES:
				return destinationNodes != null && !destinationNodes.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ResponseBlockImpl
