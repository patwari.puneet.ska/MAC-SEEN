/**
 */
package mncModel.impl;

import mncModel.MncModelPackage;
import mncModel.PrimitiveValue;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Primitive Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class PrimitiveValueImpl extends MinimalEObjectImpl.Container implements PrimitiveValue {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PrimitiveValueImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MncModelPackage.Literals.PRIMITIVE_VALUE;
	}

} //PrimitiveValueImpl
