/**
 */
package mncModel.impl;

import java.util.Collection;

import mncModel.ControlNode;
import mncModel.InterfaceDescription;
import mncModel.MncModelPackage;

import mncModel.utility.AlarmBlockUtility;
import mncModel.utility.CommandResponseBlockUtility;
import mncModel.utility.DataPointBlockUtility;
import mncModel.utility.EventBlockUtility;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Control Node</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link mncModel.impl.ControlNodeImpl#getName <em>Name</em>}</li>
 *   <li>{@link mncModel.impl.ControlNodeImpl#getInterfaceDescription <em>Interface Description</em>}</li>
 *   <li>{@link mncModel.impl.ControlNodeImpl#getChildNodes <em>Child Nodes</em>}</li>
 *   <li>{@link mncModel.impl.ControlNodeImpl#getParentNode <em>Parent Node</em>}</li>
 *   <li>{@link mncModel.impl.ControlNodeImpl#getCommandResponseBlocks <em>Command Response Blocks</em>}</li>
 *   <li>{@link mncModel.impl.ControlNodeImpl#getEventBlocks <em>Event Blocks</em>}</li>
 *   <li>{@link mncModel.impl.ControlNodeImpl#getAlarmBlocks <em>Alarm Blocks</em>}</li>
 *   <li>{@link mncModel.impl.ControlNodeImpl#getDataPointBlocks <em>Data Point Blocks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ControlNodeImpl extends SystemImpl implements ControlNode {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getInterfaceDescription() <em>Interface Description</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInterfaceDescription()
	 * @generated
	 * @ordered
	 */
	protected InterfaceDescription interfaceDescription;

	/**
	 * The cached value of the '{@link #getChildNodes() <em>Child Nodes</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChildNodes()
	 * @generated
	 * @ordered
	 */
	protected EList<ControlNode> childNodes;

	/**
	 * The cached value of the '{@link #getParentNode() <em>Parent Node</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParentNode()
	 * @generated
	 * @ordered
	 */
	protected ControlNode parentNode;

	/**
	 * The cached value of the '{@link #getCommandResponseBlocks() <em>Command Response Blocks</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCommandResponseBlocks()
	 * @generated
	 * @ordered
	 */
	protected CommandResponseBlockUtility commandResponseBlocks;

	/**
	 * The cached value of the '{@link #getEventBlocks() <em>Event Blocks</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEventBlocks()
	 * @generated
	 * @ordered
	 */
	protected EventBlockUtility eventBlocks;

	/**
	 * The cached value of the '{@link #getAlarmBlocks() <em>Alarm Blocks</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAlarmBlocks()
	 * @generated
	 * @ordered
	 */
	protected AlarmBlockUtility alarmBlocks;

	/**
	 * The cached value of the '{@link #getDataPointBlocks() <em>Data Point Blocks</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataPointBlocks()
	 * @generated
	 * @ordered
	 */
	protected DataPointBlockUtility dataPointBlocks;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ControlNodeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MncModelPackage.Literals.CONTROL_NODE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MncModelPackage.CONTROL_NODE__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InterfaceDescription getInterfaceDescription() {
		if (interfaceDescription != null && interfaceDescription.eIsProxy()) {
			InternalEObject oldInterfaceDescription = (InternalEObject)interfaceDescription;
			interfaceDescription = (InterfaceDescription)eResolveProxy(oldInterfaceDescription);
			if (interfaceDescription != oldInterfaceDescription) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MncModelPackage.CONTROL_NODE__INTERFACE_DESCRIPTION, oldInterfaceDescription, interfaceDescription));
			}
		}
		return interfaceDescription;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InterfaceDescription basicGetInterfaceDescription() {
		return interfaceDescription;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInterfaceDescription(InterfaceDescription newInterfaceDescription) {
		InterfaceDescription oldInterfaceDescription = interfaceDescription;
		interfaceDescription = newInterfaceDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MncModelPackage.CONTROL_NODE__INTERFACE_DESCRIPTION, oldInterfaceDescription, interfaceDescription));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ControlNode> getChildNodes() {
		if (childNodes == null) {
			childNodes = new EObjectEList<ControlNode>(ControlNode.class, this, MncModelPackage.CONTROL_NODE__CHILD_NODES);
		}
		return childNodes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ControlNode getParentNode() {
		if (parentNode != null && parentNode.eIsProxy()) {
			InternalEObject oldParentNode = (InternalEObject)parentNode;
			parentNode = (ControlNode)eResolveProxy(oldParentNode);
			if (parentNode != oldParentNode) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MncModelPackage.CONTROL_NODE__PARENT_NODE, oldParentNode, parentNode));
			}
		}
		return parentNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ControlNode basicGetParentNode() {
		return parentNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParentNode(ControlNode newParentNode) {
		ControlNode oldParentNode = parentNode;
		parentNode = newParentNode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MncModelPackage.CONTROL_NODE__PARENT_NODE, oldParentNode, parentNode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CommandResponseBlockUtility getCommandResponseBlocks() {
		return commandResponseBlocks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCommandResponseBlocks(CommandResponseBlockUtility newCommandResponseBlocks, NotificationChain msgs) {
		CommandResponseBlockUtility oldCommandResponseBlocks = commandResponseBlocks;
		commandResponseBlocks = newCommandResponseBlocks;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MncModelPackage.CONTROL_NODE__COMMAND_RESPONSE_BLOCKS, oldCommandResponseBlocks, newCommandResponseBlocks);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCommandResponseBlocks(CommandResponseBlockUtility newCommandResponseBlocks) {
		if (newCommandResponseBlocks != commandResponseBlocks) {
			NotificationChain msgs = null;
			if (commandResponseBlocks != null)
				msgs = ((InternalEObject)commandResponseBlocks).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.CONTROL_NODE__COMMAND_RESPONSE_BLOCKS, null, msgs);
			if (newCommandResponseBlocks != null)
				msgs = ((InternalEObject)newCommandResponseBlocks).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.CONTROL_NODE__COMMAND_RESPONSE_BLOCKS, null, msgs);
			msgs = basicSetCommandResponseBlocks(newCommandResponseBlocks, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MncModelPackage.CONTROL_NODE__COMMAND_RESPONSE_BLOCKS, newCommandResponseBlocks, newCommandResponseBlocks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EventBlockUtility getEventBlocks() {
		return eventBlocks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEventBlocks(EventBlockUtility newEventBlocks, NotificationChain msgs) {
		EventBlockUtility oldEventBlocks = eventBlocks;
		eventBlocks = newEventBlocks;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MncModelPackage.CONTROL_NODE__EVENT_BLOCKS, oldEventBlocks, newEventBlocks);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEventBlocks(EventBlockUtility newEventBlocks) {
		if (newEventBlocks != eventBlocks) {
			NotificationChain msgs = null;
			if (eventBlocks != null)
				msgs = ((InternalEObject)eventBlocks).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.CONTROL_NODE__EVENT_BLOCKS, null, msgs);
			if (newEventBlocks != null)
				msgs = ((InternalEObject)newEventBlocks).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.CONTROL_NODE__EVENT_BLOCKS, null, msgs);
			msgs = basicSetEventBlocks(newEventBlocks, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MncModelPackage.CONTROL_NODE__EVENT_BLOCKS, newEventBlocks, newEventBlocks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AlarmBlockUtility getAlarmBlocks() {
		return alarmBlocks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAlarmBlocks(AlarmBlockUtility newAlarmBlocks, NotificationChain msgs) {
		AlarmBlockUtility oldAlarmBlocks = alarmBlocks;
		alarmBlocks = newAlarmBlocks;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MncModelPackage.CONTROL_NODE__ALARM_BLOCKS, oldAlarmBlocks, newAlarmBlocks);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAlarmBlocks(AlarmBlockUtility newAlarmBlocks) {
		if (newAlarmBlocks != alarmBlocks) {
			NotificationChain msgs = null;
			if (alarmBlocks != null)
				msgs = ((InternalEObject)alarmBlocks).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.CONTROL_NODE__ALARM_BLOCKS, null, msgs);
			if (newAlarmBlocks != null)
				msgs = ((InternalEObject)newAlarmBlocks).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.CONTROL_NODE__ALARM_BLOCKS, null, msgs);
			msgs = basicSetAlarmBlocks(newAlarmBlocks, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MncModelPackage.CONTROL_NODE__ALARM_BLOCKS, newAlarmBlocks, newAlarmBlocks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataPointBlockUtility getDataPointBlocks() {
		return dataPointBlocks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDataPointBlocks(DataPointBlockUtility newDataPointBlocks, NotificationChain msgs) {
		DataPointBlockUtility oldDataPointBlocks = dataPointBlocks;
		dataPointBlocks = newDataPointBlocks;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MncModelPackage.CONTROL_NODE__DATA_POINT_BLOCKS, oldDataPointBlocks, newDataPointBlocks);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataPointBlocks(DataPointBlockUtility newDataPointBlocks) {
		if (newDataPointBlocks != dataPointBlocks) {
			NotificationChain msgs = null;
			if (dataPointBlocks != null)
				msgs = ((InternalEObject)dataPointBlocks).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.CONTROL_NODE__DATA_POINT_BLOCKS, null, msgs);
			if (newDataPointBlocks != null)
				msgs = ((InternalEObject)newDataPointBlocks).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.CONTROL_NODE__DATA_POINT_BLOCKS, null, msgs);
			msgs = basicSetDataPointBlocks(newDataPointBlocks, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MncModelPackage.CONTROL_NODE__DATA_POINT_BLOCKS, newDataPointBlocks, newDataPointBlocks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MncModelPackage.CONTROL_NODE__COMMAND_RESPONSE_BLOCKS:
				return basicSetCommandResponseBlocks(null, msgs);
			case MncModelPackage.CONTROL_NODE__EVENT_BLOCKS:
				return basicSetEventBlocks(null, msgs);
			case MncModelPackage.CONTROL_NODE__ALARM_BLOCKS:
				return basicSetAlarmBlocks(null, msgs);
			case MncModelPackage.CONTROL_NODE__DATA_POINT_BLOCKS:
				return basicSetDataPointBlocks(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MncModelPackage.CONTROL_NODE__NAME:
				return getName();
			case MncModelPackage.CONTROL_NODE__INTERFACE_DESCRIPTION:
				if (resolve) return getInterfaceDescription();
				return basicGetInterfaceDescription();
			case MncModelPackage.CONTROL_NODE__CHILD_NODES:
				return getChildNodes();
			case MncModelPackage.CONTROL_NODE__PARENT_NODE:
				if (resolve) return getParentNode();
				return basicGetParentNode();
			case MncModelPackage.CONTROL_NODE__COMMAND_RESPONSE_BLOCKS:
				return getCommandResponseBlocks();
			case MncModelPackage.CONTROL_NODE__EVENT_BLOCKS:
				return getEventBlocks();
			case MncModelPackage.CONTROL_NODE__ALARM_BLOCKS:
				return getAlarmBlocks();
			case MncModelPackage.CONTROL_NODE__DATA_POINT_BLOCKS:
				return getDataPointBlocks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MncModelPackage.CONTROL_NODE__NAME:
				setName((String)newValue);
				return;
			case MncModelPackage.CONTROL_NODE__INTERFACE_DESCRIPTION:
				setInterfaceDescription((InterfaceDescription)newValue);
				return;
			case MncModelPackage.CONTROL_NODE__CHILD_NODES:
				getChildNodes().clear();
				getChildNodes().addAll((Collection<? extends ControlNode>)newValue);
				return;
			case MncModelPackage.CONTROL_NODE__PARENT_NODE:
				setParentNode((ControlNode)newValue);
				return;
			case MncModelPackage.CONTROL_NODE__COMMAND_RESPONSE_BLOCKS:
				setCommandResponseBlocks((CommandResponseBlockUtility)newValue);
				return;
			case MncModelPackage.CONTROL_NODE__EVENT_BLOCKS:
				setEventBlocks((EventBlockUtility)newValue);
				return;
			case MncModelPackage.CONTROL_NODE__ALARM_BLOCKS:
				setAlarmBlocks((AlarmBlockUtility)newValue);
				return;
			case MncModelPackage.CONTROL_NODE__DATA_POINT_BLOCKS:
				setDataPointBlocks((DataPointBlockUtility)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MncModelPackage.CONTROL_NODE__NAME:
				setName(NAME_EDEFAULT);
				return;
			case MncModelPackage.CONTROL_NODE__INTERFACE_DESCRIPTION:
				setInterfaceDescription((InterfaceDescription)null);
				return;
			case MncModelPackage.CONTROL_NODE__CHILD_NODES:
				getChildNodes().clear();
				return;
			case MncModelPackage.CONTROL_NODE__PARENT_NODE:
				setParentNode((ControlNode)null);
				return;
			case MncModelPackage.CONTROL_NODE__COMMAND_RESPONSE_BLOCKS:
				setCommandResponseBlocks((CommandResponseBlockUtility)null);
				return;
			case MncModelPackage.CONTROL_NODE__EVENT_BLOCKS:
				setEventBlocks((EventBlockUtility)null);
				return;
			case MncModelPackage.CONTROL_NODE__ALARM_BLOCKS:
				setAlarmBlocks((AlarmBlockUtility)null);
				return;
			case MncModelPackage.CONTROL_NODE__DATA_POINT_BLOCKS:
				setDataPointBlocks((DataPointBlockUtility)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MncModelPackage.CONTROL_NODE__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case MncModelPackage.CONTROL_NODE__INTERFACE_DESCRIPTION:
				return interfaceDescription != null;
			case MncModelPackage.CONTROL_NODE__CHILD_NODES:
				return childNodes != null && !childNodes.isEmpty();
			case MncModelPackage.CONTROL_NODE__PARENT_NODE:
				return parentNode != null;
			case MncModelPackage.CONTROL_NODE__COMMAND_RESPONSE_BLOCKS:
				return commandResponseBlocks != null;
			case MncModelPackage.CONTROL_NODE__EVENT_BLOCKS:
				return eventBlocks != null;
			case MncModelPackage.CONTROL_NODE__ALARM_BLOCKS:
				return alarmBlocks != null;
			case MncModelPackage.CONTROL_NODE__DATA_POINT_BLOCKS:
				return dataPointBlocks != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //ControlNodeImpl
