/**
 */
package mncModel.impl;

import mncModel.DataPoint;
import mncModel.DataPointBlock;
import mncModel.DataPointHandling;
import mncModel.DataPointTriggerCondition;
import mncModel.MncModelPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data Point Block</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link mncModel.impl.DataPointBlockImpl#getDataPoint <em>Data Point</em>}</li>
 *   <li>{@link mncModel.impl.DataPointBlockImpl#getDataPointCondition <em>Data Point Condition</em>}</li>
 *   <li>{@link mncModel.impl.DataPointBlockImpl#getDataPointHandling <em>Data Point Handling</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DataPointBlockImpl extends MinimalEObjectImpl.Container implements DataPointBlock {
	/**
	 * The cached value of the '{@link #getDataPoint() <em>Data Point</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataPoint()
	 * @generated
	 * @ordered
	 */
	protected DataPoint dataPoint;

	/**
	 * The cached value of the '{@link #getDataPointCondition() <em>Data Point Condition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataPointCondition()
	 * @generated
	 * @ordered
	 */
	protected DataPointTriggerCondition dataPointCondition;

	/**
	 * The cached value of the '{@link #getDataPointHandling() <em>Data Point Handling</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataPointHandling()
	 * @generated
	 * @ordered
	 */
	protected DataPointHandling dataPointHandling;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataPointBlockImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MncModelPackage.Literals.DATA_POINT_BLOCK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataPoint getDataPoint() {
		if (dataPoint != null && dataPoint.eIsProxy()) {
			InternalEObject oldDataPoint = (InternalEObject)dataPoint;
			dataPoint = (DataPoint)eResolveProxy(oldDataPoint);
			if (dataPoint != oldDataPoint) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MncModelPackage.DATA_POINT_BLOCK__DATA_POINT, oldDataPoint, dataPoint));
			}
		}
		return dataPoint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataPoint basicGetDataPoint() {
		return dataPoint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataPoint(DataPoint newDataPoint) {
		DataPoint oldDataPoint = dataPoint;
		dataPoint = newDataPoint;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MncModelPackage.DATA_POINT_BLOCK__DATA_POINT, oldDataPoint, dataPoint));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataPointTriggerCondition getDataPointCondition() {
		return dataPointCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDataPointCondition(DataPointTriggerCondition newDataPointCondition, NotificationChain msgs) {
		DataPointTriggerCondition oldDataPointCondition = dataPointCondition;
		dataPointCondition = newDataPointCondition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MncModelPackage.DATA_POINT_BLOCK__DATA_POINT_CONDITION, oldDataPointCondition, newDataPointCondition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataPointCondition(DataPointTriggerCondition newDataPointCondition) {
		if (newDataPointCondition != dataPointCondition) {
			NotificationChain msgs = null;
			if (dataPointCondition != null)
				msgs = ((InternalEObject)dataPointCondition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.DATA_POINT_BLOCK__DATA_POINT_CONDITION, null, msgs);
			if (newDataPointCondition != null)
				msgs = ((InternalEObject)newDataPointCondition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.DATA_POINT_BLOCK__DATA_POINT_CONDITION, null, msgs);
			msgs = basicSetDataPointCondition(newDataPointCondition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MncModelPackage.DATA_POINT_BLOCK__DATA_POINT_CONDITION, newDataPointCondition, newDataPointCondition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataPointHandling getDataPointHandling() {
		return dataPointHandling;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDataPointHandling(DataPointHandling newDataPointHandling, NotificationChain msgs) {
		DataPointHandling oldDataPointHandling = dataPointHandling;
		dataPointHandling = newDataPointHandling;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MncModelPackage.DATA_POINT_BLOCK__DATA_POINT_HANDLING, oldDataPointHandling, newDataPointHandling);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataPointHandling(DataPointHandling newDataPointHandling) {
		if (newDataPointHandling != dataPointHandling) {
			NotificationChain msgs = null;
			if (dataPointHandling != null)
				msgs = ((InternalEObject)dataPointHandling).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.DATA_POINT_BLOCK__DATA_POINT_HANDLING, null, msgs);
			if (newDataPointHandling != null)
				msgs = ((InternalEObject)newDataPointHandling).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.DATA_POINT_BLOCK__DATA_POINT_HANDLING, null, msgs);
			msgs = basicSetDataPointHandling(newDataPointHandling, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MncModelPackage.DATA_POINT_BLOCK__DATA_POINT_HANDLING, newDataPointHandling, newDataPointHandling));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MncModelPackage.DATA_POINT_BLOCK__DATA_POINT_CONDITION:
				return basicSetDataPointCondition(null, msgs);
			case MncModelPackage.DATA_POINT_BLOCK__DATA_POINT_HANDLING:
				return basicSetDataPointHandling(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MncModelPackage.DATA_POINT_BLOCK__DATA_POINT:
				if (resolve) return getDataPoint();
				return basicGetDataPoint();
			case MncModelPackage.DATA_POINT_BLOCK__DATA_POINT_CONDITION:
				return getDataPointCondition();
			case MncModelPackage.DATA_POINT_BLOCK__DATA_POINT_HANDLING:
				return getDataPointHandling();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MncModelPackage.DATA_POINT_BLOCK__DATA_POINT:
				setDataPoint((DataPoint)newValue);
				return;
			case MncModelPackage.DATA_POINT_BLOCK__DATA_POINT_CONDITION:
				setDataPointCondition((DataPointTriggerCondition)newValue);
				return;
			case MncModelPackage.DATA_POINT_BLOCK__DATA_POINT_HANDLING:
				setDataPointHandling((DataPointHandling)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MncModelPackage.DATA_POINT_BLOCK__DATA_POINT:
				setDataPoint((DataPoint)null);
				return;
			case MncModelPackage.DATA_POINT_BLOCK__DATA_POINT_CONDITION:
				setDataPointCondition((DataPointTriggerCondition)null);
				return;
			case MncModelPackage.DATA_POINT_BLOCK__DATA_POINT_HANDLING:
				setDataPointHandling((DataPointHandling)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MncModelPackage.DATA_POINT_BLOCK__DATA_POINT:
				return dataPoint != null;
			case MncModelPackage.DATA_POINT_BLOCK__DATA_POINT_CONDITION:
				return dataPointCondition != null;
			case MncModelPackage.DATA_POINT_BLOCK__DATA_POINT_HANDLING:
				return dataPointHandling != null;
		}
		return super.eIsSet(featureID);
	}

} //DataPointBlockImpl
