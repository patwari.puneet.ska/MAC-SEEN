/**
 */
package mncModel.impl;

import java.util.Collection;

import mncModel.DataPoint;
import mncModel.DataPointValidCondition;
import mncModel.MncModelPackage;
import mncModel.PrimitiveValue;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data Point Valid Condition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link mncModel.impl.DataPointValidConditionImpl#getDataPoint <em>Data Point</em>}</li>
 *   <li>{@link mncModel.impl.DataPointValidConditionImpl#getCheckValues <em>Check Values</em>}</li>
 *   <li>{@link mncModel.impl.DataPointValidConditionImpl#getCheckMaxValue <em>Check Max Value</em>}</li>
 *   <li>{@link mncModel.impl.DataPointValidConditionImpl#getCheckMinValue <em>Check Min Value</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DataPointValidConditionImpl extends MinimalEObjectImpl.Container implements DataPointValidCondition {
	/**
	 * The cached value of the '{@link #getDataPoint() <em>Data Point</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataPoint()
	 * @generated
	 * @ordered
	 */
	protected DataPoint dataPoint;

	/**
	 * The cached value of the '{@link #getCheckValues() <em>Check Values</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCheckValues()
	 * @generated
	 * @ordered
	 */
	protected EList<PrimitiveValue> checkValues;

	/**
	 * The cached value of the '{@link #getCheckMaxValue() <em>Check Max Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCheckMaxValue()
	 * @generated
	 * @ordered
	 */
	protected PrimitiveValue checkMaxValue;

	/**
	 * The cached value of the '{@link #getCheckMinValue() <em>Check Min Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCheckMinValue()
	 * @generated
	 * @ordered
	 */
	protected PrimitiveValue checkMinValue;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataPointValidConditionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MncModelPackage.Literals.DATA_POINT_VALID_CONDITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataPoint getDataPoint() {
		if (dataPoint != null && dataPoint.eIsProxy()) {
			InternalEObject oldDataPoint = (InternalEObject)dataPoint;
			dataPoint = (DataPoint)eResolveProxy(oldDataPoint);
			if (dataPoint != oldDataPoint) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MncModelPackage.DATA_POINT_VALID_CONDITION__DATA_POINT, oldDataPoint, dataPoint));
			}
		}
		return dataPoint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataPoint basicGetDataPoint() {
		return dataPoint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataPoint(DataPoint newDataPoint) {
		DataPoint oldDataPoint = dataPoint;
		dataPoint = newDataPoint;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MncModelPackage.DATA_POINT_VALID_CONDITION__DATA_POINT, oldDataPoint, dataPoint));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PrimitiveValue> getCheckValues() {
		if (checkValues == null) {
			checkValues = new EObjectContainmentEList<PrimitiveValue>(PrimitiveValue.class, this, MncModelPackage.DATA_POINT_VALID_CONDITION__CHECK_VALUES);
		}
		return checkValues;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PrimitiveValue getCheckMaxValue() {
		return checkMaxValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCheckMaxValue(PrimitiveValue newCheckMaxValue, NotificationChain msgs) {
		PrimitiveValue oldCheckMaxValue = checkMaxValue;
		checkMaxValue = newCheckMaxValue;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MncModelPackage.DATA_POINT_VALID_CONDITION__CHECK_MAX_VALUE, oldCheckMaxValue, newCheckMaxValue);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCheckMaxValue(PrimitiveValue newCheckMaxValue) {
		if (newCheckMaxValue != checkMaxValue) {
			NotificationChain msgs = null;
			if (checkMaxValue != null)
				msgs = ((InternalEObject)checkMaxValue).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.DATA_POINT_VALID_CONDITION__CHECK_MAX_VALUE, null, msgs);
			if (newCheckMaxValue != null)
				msgs = ((InternalEObject)newCheckMaxValue).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.DATA_POINT_VALID_CONDITION__CHECK_MAX_VALUE, null, msgs);
			msgs = basicSetCheckMaxValue(newCheckMaxValue, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MncModelPackage.DATA_POINT_VALID_CONDITION__CHECK_MAX_VALUE, newCheckMaxValue, newCheckMaxValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PrimitiveValue getCheckMinValue() {
		return checkMinValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCheckMinValue(PrimitiveValue newCheckMinValue, NotificationChain msgs) {
		PrimitiveValue oldCheckMinValue = checkMinValue;
		checkMinValue = newCheckMinValue;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MncModelPackage.DATA_POINT_VALID_CONDITION__CHECK_MIN_VALUE, oldCheckMinValue, newCheckMinValue);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCheckMinValue(PrimitiveValue newCheckMinValue) {
		if (newCheckMinValue != checkMinValue) {
			NotificationChain msgs = null;
			if (checkMinValue != null)
				msgs = ((InternalEObject)checkMinValue).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.DATA_POINT_VALID_CONDITION__CHECK_MIN_VALUE, null, msgs);
			if (newCheckMinValue != null)
				msgs = ((InternalEObject)newCheckMinValue).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.DATA_POINT_VALID_CONDITION__CHECK_MIN_VALUE, null, msgs);
			msgs = basicSetCheckMinValue(newCheckMinValue, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MncModelPackage.DATA_POINT_VALID_CONDITION__CHECK_MIN_VALUE, newCheckMinValue, newCheckMinValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MncModelPackage.DATA_POINT_VALID_CONDITION__CHECK_VALUES:
				return ((InternalEList<?>)getCheckValues()).basicRemove(otherEnd, msgs);
			case MncModelPackage.DATA_POINT_VALID_CONDITION__CHECK_MAX_VALUE:
				return basicSetCheckMaxValue(null, msgs);
			case MncModelPackage.DATA_POINT_VALID_CONDITION__CHECK_MIN_VALUE:
				return basicSetCheckMinValue(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MncModelPackage.DATA_POINT_VALID_CONDITION__DATA_POINT:
				if (resolve) return getDataPoint();
				return basicGetDataPoint();
			case MncModelPackage.DATA_POINT_VALID_CONDITION__CHECK_VALUES:
				return getCheckValues();
			case MncModelPackage.DATA_POINT_VALID_CONDITION__CHECK_MAX_VALUE:
				return getCheckMaxValue();
			case MncModelPackage.DATA_POINT_VALID_CONDITION__CHECK_MIN_VALUE:
				return getCheckMinValue();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MncModelPackage.DATA_POINT_VALID_CONDITION__DATA_POINT:
				setDataPoint((DataPoint)newValue);
				return;
			case MncModelPackage.DATA_POINT_VALID_CONDITION__CHECK_VALUES:
				getCheckValues().clear();
				getCheckValues().addAll((Collection<? extends PrimitiveValue>)newValue);
				return;
			case MncModelPackage.DATA_POINT_VALID_CONDITION__CHECK_MAX_VALUE:
				setCheckMaxValue((PrimitiveValue)newValue);
				return;
			case MncModelPackage.DATA_POINT_VALID_CONDITION__CHECK_MIN_VALUE:
				setCheckMinValue((PrimitiveValue)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MncModelPackage.DATA_POINT_VALID_CONDITION__DATA_POINT:
				setDataPoint((DataPoint)null);
				return;
			case MncModelPackage.DATA_POINT_VALID_CONDITION__CHECK_VALUES:
				getCheckValues().clear();
				return;
			case MncModelPackage.DATA_POINT_VALID_CONDITION__CHECK_MAX_VALUE:
				setCheckMaxValue((PrimitiveValue)null);
				return;
			case MncModelPackage.DATA_POINT_VALID_CONDITION__CHECK_MIN_VALUE:
				setCheckMinValue((PrimitiveValue)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MncModelPackage.DATA_POINT_VALID_CONDITION__DATA_POINT:
				return dataPoint != null;
			case MncModelPackage.DATA_POINT_VALID_CONDITION__CHECK_VALUES:
				return checkValues != null && !checkValues.isEmpty();
			case MncModelPackage.DATA_POINT_VALID_CONDITION__CHECK_MAX_VALUE:
				return checkMaxValue != null;
			case MncModelPackage.DATA_POINT_VALID_CONDITION__CHECK_MIN_VALUE:
				return checkMinValue != null;
		}
		return super.eIsSet(featureID);
	}

} //DataPointValidConditionImpl
