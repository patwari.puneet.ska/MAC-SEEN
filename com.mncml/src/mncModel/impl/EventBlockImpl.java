/**
 */
package mncModel.impl;

import mncModel.Event;
import mncModel.EventBlock;
import mncModel.EventHandling;
import mncModel.EventTriggerCondition;
import mncModel.MncModelPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Event Block</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link mncModel.impl.EventBlockImpl#getEvent <em>Event</em>}</li>
 *   <li>{@link mncModel.impl.EventBlockImpl#getEventCondition <em>Event Condition</em>}</li>
 *   <li>{@link mncModel.impl.EventBlockImpl#getEventHandling <em>Event Handling</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EventBlockImpl extends MinimalEObjectImpl.Container implements EventBlock {
	/**
	 * The cached value of the '{@link #getEvent() <em>Event</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEvent()
	 * @generated
	 * @ordered
	 */
	protected Event event;

	/**
	 * The cached value of the '{@link #getEventCondition() <em>Event Condition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEventCondition()
	 * @generated
	 * @ordered
	 */
	protected EventTriggerCondition eventCondition;

	/**
	 * The cached value of the '{@link #getEventHandling() <em>Event Handling</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEventHandling()
	 * @generated
	 * @ordered
	 */
	protected EventHandling eventHandling;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EventBlockImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MncModelPackage.Literals.EVENT_BLOCK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Event getEvent() {
		if (event != null && event.eIsProxy()) {
			InternalEObject oldEvent = (InternalEObject)event;
			event = (Event)eResolveProxy(oldEvent);
			if (event != oldEvent) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MncModelPackage.EVENT_BLOCK__EVENT, oldEvent, event));
			}
		}
		return event;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Event basicGetEvent() {
		return event;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEvent(Event newEvent) {
		Event oldEvent = event;
		event = newEvent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MncModelPackage.EVENT_BLOCK__EVENT, oldEvent, event));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EventTriggerCondition getEventCondition() {
		return eventCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEventCondition(EventTriggerCondition newEventCondition, NotificationChain msgs) {
		EventTriggerCondition oldEventCondition = eventCondition;
		eventCondition = newEventCondition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MncModelPackage.EVENT_BLOCK__EVENT_CONDITION, oldEventCondition, newEventCondition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEventCondition(EventTriggerCondition newEventCondition) {
		if (newEventCondition != eventCondition) {
			NotificationChain msgs = null;
			if (eventCondition != null)
				msgs = ((InternalEObject)eventCondition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.EVENT_BLOCK__EVENT_CONDITION, null, msgs);
			if (newEventCondition != null)
				msgs = ((InternalEObject)newEventCondition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.EVENT_BLOCK__EVENT_CONDITION, null, msgs);
			msgs = basicSetEventCondition(newEventCondition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MncModelPackage.EVENT_BLOCK__EVENT_CONDITION, newEventCondition, newEventCondition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EventHandling getEventHandling() {
		return eventHandling;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEventHandling(EventHandling newEventHandling, NotificationChain msgs) {
		EventHandling oldEventHandling = eventHandling;
		eventHandling = newEventHandling;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MncModelPackage.EVENT_BLOCK__EVENT_HANDLING, oldEventHandling, newEventHandling);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEventHandling(EventHandling newEventHandling) {
		if (newEventHandling != eventHandling) {
			NotificationChain msgs = null;
			if (eventHandling != null)
				msgs = ((InternalEObject)eventHandling).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.EVENT_BLOCK__EVENT_HANDLING, null, msgs);
			if (newEventHandling != null)
				msgs = ((InternalEObject)newEventHandling).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.EVENT_BLOCK__EVENT_HANDLING, null, msgs);
			msgs = basicSetEventHandling(newEventHandling, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MncModelPackage.EVENT_BLOCK__EVENT_HANDLING, newEventHandling, newEventHandling));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MncModelPackage.EVENT_BLOCK__EVENT_CONDITION:
				return basicSetEventCondition(null, msgs);
			case MncModelPackage.EVENT_BLOCK__EVENT_HANDLING:
				return basicSetEventHandling(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MncModelPackage.EVENT_BLOCK__EVENT:
				if (resolve) return getEvent();
				return basicGetEvent();
			case MncModelPackage.EVENT_BLOCK__EVENT_CONDITION:
				return getEventCondition();
			case MncModelPackage.EVENT_BLOCK__EVENT_HANDLING:
				return getEventHandling();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MncModelPackage.EVENT_BLOCK__EVENT:
				setEvent((Event)newValue);
				return;
			case MncModelPackage.EVENT_BLOCK__EVENT_CONDITION:
				setEventCondition((EventTriggerCondition)newValue);
				return;
			case MncModelPackage.EVENT_BLOCK__EVENT_HANDLING:
				setEventHandling((EventHandling)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MncModelPackage.EVENT_BLOCK__EVENT:
				setEvent((Event)null);
				return;
			case MncModelPackage.EVENT_BLOCK__EVENT_CONDITION:
				setEventCondition((EventTriggerCondition)null);
				return;
			case MncModelPackage.EVENT_BLOCK__EVENT_HANDLING:
				setEventHandling((EventHandling)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MncModelPackage.EVENT_BLOCK__EVENT:
				return event != null;
			case MncModelPackage.EVENT_BLOCK__EVENT_CONDITION:
				return eventCondition != null;
			case MncModelPackage.EVENT_BLOCK__EVENT_HANDLING:
				return eventHandling != null;
		}
		return super.eIsSet(featureID);
	}

} //EventBlockImpl
