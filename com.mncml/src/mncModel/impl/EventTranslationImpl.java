/**
 */
package mncModel.impl;

import java.util.Collection;

import mncModel.EventTranslation;
import mncModel.EventTranslationRule;
import mncModel.MncModelPackage;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Event Translation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link mncModel.impl.EventTranslationImpl#getEventTranslationRules <em>Event Translation Rules</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EventTranslationImpl extends MinimalEObjectImpl.Container implements EventTranslation {
	/**
	 * The cached value of the '{@link #getEventTranslationRules() <em>Event Translation Rules</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEventTranslationRules()
	 * @generated
	 * @ordered
	 */
	protected EList<EventTranslationRule> eventTranslationRules;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EventTranslationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MncModelPackage.Literals.EVENT_TRANSLATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EventTranslationRule> getEventTranslationRules() {
		if (eventTranslationRules == null) {
			eventTranslationRules = new EObjectContainmentEList<EventTranslationRule>(EventTranslationRule.class, this, MncModelPackage.EVENT_TRANSLATION__EVENT_TRANSLATION_RULES);
		}
		return eventTranslationRules;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MncModelPackage.EVENT_TRANSLATION__EVENT_TRANSLATION_RULES:
				return ((InternalEList<?>)getEventTranslationRules()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MncModelPackage.EVENT_TRANSLATION__EVENT_TRANSLATION_RULES:
				return getEventTranslationRules();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MncModelPackage.EVENT_TRANSLATION__EVENT_TRANSLATION_RULES:
				getEventTranslationRules().clear();
				getEventTranslationRules().addAll((Collection<? extends EventTranslationRule>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MncModelPackage.EVENT_TRANSLATION__EVENT_TRANSLATION_RULES:
				getEventTranslationRules().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MncModelPackage.EVENT_TRANSLATION__EVENT_TRANSLATION_RULES:
				return eventTranslationRules != null && !eventTranslationRules.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //EventTranslationImpl
