/**
 */
package mncModel.impl;

import java.util.Collection;

import mncModel.Command;
import mncModel.CommandDistribution;
import mncModel.ControlNode;
import mncModel.MncModelPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Command Distribution</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link mncModel.impl.CommandDistributionImpl#getCommand <em>Command</em>}</li>
 *   <li>{@link mncModel.impl.CommandDistributionImpl#getDestinationNodes <em>Destination Nodes</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CommandDistributionImpl extends MinimalEObjectImpl.Container implements CommandDistribution {
	/**
	 * The cached value of the '{@link #getCommand() <em>Command</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCommand()
	 * @generated
	 * @ordered
	 */
	protected Command command;

	/**
	 * The cached value of the '{@link #getDestinationNodes() <em>Destination Nodes</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDestinationNodes()
	 * @generated
	 * @ordered
	 */
	protected EList<ControlNode> destinationNodes;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CommandDistributionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MncModelPackage.Literals.COMMAND_DISTRIBUTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Command getCommand() {
		if (command != null && command.eIsProxy()) {
			InternalEObject oldCommand = (InternalEObject)command;
			command = (Command)eResolveProxy(oldCommand);
			if (command != oldCommand) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MncModelPackage.COMMAND_DISTRIBUTION__COMMAND, oldCommand, command));
			}
		}
		return command;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Command basicGetCommand() {
		return command;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCommand(Command newCommand) {
		Command oldCommand = command;
		command = newCommand;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MncModelPackage.COMMAND_DISTRIBUTION__COMMAND, oldCommand, command));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ControlNode> getDestinationNodes() {
		if (destinationNodes == null) {
			destinationNodes = new EObjectResolvingEList<ControlNode>(ControlNode.class, this, MncModelPackage.COMMAND_DISTRIBUTION__DESTINATION_NODES);
		}
		return destinationNodes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MncModelPackage.COMMAND_DISTRIBUTION__COMMAND:
				if (resolve) return getCommand();
				return basicGetCommand();
			case MncModelPackage.COMMAND_DISTRIBUTION__DESTINATION_NODES:
				return getDestinationNodes();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MncModelPackage.COMMAND_DISTRIBUTION__COMMAND:
				setCommand((Command)newValue);
				return;
			case MncModelPackage.COMMAND_DISTRIBUTION__DESTINATION_NODES:
				getDestinationNodes().clear();
				getDestinationNodes().addAll((Collection<? extends ControlNode>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MncModelPackage.COMMAND_DISTRIBUTION__COMMAND:
				setCommand((Command)null);
				return;
			case MncModelPackage.COMMAND_DISTRIBUTION__DESTINATION_NODES:
				getDestinationNodes().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MncModelPackage.COMMAND_DISTRIBUTION__COMMAND:
				return command != null;
			case MncModelPackage.COMMAND_DISTRIBUTION__DESTINATION_NODES:
				return destinationNodes != null && !destinationNodes.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //CommandDistributionImpl
