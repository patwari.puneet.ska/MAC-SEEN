/**
 */
package mncModel.impl;

import java.util.Collection;

import mncModel.AbstractOperationableItems;
import mncModel.MncModelPackage;
import mncModel.ParameterTranslation;
import mncModel.Response;
import mncModel.ResponseTranslationRule;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Response Translation Rule</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link mncModel.impl.ResponseTranslationRuleImpl#getInputResponses <em>Input Responses</em>}</li>
 *   <li>{@link mncModel.impl.ResponseTranslationRuleImpl#getTranslatedResponse <em>Translated Response</em>}</li>
 *   <li>{@link mncModel.impl.ResponseTranslationRuleImpl#getParameterTranslations <em>Parameter Translations</em>}</li>
 *   <li>{@link mncModel.impl.ResponseTranslationRuleImpl#getOperation <em>Operation</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ResponseTranslationRuleImpl extends MinimalEObjectImpl.Container implements ResponseTranslationRule {
	/**
	 * The cached value of the '{@link #getInputResponses() <em>Input Responses</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInputResponses()
	 * @generated
	 * @ordered
	 */
	protected EList<Response> inputResponses;

	/**
	 * The cached value of the '{@link #getTranslatedResponse() <em>Translated Response</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTranslatedResponse()
	 * @generated
	 * @ordered
	 */
	protected Response translatedResponse;

	/**
	 * The cached value of the '{@link #getParameterTranslations() <em>Parameter Translations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameterTranslations()
	 * @generated
	 * @ordered
	 */
	protected EList<ParameterTranslation> parameterTranslations;

	/**
	 * The cached value of the '{@link #getOperation() <em>Operation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperation()
	 * @generated
	 * @ordered
	 */
	protected AbstractOperationableItems operation;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ResponseTranslationRuleImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MncModelPackage.Literals.RESPONSE_TRANSLATION_RULE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Response> getInputResponses() {
		if (inputResponses == null) {
			inputResponses = new EObjectResolvingEList<Response>(Response.class, this, MncModelPackage.RESPONSE_TRANSLATION_RULE__INPUT_RESPONSES);
		}
		return inputResponses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Response getTranslatedResponse() {
		if (translatedResponse != null && translatedResponse.eIsProxy()) {
			InternalEObject oldTranslatedResponse = (InternalEObject)translatedResponse;
			translatedResponse = (Response)eResolveProxy(oldTranslatedResponse);
			if (translatedResponse != oldTranslatedResponse) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MncModelPackage.RESPONSE_TRANSLATION_RULE__TRANSLATED_RESPONSE, oldTranslatedResponse, translatedResponse));
			}
		}
		return translatedResponse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Response basicGetTranslatedResponse() {
		return translatedResponse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTranslatedResponse(Response newTranslatedResponse) {
		Response oldTranslatedResponse = translatedResponse;
		translatedResponse = newTranslatedResponse;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MncModelPackage.RESPONSE_TRANSLATION_RULE__TRANSLATED_RESPONSE, oldTranslatedResponse, translatedResponse));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ParameterTranslation> getParameterTranslations() {
		if (parameterTranslations == null) {
			parameterTranslations = new EObjectContainmentEList<ParameterTranslation>(ParameterTranslation.class, this, MncModelPackage.RESPONSE_TRANSLATION_RULE__PARAMETER_TRANSLATIONS);
		}
		return parameterTranslations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractOperationableItems getOperation() {
		return operation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOperation(AbstractOperationableItems newOperation, NotificationChain msgs) {
		AbstractOperationableItems oldOperation = operation;
		operation = newOperation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MncModelPackage.RESPONSE_TRANSLATION_RULE__OPERATION, oldOperation, newOperation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperation(AbstractOperationableItems newOperation) {
		if (newOperation != operation) {
			NotificationChain msgs = null;
			if (operation != null)
				msgs = ((InternalEObject)operation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.RESPONSE_TRANSLATION_RULE__OPERATION, null, msgs);
			if (newOperation != null)
				msgs = ((InternalEObject)newOperation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.RESPONSE_TRANSLATION_RULE__OPERATION, null, msgs);
			msgs = basicSetOperation(newOperation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MncModelPackage.RESPONSE_TRANSLATION_RULE__OPERATION, newOperation, newOperation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MncModelPackage.RESPONSE_TRANSLATION_RULE__PARAMETER_TRANSLATIONS:
				return ((InternalEList<?>)getParameterTranslations()).basicRemove(otherEnd, msgs);
			case MncModelPackage.RESPONSE_TRANSLATION_RULE__OPERATION:
				return basicSetOperation(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MncModelPackage.RESPONSE_TRANSLATION_RULE__INPUT_RESPONSES:
				return getInputResponses();
			case MncModelPackage.RESPONSE_TRANSLATION_RULE__TRANSLATED_RESPONSE:
				if (resolve) return getTranslatedResponse();
				return basicGetTranslatedResponse();
			case MncModelPackage.RESPONSE_TRANSLATION_RULE__PARAMETER_TRANSLATIONS:
				return getParameterTranslations();
			case MncModelPackage.RESPONSE_TRANSLATION_RULE__OPERATION:
				return getOperation();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MncModelPackage.RESPONSE_TRANSLATION_RULE__INPUT_RESPONSES:
				getInputResponses().clear();
				getInputResponses().addAll((Collection<? extends Response>)newValue);
				return;
			case MncModelPackage.RESPONSE_TRANSLATION_RULE__TRANSLATED_RESPONSE:
				setTranslatedResponse((Response)newValue);
				return;
			case MncModelPackage.RESPONSE_TRANSLATION_RULE__PARAMETER_TRANSLATIONS:
				getParameterTranslations().clear();
				getParameterTranslations().addAll((Collection<? extends ParameterTranslation>)newValue);
				return;
			case MncModelPackage.RESPONSE_TRANSLATION_RULE__OPERATION:
				setOperation((AbstractOperationableItems)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MncModelPackage.RESPONSE_TRANSLATION_RULE__INPUT_RESPONSES:
				getInputResponses().clear();
				return;
			case MncModelPackage.RESPONSE_TRANSLATION_RULE__TRANSLATED_RESPONSE:
				setTranslatedResponse((Response)null);
				return;
			case MncModelPackage.RESPONSE_TRANSLATION_RULE__PARAMETER_TRANSLATIONS:
				getParameterTranslations().clear();
				return;
			case MncModelPackage.RESPONSE_TRANSLATION_RULE__OPERATION:
				setOperation((AbstractOperationableItems)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MncModelPackage.RESPONSE_TRANSLATION_RULE__INPUT_RESPONSES:
				return inputResponses != null && !inputResponses.isEmpty();
			case MncModelPackage.RESPONSE_TRANSLATION_RULE__TRANSLATED_RESPONSE:
				return translatedResponse != null;
			case MncModelPackage.RESPONSE_TRANSLATION_RULE__PARAMETER_TRANSLATIONS:
				return parameterTranslations != null && !parameterTranslations.isEmpty();
			case MncModelPackage.RESPONSE_TRANSLATION_RULE__OPERATION:
				return operation != null;
		}
		return super.eIsSet(featureID);
	}

} //ResponseTranslationRuleImpl
