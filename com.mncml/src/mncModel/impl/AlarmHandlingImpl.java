/**
 */
package mncModel.impl;

import mncModel.Action;
import mncModel.AlarmHandling;
import mncModel.MncModelPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Alarm Handling</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link mncModel.impl.AlarmHandlingImpl#getTriggerAction <em>Trigger Action</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AlarmHandlingImpl extends MinimalEObjectImpl.Container implements AlarmHandling {
	/**
	 * The cached value of the '{@link #getTriggerAction() <em>Trigger Action</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTriggerAction()
	 * @generated
	 * @ordered
	 */
	protected Action triggerAction;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AlarmHandlingImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MncModelPackage.Literals.ALARM_HANDLING;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Action getTriggerAction() {
		return triggerAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTriggerAction(Action newTriggerAction, NotificationChain msgs) {
		Action oldTriggerAction = triggerAction;
		triggerAction = newTriggerAction;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MncModelPackage.ALARM_HANDLING__TRIGGER_ACTION, oldTriggerAction, newTriggerAction);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTriggerAction(Action newTriggerAction) {
		if (newTriggerAction != triggerAction) {
			NotificationChain msgs = null;
			if (triggerAction != null)
				msgs = ((InternalEObject)triggerAction).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.ALARM_HANDLING__TRIGGER_ACTION, null, msgs);
			if (newTriggerAction != null)
				msgs = ((InternalEObject)newTriggerAction).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.ALARM_HANDLING__TRIGGER_ACTION, null, msgs);
			msgs = basicSetTriggerAction(newTriggerAction, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MncModelPackage.ALARM_HANDLING__TRIGGER_ACTION, newTriggerAction, newTriggerAction));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MncModelPackage.ALARM_HANDLING__TRIGGER_ACTION:
				return basicSetTriggerAction(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MncModelPackage.ALARM_HANDLING__TRIGGER_ACTION:
				return getTriggerAction();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MncModelPackage.ALARM_HANDLING__TRIGGER_ACTION:
				setTriggerAction((Action)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MncModelPackage.ALARM_HANDLING__TRIGGER_ACTION:
				setTriggerAction((Action)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MncModelPackage.ALARM_HANDLING__TRIGGER_ACTION:
				return triggerAction != null;
		}
		return super.eIsSet(featureID);
	}

} //AlarmHandlingImpl
