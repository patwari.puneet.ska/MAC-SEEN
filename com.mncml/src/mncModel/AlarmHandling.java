/**
 */
package mncModel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Alarm Handling</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mncModel.AlarmHandling#getTriggerAction <em>Trigger Action</em>}</li>
 * </ul>
 *
 * @see mncModel.MncModelPackage#getAlarmHandling()
 * @model
 * @generated
 */
public interface AlarmHandling extends EObject {
	/**
	 * Returns the value of the '<em><b>Trigger Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Trigger Action</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Trigger Action</em>' containment reference.
	 * @see #setTriggerAction(Action)
	 * @see mncModel.MncModelPackage#getAlarmHandling_TriggerAction()
	 * @model containment="true"
	 * @generated
	 */
	Action getTriggerAction();

	/**
	 * Sets the value of the '{@link mncModel.AlarmHandling#getTriggerAction <em>Trigger Action</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Trigger Action</em>' containment reference.
	 * @see #getTriggerAction()
	 * @generated
	 */
	void setTriggerAction(Action value);

} // AlarmHandling
