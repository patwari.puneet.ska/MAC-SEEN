/**
 */
package mncModel.util;

import mncModel.AbstractInterfaceItems;
import mncModel.AbstractOperationableItems;
import mncModel.Action;
import mncModel.Address;
import mncModel.Alarm;
import mncModel.AlarmBlock;
import mncModel.AlarmHandling;
import mncModel.AlarmTranslation;
import mncModel.AlarmTranslationRule;
import mncModel.AlarmTriggerCondition;
import mncModel.ArrayType;
import mncModel.BoolValue;
import mncModel.CheckParameterCondition;
import mncModel.Command;
import mncModel.CommandDistribution;
import mncModel.CommandResponseBlock;
import mncModel.CommandTranslation;
import mncModel.CommandTriggerCondition;
import mncModel.CommandValidation;
import mncModel.ControlNode;
import mncModel.DataPoint;
import mncModel.DataPointBlock;
import mncModel.DataPointHandling;
import mncModel.DataPointTranslation;
import mncModel.DataPointTranslationRule;
import mncModel.DataPointTriggerCondition;
import mncModel.DataPointValidCondition;
import mncModel.Event;
import mncModel.EventBlock;
import mncModel.EventHandling;
import mncModel.EventTranslation;
import mncModel.EventTranslationRule;
import mncModel.EventTriggerCondition;
import mncModel.FloatValue;
import mncModel.Import;
import mncModel.IntValue;
import mncModel.InterfaceDescription;
import mncModel.MncModelPackage;
import mncModel.Model;
import mncModel.OperatingState;
import mncModel.Operation;
import mncModel.Parameter;
import mncModel.ParameterTranslation;
import mncModel.Port;
import mncModel.PrimitiveValue;
import mncModel.Publish;
import mncModel.Response;
import mncModel.ResponseBlock;
import mncModel.ResponseDistribution;
import mncModel.ResponseTranslation;
import mncModel.ResponseTranslationRule;
import mncModel.ResponseValidation;
import mncModel.ResponsibleItemList;
import mncModel.SimpleType;
import mncModel.StringValue;
import mncModel.SubscribableItemList;
import mncModel.SubscribableItems;
import mncModel.Transition;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see mncModel.MncModelPackage
 * @generated
 */
public class MncModelAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static MncModelPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MncModelAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = MncModelPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MncModelSwitch<Adapter> modelSwitch =
		new MncModelSwitch<Adapter>() {
			@Override
			public Adapter caseModel(Model object) {
				return createModelAdapter();
			}
			@Override
			public Adapter caseImport(Import object) {
				return createImportAdapter();
			}
			@Override
			public Adapter caseSystem(mncModel.System object) {
				return createSystemAdapter();
			}
			@Override
			public Adapter caseControlNode(ControlNode object) {
				return createControlNodeAdapter();
			}
			@Override
			public Adapter caseInterfaceDescription(InterfaceDescription object) {
				return createInterfaceDescriptionAdapter();
			}
			@Override
			public Adapter caseAbstractInterfaceItems(AbstractInterfaceItems object) {
				return createAbstractInterfaceItemsAdapter();
			}
			@Override
			public Adapter caseAbstractOperationableItems(AbstractOperationableItems object) {
				return createAbstractOperationableItemsAdapter();
			}
			@Override
			public Adapter caseOperatingState(OperatingState object) {
				return createOperatingStateAdapter();
			}
			@Override
			public Adapter caseCommandResponseBlock(CommandResponseBlock object) {
				return createCommandResponseBlockAdapter();
			}
			@Override
			public Adapter caseCommand(Command object) {
				return createCommandAdapter();
			}
			@Override
			public Adapter caseCommandValidation(CommandValidation object) {
				return createCommandValidationAdapter();
			}
			@Override
			public Adapter caseCommandDistribution(CommandDistribution object) {
				return createCommandDistributionAdapter();
			}
			@Override
			public Adapter caseCommandTranslation(CommandTranslation object) {
				return createCommandTranslationAdapter();
			}
			@Override
			public Adapter caseCommandTriggerCondition(CommandTriggerCondition object) {
				return createCommandTriggerConditionAdapter();
			}
			@Override
			public Adapter caseParameterTranslation(ParameterTranslation object) {
				return createParameterTranslationAdapter();
			}
			@Override
			public Adapter caseTransition(Transition object) {
				return createTransitionAdapter();
			}
			@Override
			public Adapter caseResponse(Response object) {
				return createResponseAdapter();
			}
			@Override
			public Adapter caseResponseBlock(ResponseBlock object) {
				return createResponseBlockAdapter();
			}
			@Override
			public Adapter caseResponseValidation(ResponseValidation object) {
				return createResponseValidationAdapter();
			}
			@Override
			public Adapter caseResponseDistribution(ResponseDistribution object) {
				return createResponseDistributionAdapter();
			}
			@Override
			public Adapter caseResponseTranslation(ResponseTranslation object) {
				return createResponseTranslationAdapter();
			}
			@Override
			public Adapter caseResponseTranslationRule(ResponseTranslationRule object) {
				return createResponseTranslationRuleAdapter();
			}
			@Override
			public Adapter casePort(Port object) {
				return createPortAdapter();
			}
			@Override
			public Adapter caseEventBlock(EventBlock object) {
				return createEventBlockAdapter();
			}
			@Override
			public Adapter caseEvent(Event object) {
				return createEventAdapter();
			}
			@Override
			public Adapter caseEventTriggerCondition(EventTriggerCondition object) {
				return createEventTriggerConditionAdapter();
			}
			@Override
			public Adapter caseEventTranslation(EventTranslation object) {
				return createEventTranslationAdapter();
			}
			@Override
			public Adapter caseEventTranslationRule(EventTranslationRule object) {
				return createEventTranslationRuleAdapter();
			}
			@Override
			public Adapter caseEventHandling(EventHandling object) {
				return createEventHandlingAdapter();
			}
			@Override
			public Adapter caseAlarmBlock(AlarmBlock object) {
				return createAlarmBlockAdapter();
			}
			@Override
			public Adapter caseAlarm(Alarm object) {
				return createAlarmAdapter();
			}
			@Override
			public Adapter caseAlarmTriggerCondition(AlarmTriggerCondition object) {
				return createAlarmTriggerConditionAdapter();
			}
			@Override
			public Adapter caseAlarmTranslation(AlarmTranslation object) {
				return createAlarmTranslationAdapter();
			}
			@Override
			public Adapter caseAlarmTranslationRule(AlarmTranslationRule object) {
				return createAlarmTranslationRuleAdapter();
			}
			@Override
			public Adapter caseAlarmHandling(AlarmHandling object) {
				return createAlarmHandlingAdapter();
			}
			@Override
			public Adapter caseDataPointBlock(DataPointBlock object) {
				return createDataPointBlockAdapter();
			}
			@Override
			public Adapter caseDataPoint(DataPoint object) {
				return createDataPointAdapter();
			}
			@Override
			public Adapter caseDataPointTriggerCondition(DataPointTriggerCondition object) {
				return createDataPointTriggerConditionAdapter();
			}
			@Override
			public Adapter caseDataPointTranslation(DataPointTranslation object) {
				return createDataPointTranslationAdapter();
			}
			@Override
			public Adapter caseDataPointTranslationRule(DataPointTranslationRule object) {
				return createDataPointTranslationRuleAdapter();
			}
			@Override
			public Adapter caseDataPointHandling(DataPointHandling object) {
				return createDataPointHandlingAdapter();
			}
			@Override
			public Adapter caseDataPointValidCondition(DataPointValidCondition object) {
				return createDataPointValidConditionAdapter();
			}
			@Override
			public Adapter caseParameter(Parameter object) {
				return createParameterAdapter();
			}
			@Override
			public Adapter caseCheckParameterCondition(CheckParameterCondition object) {
				return createCheckParameterConditionAdapter();
			}
			@Override
			public Adapter caseArrayType(ArrayType object) {
				return createArrayTypeAdapter();
			}
			@Override
			public Adapter caseSimpleType(SimpleType object) {
				return createSimpleTypeAdapter();
			}
			@Override
			public Adapter casePrimitiveValue(PrimitiveValue object) {
				return createPrimitiveValueAdapter();
			}
			@Override
			public Adapter caseIntValue(IntValue object) {
				return createIntValueAdapter();
			}
			@Override
			public Adapter caseBoolValue(BoolValue object) {
				return createBoolValueAdapter();
			}
			@Override
			public Adapter caseFloatValue(FloatValue object) {
				return createFloatValueAdapter();
			}
			@Override
			public Adapter caseStringValue(StringValue object) {
				return createStringValueAdapter();
			}
			@Override
			public Adapter caseOperation(Operation object) {
				return createOperationAdapter();
			}
			@Override
			public Adapter caseAddress(Address object) {
				return createAddressAdapter();
			}
			@Override
			public Adapter caseAction(Action object) {
				return createActionAdapter();
			}
			@Override
			public Adapter casePublish(Publish object) {
				return createPublishAdapter();
			}
			@Override
			public Adapter caseSubscribableItems(SubscribableItems object) {
				return createSubscribableItemsAdapter();
			}
			@Override
			public Adapter caseSubscribableItemList(SubscribableItemList object) {
				return createSubscribableItemListAdapter();
			}
			@Override
			public Adapter caseResponsibleItemList(ResponsibleItemList object) {
				return createResponsibleItemListAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link mncModel.Model <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.Model
	 * @generated
	 */
	public Adapter createModelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.Import <em>Import</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.Import
	 * @generated
	 */
	public Adapter createImportAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.System <em>System</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.System
	 * @generated
	 */
	public Adapter createSystemAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.ControlNode <em>Control Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.ControlNode
	 * @generated
	 */
	public Adapter createControlNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.InterfaceDescription <em>Interface Description</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.InterfaceDescription
	 * @generated
	 */
	public Adapter createInterfaceDescriptionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.AbstractInterfaceItems <em>Abstract Interface Items</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.AbstractInterfaceItems
	 * @generated
	 */
	public Adapter createAbstractInterfaceItemsAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.AbstractOperationableItems <em>Abstract Operationable Items</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.AbstractOperationableItems
	 * @generated
	 */
	public Adapter createAbstractOperationableItemsAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.OperatingState <em>Operating State</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.OperatingState
	 * @generated
	 */
	public Adapter createOperatingStateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.CommandResponseBlock <em>Command Response Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.CommandResponseBlock
	 * @generated
	 */
	public Adapter createCommandResponseBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.Command <em>Command</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.Command
	 * @generated
	 */
	public Adapter createCommandAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.CommandValidation <em>Command Validation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.CommandValidation
	 * @generated
	 */
	public Adapter createCommandValidationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.CommandDistribution <em>Command Distribution</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.CommandDistribution
	 * @generated
	 */
	public Adapter createCommandDistributionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.CommandTranslation <em>Command Translation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.CommandTranslation
	 * @generated
	 */
	public Adapter createCommandTranslationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.CommandTriggerCondition <em>Command Trigger Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.CommandTriggerCondition
	 * @generated
	 */
	public Adapter createCommandTriggerConditionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.ParameterTranslation <em>Parameter Translation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.ParameterTranslation
	 * @generated
	 */
	public Adapter createParameterTranslationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.Transition <em>Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.Transition
	 * @generated
	 */
	public Adapter createTransitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.Response <em>Response</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.Response
	 * @generated
	 */
	public Adapter createResponseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.ResponseBlock <em>Response Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.ResponseBlock
	 * @generated
	 */
	public Adapter createResponseBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.ResponseValidation <em>Response Validation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.ResponseValidation
	 * @generated
	 */
	public Adapter createResponseValidationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.ResponseDistribution <em>Response Distribution</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.ResponseDistribution
	 * @generated
	 */
	public Adapter createResponseDistributionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.ResponseTranslation <em>Response Translation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.ResponseTranslation
	 * @generated
	 */
	public Adapter createResponseTranslationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.ResponseTranslationRule <em>Response Translation Rule</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.ResponseTranslationRule
	 * @generated
	 */
	public Adapter createResponseTranslationRuleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.Port <em>Port</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.Port
	 * @generated
	 */
	public Adapter createPortAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.EventBlock <em>Event Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.EventBlock
	 * @generated
	 */
	public Adapter createEventBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.Event <em>Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.Event
	 * @generated
	 */
	public Adapter createEventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.EventTriggerCondition <em>Event Trigger Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.EventTriggerCondition
	 * @generated
	 */
	public Adapter createEventTriggerConditionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.EventTranslation <em>Event Translation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.EventTranslation
	 * @generated
	 */
	public Adapter createEventTranslationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.EventTranslationRule <em>Event Translation Rule</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.EventTranslationRule
	 * @generated
	 */
	public Adapter createEventTranslationRuleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.EventHandling <em>Event Handling</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.EventHandling
	 * @generated
	 */
	public Adapter createEventHandlingAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.AlarmBlock <em>Alarm Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.AlarmBlock
	 * @generated
	 */
	public Adapter createAlarmBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.Alarm <em>Alarm</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.Alarm
	 * @generated
	 */
	public Adapter createAlarmAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.AlarmTriggerCondition <em>Alarm Trigger Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.AlarmTriggerCondition
	 * @generated
	 */
	public Adapter createAlarmTriggerConditionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.AlarmTranslation <em>Alarm Translation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.AlarmTranslation
	 * @generated
	 */
	public Adapter createAlarmTranslationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.AlarmTranslationRule <em>Alarm Translation Rule</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.AlarmTranslationRule
	 * @generated
	 */
	public Adapter createAlarmTranslationRuleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.AlarmHandling <em>Alarm Handling</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.AlarmHandling
	 * @generated
	 */
	public Adapter createAlarmHandlingAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.DataPointBlock <em>Data Point Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.DataPointBlock
	 * @generated
	 */
	public Adapter createDataPointBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.DataPoint <em>Data Point</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.DataPoint
	 * @generated
	 */
	public Adapter createDataPointAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.DataPointTriggerCondition <em>Data Point Trigger Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.DataPointTriggerCondition
	 * @generated
	 */
	public Adapter createDataPointTriggerConditionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.DataPointTranslation <em>Data Point Translation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.DataPointTranslation
	 * @generated
	 */
	public Adapter createDataPointTranslationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.DataPointTranslationRule <em>Data Point Translation Rule</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.DataPointTranslationRule
	 * @generated
	 */
	public Adapter createDataPointTranslationRuleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.DataPointHandling <em>Data Point Handling</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.DataPointHandling
	 * @generated
	 */
	public Adapter createDataPointHandlingAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.DataPointValidCondition <em>Data Point Valid Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.DataPointValidCondition
	 * @generated
	 */
	public Adapter createDataPointValidConditionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.Parameter <em>Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.Parameter
	 * @generated
	 */
	public Adapter createParameterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.CheckParameterCondition <em>Check Parameter Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.CheckParameterCondition
	 * @generated
	 */
	public Adapter createCheckParameterConditionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.ArrayType <em>Array Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.ArrayType
	 * @generated
	 */
	public Adapter createArrayTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.SimpleType <em>Simple Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.SimpleType
	 * @generated
	 */
	public Adapter createSimpleTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.PrimitiveValue <em>Primitive Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.PrimitiveValue
	 * @generated
	 */
	public Adapter createPrimitiveValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.IntValue <em>Int Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.IntValue
	 * @generated
	 */
	public Adapter createIntValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.BoolValue <em>Bool Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.BoolValue
	 * @generated
	 */
	public Adapter createBoolValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.FloatValue <em>Float Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.FloatValue
	 * @generated
	 */
	public Adapter createFloatValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.StringValue <em>String Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.StringValue
	 * @generated
	 */
	public Adapter createStringValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.Operation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.Operation
	 * @generated
	 */
	public Adapter createOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.Address <em>Address</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.Address
	 * @generated
	 */
	public Adapter createAddressAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.Action <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.Action
	 * @generated
	 */
	public Adapter createActionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.Publish <em>Publish</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.Publish
	 * @generated
	 */
	public Adapter createPublishAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.SubscribableItems <em>Subscribable Items</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.SubscribableItems
	 * @generated
	 */
	public Adapter createSubscribableItemsAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.SubscribableItemList <em>Subscribable Item List</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.SubscribableItemList
	 * @generated
	 */
	public Adapter createSubscribableItemListAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.ResponsibleItemList <em>Responsible Item List</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.ResponsibleItemList
	 * @generated
	 */
	public Adapter createResponsibleItemListAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //MncModelAdapterFactory
