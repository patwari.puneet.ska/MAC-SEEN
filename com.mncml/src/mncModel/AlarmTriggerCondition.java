/**
 */
package mncModel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Alarm Trigger Condition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mncModel.AlarmTriggerCondition#getAlarmTranslation <em>Alarm Translation</em>}</li>
 *   <li>{@link mncModel.AlarmTriggerCondition#getOperation <em>Operation</em>}</li>
 *   <li>{@link mncModel.AlarmTriggerCondition#getResponsibleItems <em>Responsible Items</em>}</li>
 * </ul>
 *
 * @see mncModel.MncModelPackage#getAlarmTriggerCondition()
 * @model
 * @generated
 */
public interface AlarmTriggerCondition extends EObject {
	/**
	 * Returns the value of the '<em><b>Alarm Translation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Alarm Translation</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Alarm Translation</em>' containment reference.
	 * @see #setAlarmTranslation(AlarmTranslation)
	 * @see mncModel.MncModelPackage#getAlarmTriggerCondition_AlarmTranslation()
	 * @model containment="true"
	 * @generated
	 */
	AlarmTranslation getAlarmTranslation();

	/**
	 * Sets the value of the '{@link mncModel.AlarmTriggerCondition#getAlarmTranslation <em>Alarm Translation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Alarm Translation</em>' containment reference.
	 * @see #getAlarmTranslation()
	 * @generated
	 */
	void setAlarmTranslation(AlarmTranslation value);

	/**
	 * Returns the value of the '<em><b>Operation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operation</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operation</em>' containment reference.
	 * @see #setOperation(AbstractOperationableItems)
	 * @see mncModel.MncModelPackage#getAlarmTriggerCondition_Operation()
	 * @model containment="true"
	 * @generated
	 */
	AbstractOperationableItems getOperation();

	/**
	 * Sets the value of the '{@link mncModel.AlarmTriggerCondition#getOperation <em>Operation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operation</em>' containment reference.
	 * @see #getOperation()
	 * @generated
	 */
	void setOperation(AbstractOperationableItems value);

	/**
	 * Returns the value of the '<em><b>Responsible Items</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Responsible Items</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Responsible Items</em>' containment reference.
	 * @see #setResponsibleItems(ResponsibleItemList)
	 * @see mncModel.MncModelPackage#getAlarmTriggerCondition_ResponsibleItems()
	 * @model containment="true"
	 * @generated
	 */
	ResponsibleItemList getResponsibleItems();

	/**
	 * Sets the value of the '{@link mncModel.AlarmTriggerCondition#getResponsibleItems <em>Responsible Items</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Responsible Items</em>' containment reference.
	 * @see #getResponsibleItems()
	 * @generated
	 */
	void setResponsibleItems(ResponsibleItemList value);

} // AlarmTriggerCondition
