/**
 */
package mncModel.stakeholder.impl;

import mncModel.stakeholder.StakeholderPackage;
import mncModel.stakeholder.UserOperation;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>User Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class UserOperationImpl extends MinimalEObjectImpl.Container implements UserOperation {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UserOperationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StakeholderPackage.Literals.USER_OPERATION;
	}

} //UserOperationImpl
