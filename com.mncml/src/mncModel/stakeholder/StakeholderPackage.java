/**
 */
package mncModel.stakeholder;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see mncModel.stakeholder.StakeholderFactory
 * @model kind="package"
 * @generated
 */
public interface StakeholderPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "stakeholder";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://mncModel/stakeholder/2.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "stakeholder";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	StakeholderPackage eINSTANCE = mncModel.stakeholder.impl.StakeholderPackageImpl.init();

	/**
	 * The meta object id for the '{@link mncModel.stakeholder.impl.RoleImpl <em>Role</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.stakeholder.impl.RoleImpl
	 * @see mncModel.stakeholder.impl.StakeholderPackageImpl#getRole()
	 * @generated
	 */
	int ROLE = 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE__DESCRIPTION = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE__NAME = 1;

	/**
	 * The number of structural features of the '<em>Role</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link mncModel.stakeholder.impl.UserImpl <em>User</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.stakeholder.impl.UserImpl
	 * @see mncModel.stakeholder.impl.StakeholderPackageImpl#getUser()
	 * @generated
	 */
	int USER = 1;

	/**
	 * The feature id for the '<em><b>Password</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER__PASSWORD = 0;

	/**
	 * The feature id for the '<em><b>Role</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER__ROLE = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER__NAME = 2;

	/**
	 * The number of structural features of the '<em>User</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link mncModel.stakeholder.impl.UserOperationImpl <em>User Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.stakeholder.impl.UserOperationImpl
	 * @see mncModel.stakeholder.impl.StakeholderPackageImpl#getUserOperation()
	 * @generated
	 */
	int USER_OPERATION = 2;

	/**
	 * The number of structural features of the '<em>User Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_OPERATION_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link mncModel.stakeholder.impl.AlarmAcknowledgementImpl <em>Alarm Acknowledgement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.stakeholder.impl.AlarmAcknowledgementImpl
	 * @see mncModel.stakeholder.impl.StakeholderPackageImpl#getAlarmAcknowledgement()
	 * @generated
	 */
	int ALARM_ACKNOWLEDGEMENT = 3;

	/**
	 * The feature id for the '<em><b>Alarms</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALARM_ACKNOWLEDGEMENT__ALARMS = USER_OPERATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Alarm Acknowledgement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALARM_ACKNOWLEDGEMENT_FEATURE_COUNT = USER_OPERATION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link mncModel.stakeholder.impl.SystemControlImpl <em>System Control</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.stakeholder.impl.SystemControlImpl
	 * @see mncModel.stakeholder.impl.StakeholderPackageImpl#getSystemControl()
	 * @generated
	 */
	int SYSTEM_CONTROL = 4;

	/**
	 * The feature id for the '<em><b>Control Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_CONTROL__CONTROL_NODES = USER_OPERATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>System Control</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_CONTROL_FEATURE_COUNT = USER_OPERATION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link mncModel.stakeholder.impl.StakeholderImpl <em>Stakeholder</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.stakeholder.impl.StakeholderImpl
	 * @see mncModel.stakeholder.impl.StakeholderPackageImpl#getStakeholder()
	 * @generated
	 */
	int STAKEHOLDER = 5;

	/**
	 * The feature id for the '<em><b>Roles</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STAKEHOLDER__ROLES = 0;

	/**
	 * The feature id for the '<em><b>Users</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STAKEHOLDER__USERS = 1;

	/**
	 * The number of structural features of the '<em>Stakeholder</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STAKEHOLDER_FEATURE_COUNT = 2;


	/**
	 * Returns the meta object for class '{@link mncModel.stakeholder.Role <em>Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Role</em>'.
	 * @see mncModel.stakeholder.Role
	 * @generated
	 */
	EClass getRole();

	/**
	 * Returns the meta object for the attribute '{@link mncModel.stakeholder.Role#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see mncModel.stakeholder.Role#getDescription()
	 * @see #getRole()
	 * @generated
	 */
	EAttribute getRole_Description();

	/**
	 * Returns the meta object for the attribute '{@link mncModel.stakeholder.Role#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see mncModel.stakeholder.Role#getName()
	 * @see #getRole()
	 * @generated
	 */
	EAttribute getRole_Name();

	/**
	 * Returns the meta object for class '{@link mncModel.stakeholder.User <em>User</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>User</em>'.
	 * @see mncModel.stakeholder.User
	 * @generated
	 */
	EClass getUser();

	/**
	 * Returns the meta object for the attribute '{@link mncModel.stakeholder.User#getPassword <em>Password</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Password</em>'.
	 * @see mncModel.stakeholder.User#getPassword()
	 * @see #getUser()
	 * @generated
	 */
	EAttribute getUser_Password();

	/**
	 * Returns the meta object for the reference '{@link mncModel.stakeholder.User#getRole <em>Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Role</em>'.
	 * @see mncModel.stakeholder.User#getRole()
	 * @see #getUser()
	 * @generated
	 */
	EReference getUser_Role();

	/**
	 * Returns the meta object for the attribute '{@link mncModel.stakeholder.User#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see mncModel.stakeholder.User#getName()
	 * @see #getUser()
	 * @generated
	 */
	EAttribute getUser_Name();

	/**
	 * Returns the meta object for class '{@link mncModel.stakeholder.UserOperation <em>User Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>User Operation</em>'.
	 * @see mncModel.stakeholder.UserOperation
	 * @generated
	 */
	EClass getUserOperation();

	/**
	 * Returns the meta object for class '{@link mncModel.stakeholder.AlarmAcknowledgement <em>Alarm Acknowledgement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Alarm Acknowledgement</em>'.
	 * @see mncModel.stakeholder.AlarmAcknowledgement
	 * @generated
	 */
	EClass getAlarmAcknowledgement();

	/**
	 * Returns the meta object for the reference list '{@link mncModel.stakeholder.AlarmAcknowledgement#getAlarms <em>Alarms</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Alarms</em>'.
	 * @see mncModel.stakeholder.AlarmAcknowledgement#getAlarms()
	 * @see #getAlarmAcknowledgement()
	 * @generated
	 */
	EReference getAlarmAcknowledgement_Alarms();

	/**
	 * Returns the meta object for class '{@link mncModel.stakeholder.SystemControl <em>System Control</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>System Control</em>'.
	 * @see mncModel.stakeholder.SystemControl
	 * @generated
	 */
	EClass getSystemControl();

	/**
	 * Returns the meta object for the reference list '{@link mncModel.stakeholder.SystemControl#getControlNodes <em>Control Nodes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Control Nodes</em>'.
	 * @see mncModel.stakeholder.SystemControl#getControlNodes()
	 * @see #getSystemControl()
	 * @generated
	 */
	EReference getSystemControl_ControlNodes();

	/**
	 * Returns the meta object for class '{@link mncModel.stakeholder.Stakeholder <em>Stakeholder</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Stakeholder</em>'.
	 * @see mncModel.stakeholder.Stakeholder
	 * @generated
	 */
	EClass getStakeholder();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.stakeholder.Stakeholder#getRoles <em>Roles</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Roles</em>'.
	 * @see mncModel.stakeholder.Stakeholder#getRoles()
	 * @see #getStakeholder()
	 * @generated
	 */
	EReference getStakeholder_Roles();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.stakeholder.Stakeholder#getUsers <em>Users</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Users</em>'.
	 * @see mncModel.stakeholder.Stakeholder#getUsers()
	 * @see #getStakeholder()
	 * @generated
	 */
	EReference getStakeholder_Users();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	StakeholderFactory getStakeholderFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link mncModel.stakeholder.impl.RoleImpl <em>Role</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.stakeholder.impl.RoleImpl
		 * @see mncModel.stakeholder.impl.StakeholderPackageImpl#getRole()
		 * @generated
		 */
		EClass ROLE = eINSTANCE.getRole();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROLE__DESCRIPTION = eINSTANCE.getRole_Description();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROLE__NAME = eINSTANCE.getRole_Name();

		/**
		 * The meta object literal for the '{@link mncModel.stakeholder.impl.UserImpl <em>User</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.stakeholder.impl.UserImpl
		 * @see mncModel.stakeholder.impl.StakeholderPackageImpl#getUser()
		 * @generated
		 */
		EClass USER = eINSTANCE.getUser();

		/**
		 * The meta object literal for the '<em><b>Password</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USER__PASSWORD = eINSTANCE.getUser_Password();

		/**
		 * The meta object literal for the '<em><b>Role</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference USER__ROLE = eINSTANCE.getUser_Role();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USER__NAME = eINSTANCE.getUser_Name();

		/**
		 * The meta object literal for the '{@link mncModel.stakeholder.impl.UserOperationImpl <em>User Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.stakeholder.impl.UserOperationImpl
		 * @see mncModel.stakeholder.impl.StakeholderPackageImpl#getUserOperation()
		 * @generated
		 */
		EClass USER_OPERATION = eINSTANCE.getUserOperation();

		/**
		 * The meta object literal for the '{@link mncModel.stakeholder.impl.AlarmAcknowledgementImpl <em>Alarm Acknowledgement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.stakeholder.impl.AlarmAcknowledgementImpl
		 * @see mncModel.stakeholder.impl.StakeholderPackageImpl#getAlarmAcknowledgement()
		 * @generated
		 */
		EClass ALARM_ACKNOWLEDGEMENT = eINSTANCE.getAlarmAcknowledgement();

		/**
		 * The meta object literal for the '<em><b>Alarms</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ALARM_ACKNOWLEDGEMENT__ALARMS = eINSTANCE.getAlarmAcknowledgement_Alarms();

		/**
		 * The meta object literal for the '{@link mncModel.stakeholder.impl.SystemControlImpl <em>System Control</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.stakeholder.impl.SystemControlImpl
		 * @see mncModel.stakeholder.impl.StakeholderPackageImpl#getSystemControl()
		 * @generated
		 */
		EClass SYSTEM_CONTROL = eINSTANCE.getSystemControl();

		/**
		 * The meta object literal for the '<em><b>Control Nodes</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SYSTEM_CONTROL__CONTROL_NODES = eINSTANCE.getSystemControl_ControlNodes();

		/**
		 * The meta object literal for the '{@link mncModel.stakeholder.impl.StakeholderImpl <em>Stakeholder</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.stakeholder.impl.StakeholderImpl
		 * @see mncModel.stakeholder.impl.StakeholderPackageImpl#getStakeholder()
		 * @generated
		 */
		EClass STAKEHOLDER = eINSTANCE.getStakeholder();

		/**
		 * The meta object literal for the '<em><b>Roles</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STAKEHOLDER__ROLES = eINSTANCE.getStakeholder_Roles();

		/**
		 * The meta object literal for the '<em><b>Users</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STAKEHOLDER__USERS = eINSTANCE.getStakeholder_Users();

	}

} //StakeholderPackage
