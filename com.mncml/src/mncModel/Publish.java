/**
 */
package mncModel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Publish</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see mncModel.MncModelPackage#getPublish()
 * @model
 * @generated
 */
public interface Publish extends Operation {
} // Publish
