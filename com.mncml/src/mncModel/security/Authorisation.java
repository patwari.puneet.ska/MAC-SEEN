/**
 */
package mncModel.security;

import mncModel.stakeholder.Role;
import mncModel.stakeholder.SystemControl;
import mncModel.stakeholder.UserOperation;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Authorisation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mncModel.security.Authorisation#getRole <em>Role</em>}</li>
 *   <li>{@link mncModel.security.Authorisation#getSystemControl <em>System Control</em>}</li>
 *   <li>{@link mncModel.security.Authorisation#getUserOperations <em>User Operations</em>}</li>
 * </ul>
 *
 * @see mncModel.security.SecurityPackage#getAuthorisation()
 * @model
 * @generated
 */
public interface Authorisation extends EObject {
	/**
	 * Returns the value of the '<em><b>Role</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Role</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Role</em>' reference.
	 * @see #setRole(Role)
	 * @see mncModel.security.SecurityPackage#getAuthorisation_Role()
	 * @model
	 * @generated
	 */
	Role getRole();

	/**
	 * Sets the value of the '{@link mncModel.security.Authorisation#getRole <em>Role</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Role</em>' reference.
	 * @see #getRole()
	 * @generated
	 */
	void setRole(Role value);

	/**
	 * Returns the value of the '<em><b>System Control</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>System Control</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>System Control</em>' containment reference.
	 * @see #setSystemControl(SystemControl)
	 * @see mncModel.security.SecurityPackage#getAuthorisation_SystemControl()
	 * @model containment="true"
	 * @generated
	 */
	SystemControl getSystemControl();

	/**
	 * Sets the value of the '{@link mncModel.security.Authorisation#getSystemControl <em>System Control</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>System Control</em>' containment reference.
	 * @see #getSystemControl()
	 * @generated
	 */
	void setSystemControl(SystemControl value);

	/**
	 * Returns the value of the '<em><b>User Operations</b></em>' containment reference list.
	 * The list contents are of type {@link mncModel.stakeholder.UserOperation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>User Operations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>User Operations</em>' containment reference list.
	 * @see mncModel.security.SecurityPackage#getAuthorisation_UserOperations()
	 * @model containment="true"
	 * @generated
	 */
	EList<UserOperation> getUserOperations();

} // Authorisation
