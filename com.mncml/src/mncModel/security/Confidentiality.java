/**
 */
package mncModel.security;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Confidentiality</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see mncModel.security.SecurityPackage#getConfidentiality()
 * @model
 * @generated
 */
public interface Confidentiality extends EObject {
} // Confidentiality
