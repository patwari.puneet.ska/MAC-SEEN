/**
 */
package mncModel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Point Trigger Condition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mncModel.DataPointTriggerCondition#getDataPointTranslation <em>Data Point Translation</em>}</li>
 *   <li>{@link mncModel.DataPointTriggerCondition#getResponsibleItems <em>Responsible Items</em>}</li>
 *   <li>{@link mncModel.DataPointTriggerCondition#getOperation <em>Operation</em>}</li>
 * </ul>
 *
 * @see mncModel.MncModelPackage#getDataPointTriggerCondition()
 * @model
 * @generated
 */
public interface DataPointTriggerCondition extends EObject {
	/**
	 * Returns the value of the '<em><b>Data Point Translation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Point Translation</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Point Translation</em>' containment reference.
	 * @see #setDataPointTranslation(DataPointTranslation)
	 * @see mncModel.MncModelPackage#getDataPointTriggerCondition_DataPointTranslation()
	 * @model containment="true"
	 * @generated
	 */
	DataPointTranslation getDataPointTranslation();

	/**
	 * Sets the value of the '{@link mncModel.DataPointTriggerCondition#getDataPointTranslation <em>Data Point Translation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Point Translation</em>' containment reference.
	 * @see #getDataPointTranslation()
	 * @generated
	 */
	void setDataPointTranslation(DataPointTranslation value);

	/**
	 * Returns the value of the '<em><b>Responsible Items</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Responsible Items</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Responsible Items</em>' containment reference.
	 * @see #setResponsibleItems(ResponsibleItemList)
	 * @see mncModel.MncModelPackage#getDataPointTriggerCondition_ResponsibleItems()
	 * @model containment="true"
	 * @generated
	 */
	ResponsibleItemList getResponsibleItems();

	/**
	 * Sets the value of the '{@link mncModel.DataPointTriggerCondition#getResponsibleItems <em>Responsible Items</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Responsible Items</em>' containment reference.
	 * @see #getResponsibleItems()
	 * @generated
	 */
	void setResponsibleItems(ResponsibleItemList value);

	/**
	 * Returns the value of the '<em><b>Operation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operation</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operation</em>' containment reference.
	 * @see #setOperation(AbstractOperationableItems)
	 * @see mncModel.MncModelPackage#getDataPointTriggerCondition_Operation()
	 * @model containment="true"
	 * @generated
	 */
	AbstractOperationableItems getOperation();

	/**
	 * Sets the value of the '{@link mncModel.DataPointTriggerCondition#getOperation <em>Operation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operation</em>' containment reference.
	 * @see #getOperation()
	 * @generated
	 */
	void setOperation(AbstractOperationableItems value);

} // DataPointTriggerCondition
