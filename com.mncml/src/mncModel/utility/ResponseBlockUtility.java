/**
 */
package mncModel.utility;

import mncModel.ResponseBlock;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Response Block Utility</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mncModel.utility.ResponseBlockUtility#getResponseBlocks <em>Response Blocks</em>}</li>
 * </ul>
 *
 * @see mncModel.utility.UtilityPackage#getResponseBlockUtility()
 * @model
 * @generated
 */
public interface ResponseBlockUtility extends EObject {
	/**
	 * Returns the value of the '<em><b>Response Blocks</b></em>' containment reference list.
	 * The list contents are of type {@link mncModel.ResponseBlock}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Response Blocks</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Response Blocks</em>' containment reference list.
	 * @see mncModel.utility.UtilityPackage#getResponseBlockUtility_ResponseBlocks()
	 * @model containment="true"
	 * @generated
	 */
	EList<ResponseBlock> getResponseBlocks();

} // ResponseBlockUtility
