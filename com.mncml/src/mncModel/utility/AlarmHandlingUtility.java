/**
 */
package mncModel.utility;

import mncModel.AlarmHandling;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Alarm Handling Utility</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mncModel.utility.AlarmHandlingUtility#getAlarmHandling <em>Alarm Handling</em>}</li>
 * </ul>
 *
 * @see mncModel.utility.UtilityPackage#getAlarmHandlingUtility()
 * @model
 * @generated
 */
public interface AlarmHandlingUtility extends EObject {
	/**
	 * Returns the value of the '<em><b>Alarm Handling</b></em>' containment reference list.
	 * The list contents are of type {@link mncModel.AlarmHandling}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Alarm Handling</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Alarm Handling</em>' containment reference list.
	 * @see mncModel.utility.UtilityPackage#getAlarmHandlingUtility_AlarmHandling()
	 * @model containment="true"
	 * @generated
	 */
	EList<AlarmHandling> getAlarmHandling();

} // AlarmHandlingUtility
