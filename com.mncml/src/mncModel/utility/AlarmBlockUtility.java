/**
 */
package mncModel.utility;

import mncModel.AlarmBlock;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Alarm Block Utility</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mncModel.utility.AlarmBlockUtility#getAlarmBlocks <em>Alarm Blocks</em>}</li>
 * </ul>
 *
 * @see mncModel.utility.UtilityPackage#getAlarmBlockUtility()
 * @model
 * @generated
 */
public interface AlarmBlockUtility extends EObject {
	/**
	 * Returns the value of the '<em><b>Alarm Blocks</b></em>' containment reference list.
	 * The list contents are of type {@link mncModel.AlarmBlock}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Alarm Blocks</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Alarm Blocks</em>' containment reference list.
	 * @see mncModel.utility.UtilityPackage#getAlarmBlockUtility_AlarmBlocks()
	 * @model containment="true"
	 * @generated
	 */
	EList<AlarmBlock> getAlarmBlocks();

} // AlarmBlockUtility
