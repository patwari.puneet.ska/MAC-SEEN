/**
 */
package mncModel.utility.util;

import mncModel.utility.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see mncModel.utility.UtilityPackage
 * @generated
 */
public class UtilitySwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static UtilityPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UtilitySwitch() {
		if (modelPackage == null) {
			modelPackage = UtilityPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case UtilityPackage.COMMAND_VALIDATION_UTILITY: {
				CommandValidationUtility commandValidationUtility = (CommandValidationUtility)theEObject;
				T result = caseCommandValidationUtility(commandValidationUtility);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UtilityPackage.ALARM_HANDLING_UTILITY: {
				AlarmHandlingUtility alarmHandlingUtility = (AlarmHandlingUtility)theEObject;
				T result = caseAlarmHandlingUtility(alarmHandlingUtility);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UtilityPackage.DATA_POINT_HANDLING_UTILITY: {
				DataPointHandlingUtility dataPointHandlingUtility = (DataPointHandlingUtility)theEObject;
				T result = caseDataPointHandlingUtility(dataPointHandlingUtility);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UtilityPackage.OPERATION_UTILITY: {
				OperationUtility operationUtility = (OperationUtility)theEObject;
				T result = caseOperationUtility(operationUtility);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UtilityPackage.COMMAND_UTILITY: {
				CommandUtility commandUtility = (CommandUtility)theEObject;
				T result = caseCommandUtility(commandUtility);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UtilityPackage.RESPONSE_VALIDATION_UTILITY: {
				ResponseValidationUtility responseValidationUtility = (ResponseValidationUtility)theEObject;
				T result = caseResponseValidationUtility(responseValidationUtility);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UtilityPackage.ALARM_CONDITION_UTILITY: {
				AlarmConditionUtility alarmConditionUtility = (AlarmConditionUtility)theEObject;
				T result = caseAlarmConditionUtility(alarmConditionUtility);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UtilityPackage.DATA_POINT_UTILITY: {
				DataPointUtility dataPointUtility = (DataPointUtility)theEObject;
				T result = caseDataPointUtility(dataPointUtility);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UtilityPackage.ALARM_UTILITY: {
				AlarmUtility alarmUtility = (AlarmUtility)theEObject;
				T result = caseAlarmUtility(alarmUtility);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UtilityPackage.EVENT_UTILITY: {
				EventUtility eventUtility = (EventUtility)theEObject;
				T result = caseEventUtility(eventUtility);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UtilityPackage.RESPONSE_UTILITY: {
				ResponseUtility responseUtility = (ResponseUtility)theEObject;
				T result = caseResponseUtility(responseUtility);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UtilityPackage.STATE_UTILITY: {
				StateUtility stateUtility = (StateUtility)theEObject;
				T result = caseStateUtility(stateUtility);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UtilityPackage.AUTHORISATION_UTILITY: {
				AuthorisationUtility authorisationUtility = (AuthorisationUtility)theEObject;
				T result = caseAuthorisationUtility(authorisationUtility);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UtilityPackage.ROLE_UTILITY: {
				RoleUtility roleUtility = (RoleUtility)theEObject;
				T result = caseRoleUtility(roleUtility);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UtilityPackage.USER_UTILITY: {
				UserUtility userUtility = (UserUtility)theEObject;
				T result = caseUserUtility(userUtility);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UtilityPackage.TRANSITION_UTILITY: {
				TransitionUtility transitionUtility = (TransitionUtility)theEObject;
				T result = caseTransitionUtility(transitionUtility);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UtilityPackage.COMMAND_RESPONSE_BLOCK_UTILITY: {
				CommandResponseBlockUtility commandResponseBlockUtility = (CommandResponseBlockUtility)theEObject;
				T result = caseCommandResponseBlockUtility(commandResponseBlockUtility);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UtilityPackage.RESPONSE_BLOCK_UTILITY: {
				ResponseBlockUtility responseBlockUtility = (ResponseBlockUtility)theEObject;
				T result = caseResponseBlockUtility(responseBlockUtility);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UtilityPackage.EVENT_BLOCK_UTILITY: {
				EventBlockUtility eventBlockUtility = (EventBlockUtility)theEObject;
				T result = caseEventBlockUtility(eventBlockUtility);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UtilityPackage.ALARM_BLOCK_UTILITY: {
				AlarmBlockUtility alarmBlockUtility = (AlarmBlockUtility)theEObject;
				T result = caseAlarmBlockUtility(alarmBlockUtility);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UtilityPackage.DATA_POINT_BLOCK_UTILITY: {
				DataPointBlockUtility dataPointBlockUtility = (DataPointBlockUtility)theEObject;
				T result = caseDataPointBlockUtility(dataPointBlockUtility);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UtilityPackage.COMMAND_DISTRIBUTION_UTILITY: {
				CommandDistributionUtility commandDistributionUtility = (CommandDistributionUtility)theEObject;
				T result = caseCommandDistributionUtility(commandDistributionUtility);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Command Validation Utility</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Command Validation Utility</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCommandValidationUtility(CommandValidationUtility object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Alarm Handling Utility</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Alarm Handling Utility</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAlarmHandlingUtility(AlarmHandlingUtility object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Point Handling Utility</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Point Handling Utility</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataPointHandlingUtility(DataPointHandlingUtility object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operation Utility</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operation Utility</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperationUtility(OperationUtility object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Command Utility</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Command Utility</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCommandUtility(CommandUtility object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Response Validation Utility</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Response Validation Utility</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseResponseValidationUtility(ResponseValidationUtility object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Alarm Condition Utility</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Alarm Condition Utility</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAlarmConditionUtility(AlarmConditionUtility object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Point Utility</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Point Utility</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataPointUtility(DataPointUtility object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Alarm Utility</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Alarm Utility</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAlarmUtility(AlarmUtility object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Event Utility</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Event Utility</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEventUtility(EventUtility object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Response Utility</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Response Utility</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseResponseUtility(ResponseUtility object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>State Utility</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>State Utility</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStateUtility(StateUtility object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Authorisation Utility</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Authorisation Utility</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAuthorisationUtility(AuthorisationUtility object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Role Utility</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Role Utility</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRoleUtility(RoleUtility object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>User Utility</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>User Utility</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUserUtility(UserUtility object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Transition Utility</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Transition Utility</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTransitionUtility(TransitionUtility object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Command Response Block Utility</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Command Response Block Utility</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCommandResponseBlockUtility(CommandResponseBlockUtility object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Response Block Utility</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Response Block Utility</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseResponseBlockUtility(ResponseBlockUtility object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Event Block Utility</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Event Block Utility</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEventBlockUtility(EventBlockUtility object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Alarm Block Utility</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Alarm Block Utility</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAlarmBlockUtility(AlarmBlockUtility object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Point Block Utility</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Point Block Utility</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataPointBlockUtility(DataPointBlockUtility object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Command Distribution Utility</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Command Distribution Utility</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCommandDistributionUtility(CommandDistributionUtility object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //UtilitySwitch
