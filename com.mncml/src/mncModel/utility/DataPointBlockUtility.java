/**
 */
package mncModel.utility;

import mncModel.DataPointBlock;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Point Block Utility</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mncModel.utility.DataPointBlockUtility#getDataPointBlocks <em>Data Point Blocks</em>}</li>
 * </ul>
 *
 * @see mncModel.utility.UtilityPackage#getDataPointBlockUtility()
 * @model
 * @generated
 */
public interface DataPointBlockUtility extends EObject {
	/**
	 * Returns the value of the '<em><b>Data Point Blocks</b></em>' containment reference list.
	 * The list contents are of type {@link mncModel.DataPointBlock}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Point Blocks</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Point Blocks</em>' containment reference list.
	 * @see mncModel.utility.UtilityPackage#getDataPointBlockUtility_DataPointBlocks()
	 * @model containment="true"
	 * @generated
	 */
	EList<DataPointBlock> getDataPointBlocks();

} // DataPointBlockUtility
