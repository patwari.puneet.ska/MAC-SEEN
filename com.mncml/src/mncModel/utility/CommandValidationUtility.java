/**
 */
package mncModel.utility;

import mncModel.CommandValidation;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Command Validation Utility</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mncModel.utility.CommandValidationUtility#getCommandValidations <em>Command Validations</em>}</li>
 * </ul>
 *
 * @see mncModel.utility.UtilityPackage#getCommandValidationUtility()
 * @model
 * @generated
 */
public interface CommandValidationUtility extends EObject {
	/**
	 * Returns the value of the '<em><b>Command Validations</b></em>' containment reference list.
	 * The list contents are of type {@link mncModel.CommandValidation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Command Validations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Command Validations</em>' containment reference list.
	 * @see mncModel.utility.UtilityPackage#getCommandValidationUtility_CommandValidations()
	 * @model containment="true"
	 * @generated
	 */
	EList<CommandValidation> getCommandValidations();

} // CommandValidationUtility
