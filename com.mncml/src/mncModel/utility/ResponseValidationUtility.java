/**
 */
package mncModel.utility;

import mncModel.ResponseValidation;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Response Validation Utility</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mncModel.utility.ResponseValidationUtility#getResponseValidations <em>Response Validations</em>}</li>
 * </ul>
 *
 * @see mncModel.utility.UtilityPackage#getResponseValidationUtility()
 * @model
 * @generated
 */
public interface ResponseValidationUtility extends EObject {
	/**
	 * Returns the value of the '<em><b>Response Validations</b></em>' containment reference list.
	 * The list contents are of type {@link mncModel.ResponseValidation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Response Validations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Response Validations</em>' containment reference list.
	 * @see mncModel.utility.UtilityPackage#getResponseValidationUtility_ResponseValidations()
	 * @model containment="true"
	 * @generated
	 */
	EList<ResponseValidation> getResponseValidations();

} // ResponseValidationUtility
