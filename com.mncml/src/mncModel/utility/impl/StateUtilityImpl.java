/**
 */
package mncModel.utility.impl;

import java.util.Collection;

import mncModel.OperatingState;

import mncModel.utility.StateUtility;
import mncModel.utility.UtilityPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>State Utility</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link mncModel.utility.impl.StateUtilityImpl#getOperatingStates <em>Operating States</em>}</li>
 *   <li>{@link mncModel.utility.impl.StateUtilityImpl#getStartState <em>Start State</em>}</li>
 *   <li>{@link mncModel.utility.impl.StateUtilityImpl#getEndState <em>End State</em>}</li>
 * </ul>
 *
 * @generated
 */
public class StateUtilityImpl extends MinimalEObjectImpl.Container implements StateUtility {
	/**
	 * The cached value of the '{@link #getOperatingStates() <em>Operating States</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperatingStates()
	 * @generated
	 * @ordered
	 */
	protected EList<OperatingState> operatingStates;

	/**
	 * The cached value of the '{@link #getStartState() <em>Start State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartState()
	 * @generated
	 * @ordered
	 */
	protected OperatingState startState;

	/**
	 * The cached value of the '{@link #getEndState() <em>End State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndState()
	 * @generated
	 * @ordered
	 */
	protected OperatingState endState;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StateUtilityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UtilityPackage.Literals.STATE_UTILITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OperatingState> getOperatingStates() {
		if (operatingStates == null) {
			operatingStates = new EObjectContainmentEList<OperatingState>(OperatingState.class, this, UtilityPackage.STATE_UTILITY__OPERATING_STATES);
		}
		return operatingStates;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperatingState getStartState() {
		if (startState != null && startState.eIsProxy()) {
			InternalEObject oldStartState = (InternalEObject)startState;
			startState = (OperatingState)eResolveProxy(oldStartState);
			if (startState != oldStartState) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UtilityPackage.STATE_UTILITY__START_STATE, oldStartState, startState));
			}
		}
		return startState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperatingState basicGetStartState() {
		return startState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStartState(OperatingState newStartState) {
		OperatingState oldStartState = startState;
		startState = newStartState;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UtilityPackage.STATE_UTILITY__START_STATE, oldStartState, startState));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperatingState getEndState() {
		if (endState != null && endState.eIsProxy()) {
			InternalEObject oldEndState = (InternalEObject)endState;
			endState = (OperatingState)eResolveProxy(oldEndState);
			if (endState != oldEndState) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UtilityPackage.STATE_UTILITY__END_STATE, oldEndState, endState));
			}
		}
		return endState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperatingState basicGetEndState() {
		return endState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEndState(OperatingState newEndState) {
		OperatingState oldEndState = endState;
		endState = newEndState;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UtilityPackage.STATE_UTILITY__END_STATE, oldEndState, endState));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UtilityPackage.STATE_UTILITY__OPERATING_STATES:
				return ((InternalEList<?>)getOperatingStates()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UtilityPackage.STATE_UTILITY__OPERATING_STATES:
				return getOperatingStates();
			case UtilityPackage.STATE_UTILITY__START_STATE:
				if (resolve) return getStartState();
				return basicGetStartState();
			case UtilityPackage.STATE_UTILITY__END_STATE:
				if (resolve) return getEndState();
				return basicGetEndState();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UtilityPackage.STATE_UTILITY__OPERATING_STATES:
				getOperatingStates().clear();
				getOperatingStates().addAll((Collection<? extends OperatingState>)newValue);
				return;
			case UtilityPackage.STATE_UTILITY__START_STATE:
				setStartState((OperatingState)newValue);
				return;
			case UtilityPackage.STATE_UTILITY__END_STATE:
				setEndState((OperatingState)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UtilityPackage.STATE_UTILITY__OPERATING_STATES:
				getOperatingStates().clear();
				return;
			case UtilityPackage.STATE_UTILITY__START_STATE:
				setStartState((OperatingState)null);
				return;
			case UtilityPackage.STATE_UTILITY__END_STATE:
				setEndState((OperatingState)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UtilityPackage.STATE_UTILITY__OPERATING_STATES:
				return operatingStates != null && !operatingStates.isEmpty();
			case UtilityPackage.STATE_UTILITY__START_STATE:
				return startState != null;
			case UtilityPackage.STATE_UTILITY__END_STATE:
				return endState != null;
		}
		return super.eIsSet(featureID);
	}

} //StateUtilityImpl
