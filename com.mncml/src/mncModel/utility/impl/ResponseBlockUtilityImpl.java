/**
 */
package mncModel.utility.impl;

import java.util.Collection;

import mncModel.ResponseBlock;

import mncModel.utility.ResponseBlockUtility;
import mncModel.utility.UtilityPackage;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Response Block Utility</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link mncModel.utility.impl.ResponseBlockUtilityImpl#getResponseBlocks <em>Response Blocks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ResponseBlockUtilityImpl extends MinimalEObjectImpl.Container implements ResponseBlockUtility {
	/**
	 * The cached value of the '{@link #getResponseBlocks() <em>Response Blocks</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResponseBlocks()
	 * @generated
	 * @ordered
	 */
	protected EList<ResponseBlock> responseBlocks;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ResponseBlockUtilityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UtilityPackage.Literals.RESPONSE_BLOCK_UTILITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ResponseBlock> getResponseBlocks() {
		if (responseBlocks == null) {
			responseBlocks = new EObjectContainmentEList<ResponseBlock>(ResponseBlock.class, this, UtilityPackage.RESPONSE_BLOCK_UTILITY__RESPONSE_BLOCKS);
		}
		return responseBlocks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UtilityPackage.RESPONSE_BLOCK_UTILITY__RESPONSE_BLOCKS:
				return ((InternalEList<?>)getResponseBlocks()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UtilityPackage.RESPONSE_BLOCK_UTILITY__RESPONSE_BLOCKS:
				return getResponseBlocks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UtilityPackage.RESPONSE_BLOCK_UTILITY__RESPONSE_BLOCKS:
				getResponseBlocks().clear();
				getResponseBlocks().addAll((Collection<? extends ResponseBlock>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UtilityPackage.RESPONSE_BLOCK_UTILITY__RESPONSE_BLOCKS:
				getResponseBlocks().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UtilityPackage.RESPONSE_BLOCK_UTILITY__RESPONSE_BLOCKS:
				return responseBlocks != null && !responseBlocks.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ResponseBlockUtilityImpl
