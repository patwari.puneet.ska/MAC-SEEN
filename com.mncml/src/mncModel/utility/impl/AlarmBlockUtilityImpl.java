/**
 */
package mncModel.utility.impl;

import java.util.Collection;

import mncModel.AlarmBlock;

import mncModel.utility.AlarmBlockUtility;
import mncModel.utility.UtilityPackage;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Alarm Block Utility</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link mncModel.utility.impl.AlarmBlockUtilityImpl#getAlarmBlocks <em>Alarm Blocks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AlarmBlockUtilityImpl extends MinimalEObjectImpl.Container implements AlarmBlockUtility {
	/**
	 * The cached value of the '{@link #getAlarmBlocks() <em>Alarm Blocks</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAlarmBlocks()
	 * @generated
	 * @ordered
	 */
	protected EList<AlarmBlock> alarmBlocks;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AlarmBlockUtilityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UtilityPackage.Literals.ALARM_BLOCK_UTILITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AlarmBlock> getAlarmBlocks() {
		if (alarmBlocks == null) {
			alarmBlocks = new EObjectContainmentEList<AlarmBlock>(AlarmBlock.class, this, UtilityPackage.ALARM_BLOCK_UTILITY__ALARM_BLOCKS);
		}
		return alarmBlocks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UtilityPackage.ALARM_BLOCK_UTILITY__ALARM_BLOCKS:
				return ((InternalEList<?>)getAlarmBlocks()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UtilityPackage.ALARM_BLOCK_UTILITY__ALARM_BLOCKS:
				return getAlarmBlocks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UtilityPackage.ALARM_BLOCK_UTILITY__ALARM_BLOCKS:
				getAlarmBlocks().clear();
				getAlarmBlocks().addAll((Collection<? extends AlarmBlock>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UtilityPackage.ALARM_BLOCK_UTILITY__ALARM_BLOCKS:
				getAlarmBlocks().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UtilityPackage.ALARM_BLOCK_UTILITY__ALARM_BLOCKS:
				return alarmBlocks != null && !alarmBlocks.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //AlarmBlockUtilityImpl
