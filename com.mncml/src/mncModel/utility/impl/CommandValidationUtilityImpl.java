/**
 */
package mncModel.utility.impl;

import java.util.Collection;

import mncModel.CommandValidation;

import mncModel.utility.CommandValidationUtility;
import mncModel.utility.UtilityPackage;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Command Validation Utility</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link mncModel.utility.impl.CommandValidationUtilityImpl#getCommandValidations <em>Command Validations</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CommandValidationUtilityImpl extends MinimalEObjectImpl.Container implements CommandValidationUtility {
	/**
	 * The cached value of the '{@link #getCommandValidations() <em>Command Validations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCommandValidations()
	 * @generated
	 * @ordered
	 */
	protected EList<CommandValidation> commandValidations;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CommandValidationUtilityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UtilityPackage.Literals.COMMAND_VALIDATION_UTILITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CommandValidation> getCommandValidations() {
		if (commandValidations == null) {
			commandValidations = new EObjectContainmentEList<CommandValidation>(CommandValidation.class, this, UtilityPackage.COMMAND_VALIDATION_UTILITY__COMMAND_VALIDATIONS);
		}
		return commandValidations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UtilityPackage.COMMAND_VALIDATION_UTILITY__COMMAND_VALIDATIONS:
				return ((InternalEList<?>)getCommandValidations()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UtilityPackage.COMMAND_VALIDATION_UTILITY__COMMAND_VALIDATIONS:
				return getCommandValidations();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UtilityPackage.COMMAND_VALIDATION_UTILITY__COMMAND_VALIDATIONS:
				getCommandValidations().clear();
				getCommandValidations().addAll((Collection<? extends CommandValidation>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UtilityPackage.COMMAND_VALIDATION_UTILITY__COMMAND_VALIDATIONS:
				getCommandValidations().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UtilityPackage.COMMAND_VALIDATION_UTILITY__COMMAND_VALIDATIONS:
				return commandValidations != null && !commandValidations.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //CommandValidationUtilityImpl
