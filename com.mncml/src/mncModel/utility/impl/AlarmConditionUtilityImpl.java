/**
 */
package mncModel.utility.impl;

import java.util.Collection;

import mncModel.AlarmTriggerCondition;

import mncModel.utility.AlarmConditionUtility;
import mncModel.utility.UtilityPackage;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Alarm Condition Utility</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link mncModel.utility.impl.AlarmConditionUtilityImpl#getAlarmConditions <em>Alarm Conditions</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AlarmConditionUtilityImpl extends MinimalEObjectImpl.Container implements AlarmConditionUtility {
	/**
	 * The cached value of the '{@link #getAlarmConditions() <em>Alarm Conditions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAlarmConditions()
	 * @generated
	 * @ordered
	 */
	protected EList<AlarmTriggerCondition> alarmConditions;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AlarmConditionUtilityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UtilityPackage.Literals.ALARM_CONDITION_UTILITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AlarmTriggerCondition> getAlarmConditions() {
		if (alarmConditions == null) {
			alarmConditions = new EObjectContainmentEList<AlarmTriggerCondition>(AlarmTriggerCondition.class, this, UtilityPackage.ALARM_CONDITION_UTILITY__ALARM_CONDITIONS);
		}
		return alarmConditions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UtilityPackage.ALARM_CONDITION_UTILITY__ALARM_CONDITIONS:
				return ((InternalEList<?>)getAlarmConditions()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UtilityPackage.ALARM_CONDITION_UTILITY__ALARM_CONDITIONS:
				return getAlarmConditions();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UtilityPackage.ALARM_CONDITION_UTILITY__ALARM_CONDITIONS:
				getAlarmConditions().clear();
				getAlarmConditions().addAll((Collection<? extends AlarmTriggerCondition>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UtilityPackage.ALARM_CONDITION_UTILITY__ALARM_CONDITIONS:
				getAlarmConditions().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UtilityPackage.ALARM_CONDITION_UTILITY__ALARM_CONDITIONS:
				return alarmConditions != null && !alarmConditions.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //AlarmConditionUtilityImpl
