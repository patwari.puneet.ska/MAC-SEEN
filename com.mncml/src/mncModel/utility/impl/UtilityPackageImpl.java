/**
 */
package mncModel.utility.impl;

import mncModel.MncModelPackage;

import mncModel.impl.MncModelPackageImpl;

import mncModel.security.SecurityPackage;

import mncModel.security.impl.SecurityPackageImpl;

import mncModel.stakeholder.StakeholderPackage;

import mncModel.stakeholder.impl.StakeholderPackageImpl;

import mncModel.utility.AlarmBlockUtility;
import mncModel.utility.AlarmConditionUtility;
import mncModel.utility.AlarmHandlingUtility;
import mncModel.utility.AlarmUtility;
import mncModel.utility.AuthorisationUtility;
import mncModel.utility.CommandDistributionUtility;
import mncModel.utility.CommandResponseBlockUtility;
import mncModel.utility.CommandUtility;
import mncModel.utility.CommandValidationUtility;
import mncModel.utility.DataPointBlockUtility;
import mncModel.utility.DataPointHandlingUtility;
import mncModel.utility.DataPointUtility;
import mncModel.utility.EventBlockUtility;
import mncModel.utility.EventUtility;
import mncModel.utility.OperationUtility;
import mncModel.utility.ResponseBlockUtility;
import mncModel.utility.ResponseUtility;
import mncModel.utility.ResponseValidationUtility;
import mncModel.utility.RoleUtility;
import mncModel.utility.StateUtility;
import mncModel.utility.TransitionUtility;
import mncModel.utility.UserUtility;
import mncModel.utility.UtilityFactory;
import mncModel.utility.UtilityPackage;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class UtilityPackageImpl extends EPackageImpl implements UtilityPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass commandValidationUtilityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass alarmHandlingUtilityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataPointHandlingUtilityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass operationUtilityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass commandUtilityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass responseValidationUtilityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass alarmConditionUtilityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataPointUtilityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass alarmUtilityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eventUtilityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass responseUtilityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stateUtilityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass authorisationUtilityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass roleUtilityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass userUtilityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass transitionUtilityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass commandResponseBlockUtilityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass responseBlockUtilityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eventBlockUtilityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass alarmBlockUtilityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataPointBlockUtilityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass commandDistributionUtilityEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see mncModel.utility.UtilityPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private UtilityPackageImpl() {
		super(eNS_URI, UtilityFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link UtilityPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static UtilityPackage init() {
		if (isInited) return (UtilityPackage)EPackage.Registry.INSTANCE.getEPackage(UtilityPackage.eNS_URI);

		// Obtain or create and register package
		UtilityPackageImpl theUtilityPackage = (UtilityPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof UtilityPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new UtilityPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		MncModelPackageImpl theMncModelPackage = (MncModelPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(MncModelPackage.eNS_URI) instanceof MncModelPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(MncModelPackage.eNS_URI) : MncModelPackage.eINSTANCE);
		SecurityPackageImpl theSecurityPackage = (SecurityPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(SecurityPackage.eNS_URI) instanceof SecurityPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(SecurityPackage.eNS_URI) : SecurityPackage.eINSTANCE);
		StakeholderPackageImpl theStakeholderPackage = (StakeholderPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(StakeholderPackage.eNS_URI) instanceof StakeholderPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(StakeholderPackage.eNS_URI) : StakeholderPackage.eINSTANCE);

		// Create package meta-data objects
		theUtilityPackage.createPackageContents();
		theMncModelPackage.createPackageContents();
		theSecurityPackage.createPackageContents();
		theStakeholderPackage.createPackageContents();

		// Initialize created meta-data
		theUtilityPackage.initializePackageContents();
		theMncModelPackage.initializePackageContents();
		theSecurityPackage.initializePackageContents();
		theStakeholderPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theUtilityPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(UtilityPackage.eNS_URI, theUtilityPackage);
		return theUtilityPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCommandValidationUtility() {
		return commandValidationUtilityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCommandValidationUtility_CommandValidations() {
		return (EReference)commandValidationUtilityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAlarmHandlingUtility() {
		return alarmHandlingUtilityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAlarmHandlingUtility_AlarmHandling() {
		return (EReference)alarmHandlingUtilityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataPointHandlingUtility() {
		return dataPointHandlingUtilityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataPointHandlingUtility_DataPointHandling() {
		return (EReference)dataPointHandlingUtilityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOperationUtility() {
		return operationUtilityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationUtility_Operations() {
		return (EReference)operationUtilityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCommandUtility() {
		return commandUtilityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCommandUtility_Commands() {
		return (EReference)commandUtilityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getResponseValidationUtility() {
		return responseValidationUtilityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getResponseValidationUtility_ResponseValidations() {
		return (EReference)responseValidationUtilityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAlarmConditionUtility() {
		return alarmConditionUtilityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAlarmConditionUtility_AlarmConditions() {
		return (EReference)alarmConditionUtilityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataPointUtility() {
		return dataPointUtilityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataPointUtility_DataPoints() {
		return (EReference)dataPointUtilityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAlarmUtility() {
		return alarmUtilityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAlarmUtility_Alarms() {
		return (EReference)alarmUtilityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEventUtility() {
		return eventUtilityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEventUtility_Events() {
		return (EReference)eventUtilityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getResponseUtility() {
		return responseUtilityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getResponseUtility_Responses() {
		return (EReference)responseUtilityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStateUtility() {
		return stateUtilityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getStateUtility_OperatingStates() {
		return (EReference)stateUtilityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getStateUtility_StartState() {
		return (EReference)stateUtilityEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getStateUtility_EndState() {
		return (EReference)stateUtilityEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAuthorisationUtility() {
		return authorisationUtilityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAuthorisationUtility_Authorisations() {
		return (EReference)authorisationUtilityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRoleUtility() {
		return roleUtilityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRoleUtility_Roles() {
		return (EReference)roleUtilityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUserUtility() {
		return userUtilityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUserUtility_Users() {
		return (EReference)userUtilityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTransitionUtility() {
		return transitionUtilityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransitionUtility_Transitions() {
		return (EReference)transitionUtilityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCommandResponseBlockUtility() {
		return commandResponseBlockUtilityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCommandResponseBlockUtility_CommandResponseBlocks() {
		return (EReference)commandResponseBlockUtilityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getResponseBlockUtility() {
		return responseBlockUtilityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getResponseBlockUtility_ResponseBlocks() {
		return (EReference)responseBlockUtilityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEventBlockUtility() {
		return eventBlockUtilityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEventBlockUtility_EventBlocks() {
		return (EReference)eventBlockUtilityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAlarmBlockUtility() {
		return alarmBlockUtilityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAlarmBlockUtility_AlarmBlocks() {
		return (EReference)alarmBlockUtilityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataPointBlockUtility() {
		return dataPointBlockUtilityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataPointBlockUtility_DataPointBlocks() {
		return (EReference)dataPointBlockUtilityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCommandDistributionUtility() {
		return commandDistributionUtilityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCommandDistributionUtility_CommandDistributions() {
		return (EReference)commandDistributionUtilityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UtilityFactory getUtilityFactory() {
		return (UtilityFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		commandValidationUtilityEClass = createEClass(COMMAND_VALIDATION_UTILITY);
		createEReference(commandValidationUtilityEClass, COMMAND_VALIDATION_UTILITY__COMMAND_VALIDATIONS);

		alarmHandlingUtilityEClass = createEClass(ALARM_HANDLING_UTILITY);
		createEReference(alarmHandlingUtilityEClass, ALARM_HANDLING_UTILITY__ALARM_HANDLING);

		dataPointHandlingUtilityEClass = createEClass(DATA_POINT_HANDLING_UTILITY);
		createEReference(dataPointHandlingUtilityEClass, DATA_POINT_HANDLING_UTILITY__DATA_POINT_HANDLING);

		operationUtilityEClass = createEClass(OPERATION_UTILITY);
		createEReference(operationUtilityEClass, OPERATION_UTILITY__OPERATIONS);

		commandUtilityEClass = createEClass(COMMAND_UTILITY);
		createEReference(commandUtilityEClass, COMMAND_UTILITY__COMMANDS);

		responseValidationUtilityEClass = createEClass(RESPONSE_VALIDATION_UTILITY);
		createEReference(responseValidationUtilityEClass, RESPONSE_VALIDATION_UTILITY__RESPONSE_VALIDATIONS);

		alarmConditionUtilityEClass = createEClass(ALARM_CONDITION_UTILITY);
		createEReference(alarmConditionUtilityEClass, ALARM_CONDITION_UTILITY__ALARM_CONDITIONS);

		dataPointUtilityEClass = createEClass(DATA_POINT_UTILITY);
		createEReference(dataPointUtilityEClass, DATA_POINT_UTILITY__DATA_POINTS);

		alarmUtilityEClass = createEClass(ALARM_UTILITY);
		createEReference(alarmUtilityEClass, ALARM_UTILITY__ALARMS);

		eventUtilityEClass = createEClass(EVENT_UTILITY);
		createEReference(eventUtilityEClass, EVENT_UTILITY__EVENTS);

		responseUtilityEClass = createEClass(RESPONSE_UTILITY);
		createEReference(responseUtilityEClass, RESPONSE_UTILITY__RESPONSES);

		stateUtilityEClass = createEClass(STATE_UTILITY);
		createEReference(stateUtilityEClass, STATE_UTILITY__OPERATING_STATES);
		createEReference(stateUtilityEClass, STATE_UTILITY__START_STATE);
		createEReference(stateUtilityEClass, STATE_UTILITY__END_STATE);

		authorisationUtilityEClass = createEClass(AUTHORISATION_UTILITY);
		createEReference(authorisationUtilityEClass, AUTHORISATION_UTILITY__AUTHORISATIONS);

		roleUtilityEClass = createEClass(ROLE_UTILITY);
		createEReference(roleUtilityEClass, ROLE_UTILITY__ROLES);

		userUtilityEClass = createEClass(USER_UTILITY);
		createEReference(userUtilityEClass, USER_UTILITY__USERS);

		transitionUtilityEClass = createEClass(TRANSITION_UTILITY);
		createEReference(transitionUtilityEClass, TRANSITION_UTILITY__TRANSITIONS);

		commandResponseBlockUtilityEClass = createEClass(COMMAND_RESPONSE_BLOCK_UTILITY);
		createEReference(commandResponseBlockUtilityEClass, COMMAND_RESPONSE_BLOCK_UTILITY__COMMAND_RESPONSE_BLOCKS);

		responseBlockUtilityEClass = createEClass(RESPONSE_BLOCK_UTILITY);
		createEReference(responseBlockUtilityEClass, RESPONSE_BLOCK_UTILITY__RESPONSE_BLOCKS);

		eventBlockUtilityEClass = createEClass(EVENT_BLOCK_UTILITY);
		createEReference(eventBlockUtilityEClass, EVENT_BLOCK_UTILITY__EVENT_BLOCKS);

		alarmBlockUtilityEClass = createEClass(ALARM_BLOCK_UTILITY);
		createEReference(alarmBlockUtilityEClass, ALARM_BLOCK_UTILITY__ALARM_BLOCKS);

		dataPointBlockUtilityEClass = createEClass(DATA_POINT_BLOCK_UTILITY);
		createEReference(dataPointBlockUtilityEClass, DATA_POINT_BLOCK_UTILITY__DATA_POINT_BLOCKS);

		commandDistributionUtilityEClass = createEClass(COMMAND_DISTRIBUTION_UTILITY);
		createEReference(commandDistributionUtilityEClass, COMMAND_DISTRIBUTION_UTILITY__COMMAND_DISTRIBUTIONS);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		MncModelPackage theMncModelPackage = (MncModelPackage)EPackage.Registry.INSTANCE.getEPackage(MncModelPackage.eNS_URI);
		SecurityPackage theSecurityPackage = (SecurityPackage)EPackage.Registry.INSTANCE.getEPackage(SecurityPackage.eNS_URI);
		StakeholderPackage theStakeholderPackage = (StakeholderPackage)EPackage.Registry.INSTANCE.getEPackage(StakeholderPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes and features; add operations and parameters
		initEClass(commandValidationUtilityEClass, CommandValidationUtility.class, "CommandValidationUtility", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCommandValidationUtility_CommandValidations(), theMncModelPackage.getCommandValidation(), null, "commandValidations", null, 0, -1, CommandValidationUtility.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(alarmHandlingUtilityEClass, AlarmHandlingUtility.class, "AlarmHandlingUtility", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAlarmHandlingUtility_AlarmHandling(), theMncModelPackage.getAlarmHandling(), null, "alarmHandling", null, 0, -1, AlarmHandlingUtility.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dataPointHandlingUtilityEClass, DataPointHandlingUtility.class, "DataPointHandlingUtility", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDataPointHandlingUtility_DataPointHandling(), theMncModelPackage.getDataPointHandling(), null, "dataPointHandling", null, 0, -1, DataPointHandlingUtility.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(operationUtilityEClass, OperationUtility.class, "OperationUtility", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getOperationUtility_Operations(), theMncModelPackage.getAbstractOperationableItems(), null, "operations", null, 0, -1, OperationUtility.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(commandUtilityEClass, CommandUtility.class, "CommandUtility", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCommandUtility_Commands(), theMncModelPackage.getCommand(), null, "commands", null, 0, -1, CommandUtility.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(responseValidationUtilityEClass, ResponseValidationUtility.class, "ResponseValidationUtility", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getResponseValidationUtility_ResponseValidations(), theMncModelPackage.getResponseValidation(), null, "responseValidations", null, 0, -1, ResponseValidationUtility.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(alarmConditionUtilityEClass, AlarmConditionUtility.class, "AlarmConditionUtility", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAlarmConditionUtility_AlarmConditions(), theMncModelPackage.getAlarmTriggerCondition(), null, "alarmConditions", null, 0, -1, AlarmConditionUtility.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dataPointUtilityEClass, DataPointUtility.class, "DataPointUtility", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDataPointUtility_DataPoints(), theMncModelPackage.getDataPoint(), null, "dataPoints", null, 0, -1, DataPointUtility.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(alarmUtilityEClass, AlarmUtility.class, "AlarmUtility", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAlarmUtility_Alarms(), theMncModelPackage.getAlarm(), null, "alarms", null, 0, -1, AlarmUtility.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(eventUtilityEClass, EventUtility.class, "EventUtility", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEventUtility_Events(), theMncModelPackage.getEvent(), null, "events", null, 0, -1, EventUtility.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(responseUtilityEClass, ResponseUtility.class, "ResponseUtility", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getResponseUtility_Responses(), theMncModelPackage.getResponse(), null, "responses", null, 0, -1, ResponseUtility.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(stateUtilityEClass, StateUtility.class, "StateUtility", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getStateUtility_OperatingStates(), theMncModelPackage.getOperatingState(), null, "operatingStates", null, 0, -1, StateUtility.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getStateUtility_StartState(), theMncModelPackage.getOperatingState(), null, "startState", null, 0, 1, StateUtility.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getStateUtility_EndState(), theMncModelPackage.getOperatingState(), null, "endState", null, 0, 1, StateUtility.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(authorisationUtilityEClass, AuthorisationUtility.class, "AuthorisationUtility", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAuthorisationUtility_Authorisations(), theSecurityPackage.getAuthorisation(), null, "authorisations", null, 0, -1, AuthorisationUtility.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(roleUtilityEClass, RoleUtility.class, "RoleUtility", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRoleUtility_Roles(), theStakeholderPackage.getRole(), null, "roles", null, 0, -1, RoleUtility.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(userUtilityEClass, UserUtility.class, "UserUtility", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUserUtility_Users(), theStakeholderPackage.getUser(), null, "users", null, 0, -1, UserUtility.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(transitionUtilityEClass, TransitionUtility.class, "TransitionUtility", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTransitionUtility_Transitions(), theMncModelPackage.getTransition(), null, "transitions", null, 0, -1, TransitionUtility.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(commandResponseBlockUtilityEClass, CommandResponseBlockUtility.class, "CommandResponseBlockUtility", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCommandResponseBlockUtility_CommandResponseBlocks(), theMncModelPackage.getCommandResponseBlock(), null, "commandResponseBlocks", null, 0, -1, CommandResponseBlockUtility.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(responseBlockUtilityEClass, ResponseBlockUtility.class, "ResponseBlockUtility", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getResponseBlockUtility_ResponseBlocks(), theMncModelPackage.getResponseBlock(), null, "responseBlocks", null, 0, -1, ResponseBlockUtility.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(eventBlockUtilityEClass, EventBlockUtility.class, "EventBlockUtility", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEventBlockUtility_EventBlocks(), theMncModelPackage.getEventBlock(), null, "eventBlocks", null, 0, -1, EventBlockUtility.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(alarmBlockUtilityEClass, AlarmBlockUtility.class, "AlarmBlockUtility", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAlarmBlockUtility_AlarmBlocks(), theMncModelPackage.getAlarmBlock(), null, "alarmBlocks", null, 0, -1, AlarmBlockUtility.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dataPointBlockUtilityEClass, DataPointBlockUtility.class, "DataPointBlockUtility", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDataPointBlockUtility_DataPointBlocks(), theMncModelPackage.getDataPointBlock(), null, "dataPointBlocks", null, 0, -1, DataPointBlockUtility.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(commandDistributionUtilityEClass, CommandDistributionUtility.class, "CommandDistributionUtility", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCommandDistributionUtility_CommandDistributions(), theMncModelPackage.getCommandDistribution(), null, "commandDistributions", null, 0, -1, CommandDistributionUtility.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
	}

} //UtilityPackageImpl
