/**
 */
package mncModel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Command Distribution</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mncModel.CommandDistribution#getCommand <em>Command</em>}</li>
 *   <li>{@link mncModel.CommandDistribution#getDestinationNodes <em>Destination Nodes</em>}</li>
 * </ul>
 *
 * @see mncModel.MncModelPackage#getCommandDistribution()
 * @model
 * @generated
 */
public interface CommandDistribution extends EObject {
	/**
	 * Returns the value of the '<em><b>Command</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Command</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Command</em>' reference.
	 * @see #setCommand(Command)
	 * @see mncModel.MncModelPackage#getCommandDistribution_Command()
	 * @model
	 * @generated
	 */
	Command getCommand();

	/**
	 * Sets the value of the '{@link mncModel.CommandDistribution#getCommand <em>Command</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Command</em>' reference.
	 * @see #getCommand()
	 * @generated
	 */
	void setCommand(Command value);

	/**
	 * Returns the value of the '<em><b>Destination Nodes</b></em>' reference list.
	 * The list contents are of type {@link mncModel.ControlNode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Destination Nodes</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Destination Nodes</em>' reference list.
	 * @see mncModel.MncModelPackage#getCommandDistribution_DestinationNodes()
	 * @model
	 * @generated
	 */
	EList<ControlNode> getDestinationNodes();

} // CommandDistribution
