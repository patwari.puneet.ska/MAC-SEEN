/**
 */
package mncModel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>System</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see mncModel.MncModelPackage#getSystem()
 * @model
 * @generated
 */
public interface System extends EObject {
} // System
