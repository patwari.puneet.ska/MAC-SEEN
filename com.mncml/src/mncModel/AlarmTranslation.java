/**
 */
package mncModel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Alarm Translation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mncModel.AlarmTranslation#getAlarmTranslationRules <em>Alarm Translation Rules</em>}</li>
 * </ul>
 *
 * @see mncModel.MncModelPackage#getAlarmTranslation()
 * @model
 * @generated
 */
public interface AlarmTranslation extends EObject {
	/**
	 * Returns the value of the '<em><b>Alarm Translation Rules</b></em>' containment reference list.
	 * The list contents are of type {@link mncModel.AlarmTranslationRule}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Alarm Translation Rules</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Alarm Translation Rules</em>' containment reference list.
	 * @see mncModel.MncModelPackage#getAlarmTranslation_AlarmTranslationRules()
	 * @model containment="true"
	 * @generated
	 */
	EList<AlarmTranslationRule> getAlarmTranslationRules();

} // AlarmTranslation
