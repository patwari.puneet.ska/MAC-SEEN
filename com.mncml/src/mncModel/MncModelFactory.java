/**
 */
package mncModel;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see mncModel.MncModelPackage
 * @generated
 */
public interface MncModelFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	MncModelFactory eINSTANCE = mncModel.impl.MncModelFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Model</em>'.
	 * @generated
	 */
	Model createModel();

	/**
	 * Returns a new object of class '<em>Import</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Import</em>'.
	 * @generated
	 */
	Import createImport();

	/**
	 * Returns a new object of class '<em>System</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>System</em>'.
	 * @generated
	 */
	System createSystem();

	/**
	 * Returns a new object of class '<em>Control Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Control Node</em>'.
	 * @generated
	 */
	ControlNode createControlNode();

	/**
	 * Returns a new object of class '<em>Interface Description</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Interface Description</em>'.
	 * @generated
	 */
	InterfaceDescription createInterfaceDescription();

	/**
	 * Returns a new object of class '<em>Abstract Interface Items</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Abstract Interface Items</em>'.
	 * @generated
	 */
	AbstractInterfaceItems createAbstractInterfaceItems();

	/**
	 * Returns a new object of class '<em>Abstract Operationable Items</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Abstract Operationable Items</em>'.
	 * @generated
	 */
	AbstractOperationableItems createAbstractOperationableItems();

	/**
	 * Returns a new object of class '<em>Operating State</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Operating State</em>'.
	 * @generated
	 */
	OperatingState createOperatingState();

	/**
	 * Returns a new object of class '<em>Command Response Block</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Command Response Block</em>'.
	 * @generated
	 */
	CommandResponseBlock createCommandResponseBlock();

	/**
	 * Returns a new object of class '<em>Command</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Command</em>'.
	 * @generated
	 */
	Command createCommand();

	/**
	 * Returns a new object of class '<em>Command Validation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Command Validation</em>'.
	 * @generated
	 */
	CommandValidation createCommandValidation();

	/**
	 * Returns a new object of class '<em>Command Distribution</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Command Distribution</em>'.
	 * @generated
	 */
	CommandDistribution createCommandDistribution();

	/**
	 * Returns a new object of class '<em>Command Translation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Command Translation</em>'.
	 * @generated
	 */
	CommandTranslation createCommandTranslation();

	/**
	 * Returns a new object of class '<em>Command Trigger Condition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Command Trigger Condition</em>'.
	 * @generated
	 */
	CommandTriggerCondition createCommandTriggerCondition();

	/**
	 * Returns a new object of class '<em>Parameter Translation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Parameter Translation</em>'.
	 * @generated
	 */
	ParameterTranslation createParameterTranslation();

	/**
	 * Returns a new object of class '<em>Transition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Transition</em>'.
	 * @generated
	 */
	Transition createTransition();

	/**
	 * Returns a new object of class '<em>Response</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Response</em>'.
	 * @generated
	 */
	Response createResponse();

	/**
	 * Returns a new object of class '<em>Response Block</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Response Block</em>'.
	 * @generated
	 */
	ResponseBlock createResponseBlock();

	/**
	 * Returns a new object of class '<em>Response Validation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Response Validation</em>'.
	 * @generated
	 */
	ResponseValidation createResponseValidation();

	/**
	 * Returns a new object of class '<em>Response Distribution</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Response Distribution</em>'.
	 * @generated
	 */
	ResponseDistribution createResponseDistribution();

	/**
	 * Returns a new object of class '<em>Response Translation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Response Translation</em>'.
	 * @generated
	 */
	ResponseTranslation createResponseTranslation();

	/**
	 * Returns a new object of class '<em>Response Translation Rule</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Response Translation Rule</em>'.
	 * @generated
	 */
	ResponseTranslationRule createResponseTranslationRule();

	/**
	 * Returns a new object of class '<em>Port</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Port</em>'.
	 * @generated
	 */
	Port createPort();

	/**
	 * Returns a new object of class '<em>Event Block</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Event Block</em>'.
	 * @generated
	 */
	EventBlock createEventBlock();

	/**
	 * Returns a new object of class '<em>Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Event</em>'.
	 * @generated
	 */
	Event createEvent();

	/**
	 * Returns a new object of class '<em>Event Trigger Condition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Event Trigger Condition</em>'.
	 * @generated
	 */
	EventTriggerCondition createEventTriggerCondition();

	/**
	 * Returns a new object of class '<em>Event Translation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Event Translation</em>'.
	 * @generated
	 */
	EventTranslation createEventTranslation();

	/**
	 * Returns a new object of class '<em>Event Translation Rule</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Event Translation Rule</em>'.
	 * @generated
	 */
	EventTranslationRule createEventTranslationRule();

	/**
	 * Returns a new object of class '<em>Event Handling</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Event Handling</em>'.
	 * @generated
	 */
	EventHandling createEventHandling();

	/**
	 * Returns a new object of class '<em>Alarm Block</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Alarm Block</em>'.
	 * @generated
	 */
	AlarmBlock createAlarmBlock();

	/**
	 * Returns a new object of class '<em>Alarm</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Alarm</em>'.
	 * @generated
	 */
	Alarm createAlarm();

	/**
	 * Returns a new object of class '<em>Alarm Trigger Condition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Alarm Trigger Condition</em>'.
	 * @generated
	 */
	AlarmTriggerCondition createAlarmTriggerCondition();

	/**
	 * Returns a new object of class '<em>Alarm Translation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Alarm Translation</em>'.
	 * @generated
	 */
	AlarmTranslation createAlarmTranslation();

	/**
	 * Returns a new object of class '<em>Alarm Translation Rule</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Alarm Translation Rule</em>'.
	 * @generated
	 */
	AlarmTranslationRule createAlarmTranslationRule();

	/**
	 * Returns a new object of class '<em>Alarm Handling</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Alarm Handling</em>'.
	 * @generated
	 */
	AlarmHandling createAlarmHandling();

	/**
	 * Returns a new object of class '<em>Data Point Block</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Point Block</em>'.
	 * @generated
	 */
	DataPointBlock createDataPointBlock();

	/**
	 * Returns a new object of class '<em>Data Point</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Point</em>'.
	 * @generated
	 */
	DataPoint createDataPoint();

	/**
	 * Returns a new object of class '<em>Data Point Trigger Condition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Point Trigger Condition</em>'.
	 * @generated
	 */
	DataPointTriggerCondition createDataPointTriggerCondition();

	/**
	 * Returns a new object of class '<em>Data Point Translation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Point Translation</em>'.
	 * @generated
	 */
	DataPointTranslation createDataPointTranslation();

	/**
	 * Returns a new object of class '<em>Data Point Translation Rule</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Point Translation Rule</em>'.
	 * @generated
	 */
	DataPointTranslationRule createDataPointTranslationRule();

	/**
	 * Returns a new object of class '<em>Data Point Handling</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Point Handling</em>'.
	 * @generated
	 */
	DataPointHandling createDataPointHandling();

	/**
	 * Returns a new object of class '<em>Data Point Valid Condition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Point Valid Condition</em>'.
	 * @generated
	 */
	DataPointValidCondition createDataPointValidCondition();

	/**
	 * Returns a new object of class '<em>Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Parameter</em>'.
	 * @generated
	 */
	Parameter createParameter();

	/**
	 * Returns a new object of class '<em>Check Parameter Condition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Check Parameter Condition</em>'.
	 * @generated
	 */
	CheckParameterCondition createCheckParameterCondition();

	/**
	 * Returns a new object of class '<em>Array Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Array Type</em>'.
	 * @generated
	 */
	ArrayType createArrayType();

	/**
	 * Returns a new object of class '<em>Simple Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Simple Type</em>'.
	 * @generated
	 */
	SimpleType createSimpleType();

	/**
	 * Returns a new object of class '<em>Primitive Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Primitive Value</em>'.
	 * @generated
	 */
	PrimitiveValue createPrimitiveValue();

	/**
	 * Returns a new object of class '<em>Int Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Int Value</em>'.
	 * @generated
	 */
	IntValue createIntValue();

	/**
	 * Returns a new object of class '<em>Bool Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Bool Value</em>'.
	 * @generated
	 */
	BoolValue createBoolValue();

	/**
	 * Returns a new object of class '<em>Float Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Float Value</em>'.
	 * @generated
	 */
	FloatValue createFloatValue();

	/**
	 * Returns a new object of class '<em>String Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>String Value</em>'.
	 * @generated
	 */
	StringValue createStringValue();

	/**
	 * Returns a new object of class '<em>Operation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Operation</em>'.
	 * @generated
	 */
	Operation createOperation();

	/**
	 * Returns a new object of class '<em>Address</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Address</em>'.
	 * @generated
	 */
	Address createAddress();

	/**
	 * Returns a new object of class '<em>Action</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Action</em>'.
	 * @generated
	 */
	Action createAction();

	/**
	 * Returns a new object of class '<em>Publish</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Publish</em>'.
	 * @generated
	 */
	Publish createPublish();

	/**
	 * Returns a new object of class '<em>Subscribable Items</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Subscribable Items</em>'.
	 * @generated
	 */
	SubscribableItems createSubscribableItems();

	/**
	 * Returns a new object of class '<em>Subscribable Item List</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Subscribable Item List</em>'.
	 * @generated
	 */
	SubscribableItemList createSubscribableItemList();

	/**
	 * Returns a new object of class '<em>Responsible Item List</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Responsible Item List</em>'.
	 * @generated
	 */
	ResponsibleItemList createResponsibleItemList();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	MncModelPackage getMncModelPackage();

} //MncModelFactory
